package com.mkotlarz.mobile.productivity.phonelocker.main;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.ContactsContract;

import com.mkotlarz.mobile.productivity.phonelocker.data.CallRulesDao;
import com.mkotlarz.mobile.productivity.phonelocker.data.DataManagerImpl;
import com.mkotlarz.mobile.productivity.phonelocker.data.SmsRulesDao;
import com.mkotlarz.mobile.productivity.phonelocker.interfaces.ICondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.CallTypeCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.ContactNameCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.DateCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.DayCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.HourCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.KeywordCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.NumberCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.PatternCondition;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.lang.reflect.Constructor;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Maciej on 2014-07-04.
 * Jedna z najważneijszych klas tej aplikacji, jej zadaniem jest przetworzenie warunków i zdecydowanie czy połączenie/sms ma zostać odrzucone
 * Korzysta ona z logicznego ORa w przyszłości zostanie uzupełniona o enda
 * Przetwarza zarówno reguły dla smsów jak i połączeń
 */
public class RulesExecutor {

    public static final String PATH_TO_CONDITION = "com.mkotlarz.mobile.productivity.phonelocker.models.conditions.";

    private DataManagerImpl m_dataManager = null;
    private SmsRulesDao m_smsRulesDao = null;
    private CallRulesDao m_callRulesDao = null;
    private int m_actionType;
    private String m_number = null;
    private String m_messageBody = null;
    private Context m_context = null;
    private boolean m_result = false;
    private final Logger Log = Logger.getLogger(RulesExecutor.class);

    public ICondition MatchedCondition = null;

    public RulesExecutor(DataManagerImpl dataManager, Context context) {
        m_dataManager = dataManager;
        m_smsRulesDao = new SmsRulesDao(m_dataManager.getDb());
        m_callRulesDao = new CallRulesDao(m_dataManager.getDb());
        m_context = context;
    }

    public boolean Process(int actionType, String number, String messageBody) {
        m_actionType = actionType;
        m_number = number;
        m_messageBody = messageBody;
        switch (actionType) {
            case 0:
                return processCallRules();
            case 1:
                return processCallRules();
            case 2:
                //Incoming SMS
                return processSmsRules();
        }
        return false;
    }

    /*
    @m_databaseRecords - zawiera listę aktywnych reguł dla SMSów
    m_databaseRecords[0] - ruleActive 0/1
    m_databaseReceords[1] - ruleName
    m_databaseRecords[2] - ruleConditions dane zakodowane w formacie JSON
    m_databaseRecords[3] - data
     */


    /*
    * Przetwarza dane z bazy danych
    * Warunki w bazie są zapisane za pomocą JSONa
    * {"Conditions": { "NumberCondition": { "number": "123456789" }, "ContactNameCondition" : { "contactName": "SampleName" }}}
     */
    private boolean processSmsRules() {
        //Pobierz warunki z bazy
        String[][] m_databaseRecords = m_smsRulesDao.getAllActiveRules();
        if(m_databaseRecords.length == 0)
            return true;
        for(int i = 0; i < m_databaseRecords.length; i++) {
            try {
                Log.info(m_databaseRecords[i][1]);
                //Zapisz warunki do obiektu JSON wbudowanego w Jave
                JSONObject conditions = new JSONObject(m_databaseRecords[i][2]).getJSONObject("Conditions");
                //Pobierz klucze jako iteratory
                Iterator keys = conditions.keys();
                while(keys.hasNext()) {
                    //Zapisz iterator jako nazwe warunku
                    String conditionName = (String) keys.next();
                    //Sprawdź czy warunek zwraca true
                    m_result = checkConditionsMatch(conditionName, conditions.getJSONObject(conditionName));
                    if(m_result)
                        break;
                }
                if(m_result)
                    break;
            } catch (Exception ex) {
                Log.error(ex.getMessage() + "..." + ex.getStackTrace());
            }
        }
        Log.info("Conditions met:" + m_result);
        return m_result;
    }

    /*
    * Przetwarza dane z bazy danych
    * Warunki w bazie są zapisane za pomocą JSONa
    * {"Conditions": { "NumberCondition": { "number": "123456789" }, "ContactNameCondition" : { "contactName": "SampleName" }}}
    * Więcej danych szukaj wyżej
    */
    private boolean processCallRules() {
        String[][] m_databaseRecords = m_callRulesDao.getAllActiveRules();
        for(int i = 0; i < m_databaseRecords.length; i++) {
            try {
                Log.info(m_databaseRecords[i][1]);
                JSONObject conditions = new JSONObject(m_databaseRecords[i][2]).getJSONObject("Conditions");
                Iterator keys = conditions.keys();
                while(keys.hasNext()) {
                    String conditionName = (String) keys.next();
                    m_result = checkConditionsMatch(conditionName, conditions.getJSONObject(conditionName));
                    if(m_result)
                        break;
                }
            } catch (Exception ex) {
                Log.error(ex.getMessage() + "..." + ex.getStackTrace());
            }
        }
        Log.info("Conditions met:" + m_result);
        return m_result;
    }


    /*
    * Metoda przygotowywuje warunek do pozniejszego parsowania
    * Tworzy klase na podstawie nazwy przekazanej do metody
    * klasy szuka w namespacu com.mkotlarz.mobile.productivity.phonelocker.models.conditions
    */
    private boolean checkConditionsMatch(String conditionName, JSONObject values) {
        try {
            Class<?> clazz = Class.forName(PATH_TO_CONDITION + conditionName);
            Constructor<?> constructor = clazz.getConstructor();
            ICondition condition = (ICondition) constructor.newInstance();
            if(matchCondition(condition, values)) {
                MatchedCondition = condition;
                return true;
            }
            return false;
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getStackTrace());
            return false;
        }
    }

    //Wybiera odpowiednia metodę do sprawdzenia waruniu na podstawie jego typu bazowego
    private boolean matchCondition(ICondition condition, JSONObject values) {
            switch (condition.getClassCode()) {
                case 0:
                    CallTypeCondition callTypeCondition = (CallTypeCondition) condition;
                    return matchConditionCallTypeCondition(callTypeCondition, values);
                case 1:
                    NumberCondition numberCondition = (NumberCondition) condition;
                    return matchConditionNumberCondition(numberCondition, values);
                case 2:
                    DayCondition dayCondition = (DayCondition) condition;
                    return matchConditionDayCondition(dayCondition, values);
                case 3:
                    DateCondition dateCondition = (DateCondition) condition;
                    return matchConditionDateCondition(dateCondition, values);
                case 4:
                    ContactNameCondition contactNameCondition = (ContactNameCondition) condition;
                    return matchConditionContactNameCondition(contactNameCondition, values);
                case 5:
                    HourCondition hourCondition = (HourCondition) condition;
                    return matchConditionHourCondition(hourCondition, values);
                case 6:
                    KeywordCondition keywordCondition = (KeywordCondition) condition;
                    return  matchConditionKeywordCondition(keywordCondition, values);
                case 7:
                    PatternCondition patternCondition = (PatternCondition) condition;
                    return matchConditionPatternCondition(patternCondition, values);

            }
        return false;
    }

    /*
    * Metody poniżej pobierają dane z JSONObject values
    * oraz wypełniają nimi obiekt klasy warunku
    * po czym przekazują metodzie Match() potrzebne informacje o aktualnym zdarzeniu
    * Tu odbywa się prawidłowe sprawdzanie warunków reguły
    */

    private boolean matchConditionCallTypeCondition(CallTypeCondition condition, JSONObject values) {
        Log.info("Matching " + condition.getJSONName());
        try {
            String callType = values.getString("callType");
            condition.setCallType(callType);
            if (m_actionType == 0)
                return condition.Match(CallTypeCondition.Incoming);
            if (m_actionType == 1)
                return condition.Match(CallTypeCondition.Outgoing);
            return false;
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getStackTrace());
            return false;
        }
    }

    private boolean matchConditionNumberCondition(NumberCondition condition, JSONObject values) {
        try {
            String conditionNumber = values.getString("callNumber");
            condition.setNumber(conditionNumber);
            return condition.Match(m_number);
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getStackTrace());
            return false;
        }
    }

    private boolean matchConditionDayCondition(DayCondition condition, JSONObject values) {
        Log.info("Matching " + condition.getJSONName());
        try {
            int day = values.getInt("Day");
            condition.setDay(day);
            condition.setEveryWeek(true);
            return  condition.Match();
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getStackTrace());
            return false;
        }
    }

    private boolean matchConditionDateCondition(DateCondition condition, JSONObject values) {
        Log.info("Matching " + condition.getJSONName());
        try {
            SimpleDateFormat dateFormater = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            String startDateString = values.getString("startDate");
            String endDateString = values.getString("endDate");
            Date startDate = dateFormater.parse(startDateString);
            Date endDate = dateFormater.parse(endDateString);
            condition.setStartDate(startDate);
            condition.setEndDate(endDate);
            return condition.Match(Calendar.getInstance().getTime());
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getStackTrace());
            return false;
        }
    }

    private boolean matchConditionContactNameCondition(ContactNameCondition condition, JSONObject values) {
            Log.info("Matching " + condition.getJSONName());

        try {
            String conditionContactName = values.getString("contactName");
            condition.setContactName(conditionContactName);
            String contactName = "";
            try {
                contactName = getContactName();
            } catch (Exception ex) {
                return false;
            }
            return condition.Match(contactName);
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getStackTrace());
            return false;
        }
    }

    private boolean matchConditionHourCondition(HourCondition condition, JSONObject values) {
        Log.info("Matching " + condition.getJSONName());
        try {
            int startHour = values.getInt("startHour");
            int startMinutes = values.getInt("startMinutes");
            int endHour = values.getInt("endHour");
            int endMinutes = values.getInt("endMinutes");
            condition.setStartHour(startHour);
            condition.setStartMinutes(startMinutes);
            condition.setEndHour(endHour);
            condition.setEndMinutes(endMinutes);
            return condition.Match();
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getStackTrace());
            return false;
        }
    }

    private boolean matchConditionKeywordCondition(KeywordCondition condition, JSONObject values) {
        try {
            String keyword = values.getString("keyword");
            condition.setKeyword(keyword);
            return condition.Match(m_messageBody);
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getStackTrace());
            return false;
        }
    }

    private boolean matchConditionPatternCondition(PatternCondition condition, JSONObject values) {
        try {
            String pattern = values.getString("pattern");
            boolean match_contacts = values.getBoolean("match_contact_name");
            boolean match_number = values.getBoolean("match_number");
            boolean match_message_body = values.getBoolean("match_message_body");
            condition.setPattern(pattern);
            condition.setMatchContactName(match_contacts);
            condition.setMatchNumber(match_number);
            condition.setMatchMessageBody(match_message_body);
            String contact_name = "";
            try {
                contact_name = getContactName();
            } catch (Exception ex) {
            }
            return condition.Match(m_number, contact_name, m_messageBody);
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getStackTrace());
            return false;
        }
    }

    private String getContactName() throws Exception{
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(m_number));
        ContentResolver contentResolver = m_context.getContentResolver();
        String contactName = "";
        Cursor contactLookup = contentResolver.query(uri, new String[]{BaseColumns._ID,
                ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (contactLookup.moveToFirst()) {
            contactName = contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }
        return contactName;
    }
}