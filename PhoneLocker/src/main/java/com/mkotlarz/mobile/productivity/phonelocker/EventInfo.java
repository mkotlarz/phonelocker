package com.mkotlarz.mobile.productivity.phonelocker;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.support.v4.app.NavUtils;

import com.mkotlarz.mobile.productivity.phonelocker.R;
import com.mkotlarz.mobile.productivity.phonelocker.data.DataManager;
import com.mkotlarz.mobile.productivity.phonelocker.data.DataManagerImpl;
import com.mkotlarz.mobile.productivity.phonelocker.interfaces.ICondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.CallsModel;
import com.mkotlarz.mobile.productivity.phonelocker.models.Contact;
import com.mkotlarz.mobile.productivity.phonelocker.models.SMSModel;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.CallTypeCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.ContactNameCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.DateCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.DayCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.HourCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.KeywordCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.NumberCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.PatternCondition;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.security.Key;

public class EventInfo extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();
        setContentView(R.layout.activity_event_info);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }

    private void setupActionBar() {
            getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.event_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            DataManager dm = new DataManagerImpl(getActivity().getApplicationContext());
            Bundle b = getActivity().getIntent().getExtras();
            int typ = b.getInt("typ");
            long event_id = b.getLong("event_id");
            View rootView = inflater.inflate(R.layout.fragment_event_info, container, false);
            if(typ == 1) {
                CallsModel callDetail = dm.findCallbyID(event_id);
                if(!callDetail.getAlias().equals(""))
                    getActivity().getActionBar().setTitle(callDetail.getAlias());
                else
                    getActivity().getActionBar().setTitle(callDetail.getPhoneNumber());
                generateView(rootView, callDetail);
            } else {
                SMSModel smsDetail = dm.findSMSbyID(event_id);
                if(!smsDetail.getAlias().equals(""))
                    getActivity().getActionBar().setTitle(smsDetail.getAlias());
                else
                    getActivity().getActionBar().setTitle(smsDetail.getPhoneNumber());
                generateView(rootView, smsDetail);
            }
            return rootView;
        }

        private void generateView(View rootView, SMSModel smsModel) {

            TextView matchedByTextView = (TextView) rootView.findViewById(R.id.matched_by);
            if (smsModel.getMatchedOn() != null) {
                ICondition matchCondition = parseCondition(smsModel.getMatchedOn());
                TextView messageText = (TextView) rootView.findViewById(R.id.buble_text_event_info);
                messageText.setText(smsModel.getSmsBody());
                messageText.setVisibility(View.VISIBLE);
                String matchedBYString = getString(R.string.matched_on_start) + " " + matchCondition.getClassTranslatedName(getActivity().getApplicationContext()) + " " + getString(R.string.condition)
                        + " " + getString(R.string.matched_with) + ": " + matchCondition.getMatchedOn();
                matchedByTextView.setText(matchedBYString);
            } else
                matchedByTextView.setText(getString(R.string.no_data));
        }

        private void generateView(View rootView, CallsModel callModel) {
            TextView matchedByTextView = (TextView) rootView.findViewById(R.id.matched_by);
            if(callModel.getMatchedOn() != null) {
                ICondition matchCondition = parseCondition(callModel.getMatchedOn());
                String matchedBYString = getString(R.string.matched_on_start) + " " + matchCondition.getClassTranslatedName(getActivity().getApplicationContext()) + " " + getString(R.string.condition)
                        + " " + getString(R.string.matched_with) + ": " + matchCondition.getMatchedOn();
                matchedByTextView.setText(matchedBYString);
            } else
                matchedByTextView.setText(getString(R.string.no_data));
        }

        private ICondition parseCondition(String json) {
            try {
                JSONObject jsonObject = new JSONObject(json);
                int classCode = jsonObject.getInt("ClassCode");
                String matched_on = jsonObject.getString("MatchedOn");
                switch (classCode) {
                    case 0:
                        CallTypeCondition callTypeCondition = new CallTypeCondition();
                        callTypeCondition.setMatchedOn(matched_on);
                        return callTypeCondition;
                    case 1:
                        NumberCondition numberCondition = new NumberCondition();
                        numberCondition.setMatchedOn(matched_on);
                        return numberCondition;
                    case 2:
                        DayCondition dayCondition = new DayCondition();
                        dayCondition.setMatchedOn(matched_on);
                        return dayCondition;
                    case 3:
                        DateCondition dateCondition = new DateCondition();
                        dateCondition.setMatchedOn(matched_on);
                        return dateCondition;
                    case 4:
                        ContactNameCondition contactNameCondition = new ContactNameCondition();
                        contactNameCondition.setMatchedOn(matched_on);
                        return contactNameCondition;
                    case 5:
                        HourCondition hourCondition = new HourCondition();
                        hourCondition.setMatchedOn(matched_on);
                        return hourCondition;
                    case 6:
                        KeywordCondition keywordCondition = new KeywordCondition();
                        keywordCondition.setMatchedOn(matched_on);
                        return keywordCondition;
                    case 7:
                        PatternCondition patternCondition = new PatternCondition();
                        patternCondition.setMatchedOn(matched_on);
                        return patternCondition;
                }
                return null;
            } catch (JSONException ex) {
                ex.printStackTrace();
                return null;
            }
        }
    }

}
