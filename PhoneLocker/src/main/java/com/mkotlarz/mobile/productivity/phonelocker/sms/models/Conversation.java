package com.mkotlarz.mobile.productivity.phonelocker.sms.models;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Telephony;
import android.telephony.TelephonyManager;

import com.mkotlarz.mobile.productivity.phonelocker.data.Constants;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sdavies on 09/01/2014.
 */
public class Conversation {

    private String sender;
    private String contactName;
    private long threadId;
    private Context context;
    private String PhotoUri;
    private int m_read = 1;
    private List<Message> m_messageList = null;

    public Conversation(String sender, String contactName, String photoUri, long threadId, Context context) {
        this.sender = sender;
        this.threadId = threadId;
        this.context = context;
        this.contactName = contactName;
        this.PhotoUri = photoUri;
    }

    public List<Message> getMessages() {
        m_messageList = new ArrayList<Message>();
        String where = "thread_id=" + threadId;
        Cursor mycursor = context.getContentResolver().query(Uri.parse(Constants.SMS_URI), null, where ,null,Constants.SMS_DATE + " ASC");
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if(mycursor.moveToFirst()){
            for(int i=0;i<mycursor.getCount();i++){
                int id = mycursor.getInt(mycursor.getColumnIndex(Telephony.Sms._ID));
                String content = mycursor.getString(mycursor.getColumnIndex(Constants.SMS_BODY));
                String sender = mycursor.getString(mycursor.getColumnIndex(Constants.SMS_ADDRESS));
                Date time = new Date(mycursor.getLong(mycursor.getColumnIndex(Constants.SMS_DATE)));
                int messageType = mycursor.getInt(mycursor.getColumnIndex(Constants.SMS_TYPE));
                int read = mycursor.getInt(mycursor.getColumnIndex(Constants.SMS_READ));
                if(read == 0)
                    m_read = 0;
                long threadId = mycursor.getLong(mycursor.getColumnIndex(Telephony.Sms.THREAD_ID));
                String recipient = "recipient";
                String contactName = DataProvider.getInstance().getContactName(sender, context);
                if(contactName.isEmpty())
                    sender = "ja_contact";
                if(sender.equals(manager.getLine1Number()))
                    sender = "ja";
                m_messageList.add(new Message(id, content, sender, contactName, recipient, time, messageType, read, threadId));
                mycursor.moveToNext();
            }
        }
        mycursor.close();
        return m_messageList;
    }

    public List<Message> getMessageList() {
        return m_messageList;
    }

    public String getSender() {
        return sender;
    }

    public void addMessage(Message message) {
        if(m_messageList != null)
            m_messageList.add(message);
        else {
            m_messageList = new ArrayList<Message>();
            m_messageList.add(message);
        }
        if(!message.isReaded())
            m_read = 0;
    }

    public String getPhotoUri() {
        return PhotoUri;
    }

    public long getThreadId() { return  threadId; }

    public boolean CheckIfRead() {
        return m_read == 1;
    }

    public String getLastMessageBodyShortcut() {
        String body = m_messageList.get(m_messageList.size()-1).getContent();
        if(body.length() > 60)
            return body.substring(0, 60);
        return body;
    }

    @Override
    public String toString() {
        if(!contactName.isEmpty())
            return contactName;
        return sender;
    }
}