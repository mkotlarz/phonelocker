package com.mkotlarz.mobile.productivity.phonelocker.models;


public class SettingsModel extends ModelBase {
	
	private int INCOMING_CALLS_ENABLE;
	private int OUTGOING_CALLS_ENABLE;
	private int INCOMING_SMS_ENABLE;
    private int WIFI_ENABLE;
    private int NETWORK_ENABLE;

	   public SettingsModel(int icalls, int ocalls, int isms, int wifi, int net) {
		      this.INCOMING_CALLS_ENABLE = 	icalls;
		      this.OUTGOING_CALLS_ENABLE = 	ocalls;
		      this.INCOMING_SMS_ENABLE =	isms;
              this.WIFI_ENABLE =            wifi;
              this.NETWORK_ENABLE =         net;
		   }

       public SettingsModel() {

       }
	   
	   public int getIncomingCallsState() {
		   return this.INCOMING_CALLS_ENABLE;
	   }
	   
	   public int getOutgoingCallsState() {
		   return this.OUTGOING_CALLS_ENABLE;
	   }
	   
	   public int getIncimongSmsState() {
		   return this.INCOMING_SMS_ENABLE;
	   }

       public int getWifiState() { return this.WIFI_ENABLE; }

       public int getNetworkState() { return this.NETWORK_ENABLE; }
	   @Override
	   public long getId() {
		   return 0;
	   }
}
