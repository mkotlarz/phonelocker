package com.mkotlarz.mobile.productivity.phonelocker.junit_tests;

import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.mkotlarz.mobile.productivity.phonelocker.ConditionAddActivity;
import com.mkotlarz.mobile.productivity.phonelocker.R;

/**
 * Created by Maciej Kotlarz on 2014-11-05.
 */
public class CreateConditionActivityTest extends ActivityInstrumentationTestCase2<ConditionAddActivity> {
    private ConditionAddActivity m_conditionAddActivity;
    private Spinner m_spinner;
    private Button m_saveButton;

    public CreateConditionActivityTest() {
        super(ConditionAddActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Context ctx = getInstrumentation().getTargetContext();
        Intent intent = new Intent(ctx,ConditionAddActivity.class);
        Bundle extras = new Bundle();
        extras.putString("action", "sms");
        intent.putExtras(extras);
        setActivityIntent(intent);
        m_conditionAddActivity = getActivity();
        m_spinner = (Spinner) m_conditionAddActivity.findViewById(R.id.conditionTypeSpinner);
        m_saveButton = (Button) m_conditionAddActivity.findViewById(99);
    }

    public void testConditionTypeSpinner() {
        assertTrue(true);
    }
}
