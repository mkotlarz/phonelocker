package com.mkotlarz.mobile.productivity.phonelocker.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.provider.BaseColumns;

import com.mkotlarz.mobile.productivity.phonelocker.data.SmsRulesTable.SmsRulesColumns;
import com.mkotlarz.mobile.productivity.phonelocker.models.SmsRulesModel;

public class SmsRulesDao {

    private static final String INSERT =
            "insert into " + SmsRulesTable.TABLE_NAME + "("
                    + SmsRulesColumns.RULENAME + ", "
                    + SmsRulesColumns.DATE + ", "
                    + SmsRulesColumns.RULECONDITIONS + ", "
                    + SmsRulesColumns.RULEACTIVE + ") values (?, ?, ?, ?)";

    private SQLiteDatabase db;
    private SQLiteStatement insertStatement;

    public SmsRulesDao(SQLiteDatabase db) {
        this.db = db;
        insertStatement = db.compileStatement(SmsRulesDao.INSERT);
    }
    public long save(SmsRulesModel cRulesModel) {
        insertStatement.clearBindings();
        insertStatement.bindString(1, cRulesModel.getRuleName());
        insertStatement.bindString(2, cRulesModel.getRuleDate());
        insertStatement.bindString(3, cRulesModel.getRuleConditions());
        insertStatement.bindLong(4, cRulesModel.getRuleActive());
        return insertStatement.executeInsert();
    }

    public int update(SmsRulesModel cModel) {
        final ContentValues values = new ContentValues();
        values.put("rule_name" , cModel.getRuleName());
        values.put("rule_conditions", cModel.getRuleConditions());
        values.put("active", cModel.getRuleActive());
        db.update(SmsRulesTable.TABLE_NAME, values, BaseColumns._ID + " = ?", new String[] { String.valueOf(cModel.getId()) });
        return 1;
    }

    public void deleteSmsRule(SmsRulesModel sModel) {
        db.delete(SmsRulesTable.TABLE_NAME, BaseColumns._ID + " = ?", new String[] { String.valueOf(sModel.getId())});
    }

    public SmsRulesModel getId(long call_id) {
        SmsRulesModel call = null;
        Cursor c =
                db.query(SmsRulesTable.TABLE_NAME, new String[] { BaseColumns._ID, SmsRulesColumns.RULENAME},
                        BaseColumns._ID + " = ?", new String[] { String.valueOf(call_id) }, null, null, null, "1");
        if (c.moveToFirst()) {
            call = this.buildCallFromCursor(c);
        }
        if (!c.isClosed()) {
            c.close();
        }
        return call;
    }
    public String[][] getAllRules() {

        Cursor callsCursor = db.rawQuery("select * from " + SmsRulesTable.TABLE_NAME + " ORDER BY " + SmsRulesColumns._ID +" ASC", null);
        String[][] result = new String[callsCursor.getCount()][5];
        callsCursor.moveToFirst();
        for(int i = 0; i < callsCursor.getCount(); i++){
            String ruleName = callsCursor.getString(callsCursor.getColumnIndex(SmsRulesColumns.RULENAME));
            String date = callsCursor.getString(callsCursor.getColumnIndex(SmsRulesColumns.DATE));
            String ruleConditions = callsCursor.getString(callsCursor.getColumnIndex(SmsRulesColumns.RULECONDITIONS));
            String active = callsCursor.getString(callsCursor.getColumnIndex(SmsRulesColumns.RULEACTIVE));
            //You can here manipulate a single string as you please
            result[i][0] = active;
            result[i][1] = ruleName;
            result[i][2] = ruleConditions;
            result[i][3] = date;
            callsCursor.moveToNext();
        }
        callsCursor.close();
        return result;
    }

    public String[][] getAllActiveRules() {

        Cursor callsCursor = db.rawQuery("select * from " + SmsRulesTable.TABLE_NAME + " WHERE " + SmsRulesColumns.RULEACTIVE + " = '1' ORDER BY " + SmsRulesColumns._ID +" ASC", null);
        String[][] result = new String[callsCursor.getCount()][5];
        callsCursor.moveToFirst();
        for(int i = 0; i < callsCursor.getCount(); i++){
            String ruleName = callsCursor.getString(callsCursor.getColumnIndex(SmsRulesColumns.RULENAME));
            String date = callsCursor.getString(callsCursor.getColumnIndex(SmsRulesColumns.DATE));
            String ruleConditions = callsCursor.getString(callsCursor.getColumnIndex(SmsRulesColumns.RULECONDITIONS));
            String active = callsCursor.getString(callsCursor.getColumnIndex(SmsRulesColumns.RULEACTIVE));
            //You can here manipulate a single string as you please
            result[i][0] = active;
            result[i][1] = ruleName;
            result[i][2] = ruleConditions;
            result[i][3] = date;
            callsCursor.moveToNext();
        }
        callsCursor.close();
        return result;
    }

    public String dumpToFile() {
        String sql = SmsRulesTable.CreateTableSql() + System.getProperty("line.separator");
        Cursor smsCursor = db.rawQuery("select * from " + SmsRulesTable.TABLE_NAME, null);
        smsCursor.moveToFirst();
        for (int i = 0; i < smsCursor.getCount(); i++) {
            sql += INSERT.replace("(?, ?, ?, ?)", "");
            sql += "('" + smsCursor.getString(smsCursor.getColumnIndex(SmsRulesColumns.RULENAME)) + "', '" + smsCursor.getString(smsCursor.getColumnIndex(SmsRulesColumns.DATE))
                    + "', " + smsCursor.getString(smsCursor.getColumnIndex(SmsRulesColumns.RULECONDITIONS)) + "', '" + smsCursor.getString(smsCursor.getColumnIndex(SmsRulesColumns.RULEACTIVE)) + "');" + System.getProperty("line.separator");
            smsCursor.moveToNext();
        }
        smsCursor.close();
        return sql;
    }

    private SmsRulesModel buildCallFromCursor(Cursor c) {
        SmsRulesModel call = null;
        if (c != null) {
            call = new SmsRulesModel();
            call.setId(c.getLong(0));
        }
        return call;

    }
}
