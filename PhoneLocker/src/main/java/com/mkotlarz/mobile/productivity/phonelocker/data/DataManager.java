package com.mkotlarz.mobile.productivity.phonelocker.data;

import com.mkotlarz.mobile.productivity.phonelocker.models.CallRulesModel;
import com.mkotlarz.mobile.productivity.phonelocker.models.CallsModel;
import com.mkotlarz.mobile.productivity.phonelocker.models.SMSModel;
import com.mkotlarz.mobile.productivity.phonelocker.models.SettingsModel;
import com.mkotlarz.mobile.productivity.phonelocker.models.SmsRulesModel;

import java.util.List;
import java.util.Date;

/**
 * Android DataManager interface used to define data operations.
 * 
 * @author mkotlarz
 *
 */
public interface DataManager {  
   
   // Settings
	

   public SettingsModel getSettings();

   public int getAllSettingsState();

   public void setAllToValue(int value);

   public List<String> getSettingsHeaders();

   public int getIncomingCallsState();

   public int getOutgoingCallsState();

   public int getIncomingSmsState();

   public int findSettingsByName(String name);

   public void saveSelected(String column_name, int state);

   /* public long saveSettings(SettingsModel sModel);

   public boolean deleteSettings(String name);

   // optional -- used for CursorAdapter
   public Cursor getSettingsCursor(); */

   
   
   //Calls methods
   
   //public CallsModel getCallsState();

   public String[][] getAllCalls();

   public List<CallsModel> findCallsbyNumber(long number);
   
   //public List<CallsModel> findCallsbyDate(Date date);
   
   public CallsModel findCallbyID(long id);

   public long saveCall(CallsModel cModel);

   //public boolean deleteCall(CallsModel cModel);
   
   public boolean deleteCall(long id);
   
   
   
   
   //SMS
   
   //public SMSModel getSmsState();

   public String[][] getAllSMS();

   public List<SMSModel> findSMSbyNumber(long number);
   
   //public List<SMSModel> findSMSbyBody(String body);
   
   //public List<SMSModel> fingSMSbyDate(Date date);
   
   public SMSModel findSMSbyID(long id);

   public long saveSMS(SMSModel sModel);

   //public void deleteSMS(SMSModel sModel);
   
   public boolean deleteSMS(long id);



    //Call Rules

    public String[][] getAllCallRules();

    public String[][] getAllActiveCallRules();

    public CallRulesModel findCallRuleByPosition(long id);

    public CallRulesModel saveCallRule(String ruleName, Date date, String ruleConditions, boolean active);

    public CallRulesModel editCallRule(long id, String ruleName, String ruleConditions, boolean active);

    public boolean deleteCallRule(CallRulesModel cModel);

    //Sms Rules

    public String[][] getAllSmsRules();

    public String[][] getAllActiveSmsRules();

    public SmsRulesModel findSmsRuleByPosition(long id);

    public SmsRulesModel saveSmsRule(String ruleName, Date date, String ruleConditions, boolean active);

    public SmsRulesModel editSmsRule(long id, String ruleName, String ruleConditions, boolean active);
}
