package com.mkotlarz.mobile.productivity.phonelocker.data;

import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public final class CallsTable {
	
	public static final String TABLE_NAME = "calls";

	   public static class CallsColumns implements BaseColumns {
		      public static final String PHONE_NUMBER = "phone_number";
		      public static final String TYP = "typ";
		      public static final String DATE = "date";
              public static final String ALIAS = "alias";
              public static final String MATCHED_ON = "matched_on";
	   }

	   public static void onCreate(SQLiteDatabase db) {
	      StringBuilder sb = new StringBuilder();

	      // movie table
	      sb.append("CREATE TABLE " + CallsTable.TABLE_NAME + " (");
	      sb.append(BaseColumns._ID + " INTEGER PRIMARY KEY, ");
	      sb.append(CallsColumns.PHONE_NUMBER + " INTEGER, ");
	      sb.append(CallsColumns.TYP + " INTEGER NOT NULL, "); // movie names aren't unique, but for simplification we constrain
	      sb.append(CallsColumns.DATE + " TEXT, ");
          sb.append(CallsColumns.ALIAS + " TEXT, ");
           sb.append(CallsColumns.MATCHED_ON + " TEXT");
	      sb.append(");");
	      db.execSQL(sb.toString());
	   }

	   public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
           if(newVersion == 2) {
               db.execSQL("ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + CallsColumns.MATCHED_ON + " Text;");
           }
           if(newVersion == 1) {
               db.execSQL("DROP TABLE IF EXISTS " + CallsTable.TABLE_NAME);
               CallsTable.onCreate(db);
           }
	   }

}
