package com.mkotlarz.mobile.productivity.phonelocker.data;

import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public final class SmsTable {
	
	public static final String TABLE_NAME = "sms";

	   public static class SmsColumns implements BaseColumns {
		      public static final String PHONE_NUMBER = "phone_number";
		      public static final String BODY = "body";
		      public static final String typ = "typ";
		      public static final String DATE = "date";
              public static final String ALIAS = "alias";
              public static final String MATCHED_ON = "matched_on";
	   }

	   public static void onCreate(SQLiteDatabase db) {
	      StringBuilder sb = new StringBuilder();

	      // movie table
	      sb.append("CREATE TABLE " + SmsTable.TABLE_NAME + " (");
	      sb.append(BaseColumns._ID + " INTEGER PRIMARY KEY, ");
	      sb.append(SmsColumns.PHONE_NUMBER + " INTEGER, ");
	      sb.append(SmsColumns.BODY + " TEXT, ");
	      sb.append(SmsColumns.typ + " INTEGER, "); // movie names aren't unique, but for simplification we constrain
	      sb.append(SmsColumns.DATE + " TEXT, ");
          sb.append(SmsColumns.ALIAS + " TEXT, ");
          sb.append(SmsColumns.MATCHED_ON + " Text");
	      sb.append(");");
	      db.execSQL(sb.toString());
	   }

	   public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
           if(newVersion == 2) {
               db.execSQL("ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + SmsColumns.MATCHED_ON + " Text;");
           }
           if(newVersion == 1) {
               db.execSQL("DROP TABLE IF EXISTS " + SmsTable.TABLE_NAME);
               SmsTable.onCreate(db);
           }
	   }

}
