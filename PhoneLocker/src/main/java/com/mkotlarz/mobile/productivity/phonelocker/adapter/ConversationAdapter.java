package com.mkotlarz.mobile.productivity.phonelocker.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mkotlarz.mobile.productivity.phonelocker.R;
import com.mkotlarz.mobile.productivity.phonelocker.data.DataManagerImpl;
import com.mkotlarz.mobile.productivity.phonelocker.sms.models.Conversation;

import java.util.List;

/**
 * Created by Maciej on 2014-06-30.
 */
public class ConversationAdapter extends BaseAdapter {

    /**
     * The inflator used to inflate the XML layout
     */
    private LayoutInflater inflator;

    /**
     * A list containing some sample data to show.
     */
    private List<Conversation> dataList;
    private Context m_context = null;
    private DataManagerImpl m_dataManager = null;

    public ConversationAdapter(LayoutInflater inflator, List<Conversation> convesationsList, Context context) {
        super();
        this.inflator = inflator;

        dataList = convesationsList;
        m_context = context;
        m_dataManager = new DataManagerImpl(m_context);

    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Conversation getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Conversation conversation = getItem(position);
        if (convertView == null) {
            convertView = inflator.inflate(R.layout.conversation_list_item, parent, false);
            convertView.setTag(R.id.conversation_contact_thumb, convertView.findViewById(R.id.conversation_contact_thumb));
            convertView.setTag(R.id.conversation_contact_name, convertView.findViewById(R.id.conversation_contact_name));
        }
            ImageView imageView = (ImageView) convertView.findViewById(R.id.conversation_contact_thumb);
            if (conversation.getPhotoUri() == null)
                imageView.setImageResource(R.drawable.anonim);
            else
                imageView.setImageURI(Uri.parse(conversation.getPhotoUri()));

            TextView textView = (TextView) convertView.findViewById(R.id.conversation_contact_name);
            textView.setText(conversation.toString());

            TextView bodyView = (TextView) convertView.findViewById(R.id.conversation_message_body);
            bodyView.setText(conversation.getLastMessageBodyShortcut());

        if (!conversation.CheckIfRead())
            convertView.setBackgroundResource(R.color.unread_message);
        else
            convertView.setBackgroundResource(android.R.color.transparent);

        return convertView;
    }

    public void refreshAdapterValues(List<Conversation> conversationList) {
        dataList = conversationList;
        notifyDataSetChanged();
    }
}