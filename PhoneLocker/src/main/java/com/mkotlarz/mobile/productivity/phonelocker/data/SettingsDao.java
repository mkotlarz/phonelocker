package com.mkotlarz.mobile.productivity.phonelocker.data;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.provider.BaseColumns;

import com.mkotlarz.mobile.productivity.phonelocker.data.SettingsTable.SettingsColumns;
import com.mkotlarz.mobile.productivity.phonelocker.models.SettingsModel;

import org.apache.log4j.Logger;

public class SettingsDao {

   private static final String INSERT =
            "insert into " + SettingsTable.TABLE_NAME + "("
            + SettingsColumns.INCOMING_CALLS + ", "
            + SettingsColumns.OUTGOING_CALLS + ", "
            + SettingsColumns.INCOMING_SMS   + ", "
            + SettingsColumns.WIFI           + ", "
            + SettingsColumns.NETWORK        + ") values (?, ?, ?, ?, ?)";

   private SQLiteDatabase db;
   private SQLiteStatement insertStatement;
    private final Logger Log = Logger.getLogger(SettingsDao.class);

   public SettingsDao(SQLiteDatabase db) {
      this.db = db;
      insertStatement = db.compileStatement(SettingsDao.INSERT);
   }
   public long save(SettingsModel stModel) {
	   insertStatement.clearBindings();
	   insertStatement.bindLong(1, stModel.getIncomingCallsState());
	   insertStatement.bindLong(2, stModel.getOutgoingCallsState());
	   insertStatement.bindLong(3, stModel.getIncimongSmsState());
       insertStatement.bindLong(4, stModel.getWifiState());
       insertStatement.bindLong(5, stModel.getNetworkState());
	   return insertStatement.executeInsert();
   }

   public int changeAllToValue(int value) {
       final ContentValues values = new ContentValues();
       values.put(SettingsColumns.INCOMING_CALLS, value);
       values.put(SettingsColumns.OUTGOING_CALLS, value);
       values.put(SettingsColumns.INCOMING_SMS,   value);
       values.put(SettingsColumns.WIFI,           value);
       values.put(SettingsColumns.NETWORK,        value);
       try {
           db.update(SettingsTable.TABLE_NAME, values, BaseColumns._ID + " = 1 ", null);
           return 1;
       } catch (Exception ex) {
           Log.error("Error when updating database for selected column " + ex.getMessage() + "..." + ex.getStackTrace());
           return 0;
       }
   }
   public int saveSelected(String column_name, int state) {
       final ContentValues values = new ContentValues();
       values.put(column_name, state);
       try {
           db.update(SettingsTable.TABLE_NAME, values, BaseColumns._ID + " = 1 ", null);
           return 1;
       } catch (Exception ex) {
           Log.error("Error when updating database for selected column " + ex.getMessage() + "..." + ex.getStackTrace());
           return 0;
       }
   }
   
   public void update(SettingsModel sModel) {
	   final ContentValues values = new ContentValues();
	      values.put("INCOMING_CALLS" , sModel.getIncomingCallsState());
	      values.put("OUTGOING_CALLS" , sModel.getIncomingCallsState());
	      values.put("INCOMING_SMS" , sModel.getIncimongSmsState());
          values.put("WIFI", sModel.getWifiState());
          values.put("NETWORK", sModel.getNetworkState());
	      db.update(SettingsTable.TABLE_NAME, values, BaseColumns._ID + " = ?", new String[] { String.valueOf(sModel.getId()) });
	   }

    public long initialize () {
        insertStatement.clearBindings();
        insertStatement.bindLong(1, 0);
        insertStatement.bindLong(2, 0);
        insertStatement.bindLong(3, 0);
        insertStatement.bindLong(4, 0);
        insertStatement.bindLong(5, 0);
        return insertStatement.executeInsert();
    }
}

