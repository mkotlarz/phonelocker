package com.mkotlarz.mobile.productivity.phonelocker.models.conditions;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mkotlarz.mobile.productivity.phonelocker.R;
import com.mkotlarz.mobile.productivity.phonelocker.interfaces.ICondition;

import org.apache.log4j.Logger;

import java.io.Serializable;

/**
 * Created by Maciej on 2014-06-29.
 */
public class ContactNameCondition implements Serializable, ICondition {

    @Expose
    @SerializedName("contactName")
    private String m_contactName = null;
    private String m_matched_on = null;

    public final static String JSON_NAME = "ContactNameCondition";

    public String getMatchedOn() { return m_matched_on; }

    public void setMatchedOn(String matched_on) { m_matched_on = matched_on; }

    public String getContactName() {
        return m_contactName;
    }

    public void setContactName(String m_contactName) {
        this.m_contactName = m_contactName;
    }

    public String getJSONName() {
        return "ContactNameCondition";
    }

    public Class<?> getConditionClass() {
        return ContactNameCondition.class;
    }

    public int getClassCode() {
        return 4;
    }

    public boolean Match(String contactName) {
        final Logger Log = Logger.getLogger(CallTypeCondition.class);
        Log.info("Matching if: " + contactName + " equals " + m_contactName);
        if(contactName.toLowerCase().contains(m_contactName.toLowerCase())) {
            Log.info("Result: true");
            m_matched_on = m_contactName;
            return true;
        }
        Log.info("Result: false");
        return false;
    }

    public String getClassTranslatedName(Context ctx) {
        return ctx.getString(R.string.contact_name_condition);
    }
}
