package com.mkotlarz.mobile.productivity.phonelocker;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.getbase.floatingactionbutton.AddFloatingActionButton;
import com.mkotlarz.mobile.productivity.phonelocker.R;
import com.mkotlarz.mobile.productivity.phonelocker.adapter.RulesAdapter;
import com.mkotlarz.mobile.productivity.phonelocker.data.DataManagerImpl;
import com.mkotlarz.mobile.productivity.phonelocker.sms.ComposeSmsActivity;

/**
     * A placeholder fragment containing a simple view.
     */
    public class CallRulesFragment extends Fragment {

        private DataManagerImpl dm = null;
        private String[][] m_rules = null;
        private RulesAdapter m_rulesAdapter = null;
        private ListView m_listView = null;

        public CallRulesFragment() {
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setHasOptionsMenu(true);
            setMenuVisibility(true);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            dm = new DataManagerImpl(getActivity().getApplicationContext());
            m_rules = dm.getAllCallRules();
            View rootView = inflater.inflate(R.layout.fragment_call_rules, container, false);
            m_rulesAdapter = new RulesAdapter(getActivity().getLayoutInflater(), m_rules, getActivity(), "call");
            m_listView = (ListView) rootView.findViewById(R.id.callRulesListView);
            m_listView.setAdapter(m_rulesAdapter);
            AddFloatingActionButton mFab = (AddFloatingActionButton) rootView.findViewById(R.id.AddRuleFloatingButton);
            mFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity().getApplicationContext(), CreateRuleActivity.class);
                    Bundle extras = new Bundle();
                    extras.putString("action", "call");
                    intent.putExtras(extras);
                    startActivity(intent);
                }
            });
            return rootView;
        }

        @Override
        public void onResume() {
            super.onResume();
            if(m_rules != null && m_listView != null) {
                m_rules = dm.getAllCallRules();
                m_rulesAdapter.refreshAdapterValues(m_rules);
            }
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            // Inflate the menu; this adds items to the action bar if it is present.
            inflater.inflate(R.menu.call_rules, menu);
            super.onCreateOptionsMenu(menu,inflater);
        }
    }
