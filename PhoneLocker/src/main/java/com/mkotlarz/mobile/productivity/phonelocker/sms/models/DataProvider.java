package com.mkotlarz.mobile.productivity.phonelocker.sms.models;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.telephony.TelephonyManager;

import com.mkotlarz.mobile.productivity.phonelocker.data.Constants;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

/**
 * Created by sdavies on 09/01/2014.
 */
public class DataProvider extends Observable {
    private static DataProvider ourInstance = new DataProvider();

    public static DataProvider getInstance() {
        return ourInstance;
    }

    private Map<String, Conversation> conversationMap;
    private List<Conversation> conversationList;
    private final Logger Log = Logger.getLogger(DataProvider.class);

    private DataProvider() {
        // Create the store
        conversationMap = new HashMap<String, Conversation>();
        conversationList = new ArrayList<Conversation>();
    }

    public void updateMessagesList(Context context) {
        conversationList.clear();
        conversationMap.clear();
        Cursor cursor = context.getContentResolver().query(Uri.parse(Constants.CONVERSATIONS_URI), null, null, null, Telephony.Sms.Conversations.DATE + " DESC");
        if(cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(cursor.getColumnIndex(Telephony.Sms.Conversations._ID));
                String content = cursor.getString(cursor.getColumnIndex(Telephony.Sms.Conversations.SNIPPET));
                int read = cursor.getInt(cursor.getColumnIndex(Telephony.Sms.Conversations.READ));
                long recipients = cursor.getLong(cursor.getColumnIndex("recipient_ids"));
                Cursor rec_cursor = context.getContentResolver().query(Uri.parse("content://mms-sms/canonical-address/" + recipients), null, null, null, null);
                rec_cursor.moveToFirst();
                String sender = rec_cursor.getString(0);
                String contactName = getContactName(sender, context);
                String recipient = "recipient";
                int messageType = cursor.getInt(cursor.getColumnIndex(Constants.SMS_TYPE));
                Date time = new Date(cursor.getLong(cursor.getColumnIndex(Telephony.Sms.Conversations.DATE)));
                addMessageSilent(new Message(id, content, sender, contactName, recipient, time, messageType, read, recipients), context);
            } while (cursor.moveToNext());
        }
        cursor.close();
    }

    public void addMessage(Message message, Context context) {
        if(conversationMap.containsKey(message.getSender())) {
            // Can add the message to an existing conversation
            conversationMap.get(message.getSender()).addMessage(message);
        } else {
            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(message.getSender()));
            ContentResolver contentResolver = context.getContentResolver();
            String thumbnailUri = null;
            Cursor thumbnailLookup = contentResolver.query(uri, new String[]{BaseColumns._ID,
                        ContactsContract.PhoneLookup.PHOTO_URI}, null, null, null);
            if(thumbnailLookup.moveToFirst())
                thumbnailUri = thumbnailLookup.getString(thumbnailLookup.getColumnIndex(ContactsContract.PhoneLookup.PHOTO_URI));
            thumbnailLookup.close();
            Conversation conversation = new Conversation(message.getSender(), message.getContactName(), thumbnailUri, message.getThreadId(), context);
            conversation.addMessage(message);
            conversationMap.put(message.getSender(), conversation);
            conversationList.add(conversation);
        }
        // Ensure that everything gets updated
        setChanged();
        notifyObservers();
    }

    public void addMessageSilent(Message message, Context context) {
        if(conversationMap.containsKey(message.getSender())) {
            // Can add the message to an existing conversation
            conversationMap.get(message.getSender()).addMessage(message);
        } else {
            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(message.getSender()));
            ContentResolver contentResolver = context.getContentResolver();
            String thumbnailUri = null;
            Cursor thumbnailLookup = contentResolver.query(uri, new String[]{BaseColumns._ID,
                    ContactsContract.PhoneLookup.PHOTO_URI}, null, null, null);
            if(thumbnailLookup.moveToFirst())
                thumbnailUri = thumbnailLookup.getString(thumbnailLookup.getColumnIndex(ContactsContract.PhoneLookup.PHOTO_URI));
            thumbnailLookup.close();
            Conversation conversation = new Conversation(message.getSender(), message.getContactName(), thumbnailUri, message.getThreadId(), context);
            conversation.addMessage(message);
            conversationMap.put(message.getSender(), conversation);
            conversationList.add(conversation);
        }
    }

    public List<Conversation> getConversations() {
        return conversationList;
    }

    public Map<String, Conversation> getConversationMap() {
        return conversationMap;
    }

    public String getContactName(String address, Context context) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(address));
        ContentResolver contentResolver = context.getContentResolver();
        String contactName = "";
        try {
            Cursor contactLookup = contentResolver.query(uri, new String[] {BaseColumns._ID,
                    ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);
            if(contactLookup.moveToFirst()) {
                contactName = contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
                contactLookup.close();
            } else {
                TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(manager.getNetworkCountryIso() + address));
                contactLookup = contentResolver.query(uri, new String[] {BaseColumns._ID,
                        ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);
                if(contactLookup.moveToFirst()) {
                    contactName = contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
                    contactLookup.close();
                }
            }
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getStackTrace());
        }
        return contactName;
    }
}