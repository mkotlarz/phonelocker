package com.mkotlarz.mobile.productivity.phonelocker.models;

import java.util.Date;

public class CallsModel extends ModelBase {
	
	
	private long id;
	private String phoneNumber;
	private String date;
	private int typ;
    private String alias;
    private String matchedOn;
	
	//Constructor for save
	public CallsModel(String phoneNumber, Date date, int typ, String alias, String matchedOn) {
	      this.phoneNumber			= 	phoneNumber;
	      this.date					= 	date.toString();
	      this.typ 					=	typ;
          this.alias                =   alias;
          this.matchedOn            =   matchedOn;
	   }
	//Constructor for read
	public CallsModel(String phoneNumber, Date date, int typ, long id, String matchedOn) {
		  this.id 					=	id;
	      this.phoneNumber			= 	phoneNumber;
	      this.date					= 	date.toString();
	      this.typ 					=	typ;
          this.matchedOn            =   matchedOn;
	   }

    public CallsModel(String phoneNumber, String date, int typ, String alias, String matchedOn) {
        this.phoneNumber			= 	phoneNumber;
        this.date					= 	date.toString();
        this.typ 					=	typ;
        this.alias                  =   alias;
        this.matchedOn              =   matchedOn;
    }
	//Universal constructor
	public CallsModel() {
		//test
	}
	//Constructor for findById
	public CallsModel(long id) {
		this.id = id;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}
	
	public String getCallDate() {
		return this.date;
	}
	
	public long getCallType() {
		return this.typ;
	}
	
	public void setPhoneNumber(String number) {
		this.phoneNumber = number;
	}

    public String getAlias() { return this.alias; }

    public void setAlias(String alias) { this.alias = alias; }
	
	public void setDate(Date date) {
		this.date = date.toString();
	}
	
	public void setType(int typ) {
		this.typ = typ;
	}

    public String getMatchedOn() {  return matchedOn;   }

    public void setMatchedOn(String matchedOn) {    this.matchedOn = matchedOn; }
}
