package com.mkotlarz.mobile.productivity.phonelocker.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.provider.BaseColumns;

import com.mkotlarz.mobile.productivity.phonelocker.data.CallsTable.CallsColumns;
import com.mkotlarz.mobile.productivity.phonelocker.models.CallsModel;

import java.util.ArrayList;
import java.util.List;

public class CallsDao {

   private static final String INSERT =
            "insert into " + CallsTable.TABLE_NAME + "("
            + CallsColumns.PHONE_NUMBER + ", "
            + CallsColumns.TYP + ", "
            + CallsColumns.DATE + ", "
            + CallsColumns.ALIAS + ", "
            + CallsColumns.MATCHED_ON + ") values (?, ? , ?, ?, ?)";

   private SQLiteDatabase db;
   private SQLiteStatement insertStatement;

   public CallsDao(SQLiteDatabase db) {
      this.db = db;
      insertStatement = db.compileStatement(CallsDao.INSERT);
   }
   public long save(CallsModel cModel) {
	   insertStatement.clearBindings();
	      insertStatement.bindString(1, cModel.getPhoneNumber());
	      insertStatement.bindLong(2, cModel.getCallType());
	      insertStatement.bindString(3, cModel.getCallDate().toString());
          insertStatement.bindString(4, cModel.getAlias());
          insertStatement.bindString(5, cModel.getMatchedOn());
	      return insertStatement.executeInsert();
   }
   
   public int update(CallsModel cModel) {
	   final ContentValues values = new ContentValues();
	      values.put("phone_number" , cModel.getPhoneNumber());
	      values.put("typ" , cModel.getCallType());
	      values.put("date" , cModel.getCallDate().toString());
          values.put("alias", cModel.getAlias());
          values.put("matched_on", cModel.getMatchedOn());
	      db.update(CallsTable.TABLE_NAME, values, BaseColumns._ID + " = ?", new String[] { String.valueOf(cModel.getId()) });
	      return 1;
   }
   
   public static void deleteCall(CallsModel call) {
	   
   }
   
   public CallsModel getId(long call_id) {
	   CallsModel call = null;
	      Cursor c =
	               db.query(CallsTable.TABLE_NAME, new String[] { BaseColumns._ID, CallsColumns.PHONE_NUMBER, 
	            		   CallsColumns.TYP , CallsColumns.DATE},
	                        BaseColumns._ID + " = ?", new String[] { String.valueOf(call_id) }, null, null, null, "1");
	      if (c.moveToFirst()) {
	         call = this.buildCallFromCursor(c);
	      }
	      if (!c.isClosed()) {
	         c.close();
	      }
	      return call;
   }
   public String[][] getAllCalls() {

       Cursor callsCursor = db.rawQuery("select * from " + CallsTable.TABLE_NAME + " ORDER BY " + CallsColumns._ID +" DESC", null);
       String[][] result = new String[callsCursor.getCount()][4];
       callsCursor.moveToFirst();
       for(int i = 0; i < callsCursor.getCount(); i++){
           String phoneNumber = callsCursor.getString(callsCursor.getColumnIndex(CallsColumns.PHONE_NUMBER));
           String date = callsCursor.getString(callsCursor.getColumnIndex(CallsColumns.DATE));
           String typ = callsCursor.getString(callsCursor.getColumnIndex(CallsColumns.TYP));
           String alias = callsCursor.getString(callsCursor.getColumnIndex(CallsColumns.ALIAS));
           //You can here manipulate a single string as you please
           result[i][0] = phoneNumber;
           result[i][1] = date;
           result[i][2] = typ;
           result[i][3] = alias;
           callsCursor.moveToNext();
       }
       callsCursor.close();
       return result;
   }
   
   public List<CallsModel> getCallsbyNumber(long number) {
	   List<CallsModel> list = new ArrayList<CallsModel>();
	   return list;
   }
   
   private CallsModel buildCallFromCursor(Cursor c) {
	   CallsModel call = null;
	   if (c != null) {
	         call = new CallsModel();
	         call.setId(c.getLong(0));
	      }
	      return call;
	   
   }
}
