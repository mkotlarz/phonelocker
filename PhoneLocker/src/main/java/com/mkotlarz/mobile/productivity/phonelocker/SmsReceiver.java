package com.mkotlarz.mobile.productivity.phonelocker;
 
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.telephony.SmsMessage;

import com.mkotlarz.mobile.productivity.phonelocker.data.DataManagerImpl;
import com.mkotlarz.mobile.productivity.phonelocker.interfaces.ICondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.SMSModel;

import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/*
    Zadaniem tej klasy jest przechwytywanie, zapis do bazy i zatrzymanie przetwarzania SMSow przez inne aplikacje
    @author maciej_kotlarz
 */
public class SmsReceiver {

    private DataManagerImpl dm;
    private final Logger Log = Logger.getLogger(SmsReceiver.class);

 //Przychodzace SMSy
    public void incomingSMS(Context context, Intent intent, ICondition matchedCondition) {
        Log.info("Initialize database");
        dm = new DataManagerImpl(context);
        Log.info("Start processing SMS message");
        long id;
        Bundle pudsBundle = intent.getExtras();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd-MM-yyyy", Locale.getDefault());
        String date = sdf.format(new Date());
        Object[] pdus = (Object[]) pudsBundle.get("pdus");
        SmsMessage messages = SmsMessage.createFromPdu((byte[]) pdus[0]);
        String number = messages.getOriginatingAddress();
        String body = messages.getMessageBody();
        id = 1;
        Log.info("SMS from: " + number);
        Log.info("SMS body: " + body);
        Log.info("SMS date: " + date);
        Log.info("SMS type: " + "incoming(1)");
        Log.info("SMS id: "   + id);

        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        ContentResolver contentResolver = context.getContentResolver();
        String contactName = "";
        //Get contact name by phone number
        try {
            Cursor contactLookup = contentResolver.query(uri, new String[] {BaseColumns._ID,
                    ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);
            if(contactLookup.moveToFirst()) {
                contactName = contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
            }
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getStackTrace());
        }

        //Save sms into database
        try {
            String matched_on_json = "{ \"ClassCode\": " + matchedCondition.getClassCode() + ", \"MatchedOn\": \"" + matchedCondition.getMatchedOn() + "\" }";
            SMSModel sms = new SMSModel(number, body, date, 1, contactName, matched_on_json);
            dm.saveSMS(sms);
        }
        catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getStackTrace());
        }
    }
}