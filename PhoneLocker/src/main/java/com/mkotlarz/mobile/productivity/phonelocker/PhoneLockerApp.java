package com.mkotlarz.mobile.productivity.phonelocker;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.preference.PreferenceManager;

import com.mkotlarz.mobile.productivity.phonelocker.data.DataManager;
import com.mkotlarz.mobile.productivity.phonelocker.data.DataManagerImpl;

public class PhoneLockerApp extends Application {

   private ConnectivityManager cMgr;
   private DataManager dataManager;
   private SharedPreferences prefs;

   //
   // getters/setters
   //   
   public SharedPreferences getPrefs() {
      return this.prefs;
   }

   public DataManager getDataManager() {
      return this.dataManager;
   }


   //
   // lifecycle
   //
   @Override
   public void onCreate() {
      super.onCreate();
      cMgr = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
      prefs = PreferenceManager.getDefaultSharedPreferences(this);
      dataManager = new DataManagerImpl(this);
   }

   @Override
   public void onTerminate() {
      // not guaranteed to be called
      super.onTerminate();
   }

   //
   // util/helpers for app
   //
   public boolean connectionPresent() {
      NetworkInfo netInfo = cMgr.getActiveNetworkInfo();
      if ((netInfo != null) && (netInfo.getState() != null)) {
         return netInfo.getState().equals(State.CONNECTED);
      }
      return false;
   }
}
