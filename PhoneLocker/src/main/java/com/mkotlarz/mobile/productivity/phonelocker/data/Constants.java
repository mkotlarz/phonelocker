package com.mkotlarz.mobile.productivity.phonelocker.data;

import android.content.Context;
import android.graphics.Color;
import android.os.Environment;
import android.provider.Telephony;

import java.io.File;
import java.util.logging.Logger;

import org.apache.log4j.Level;


import de.mindpipe.android.logging.log4j.LogConfigurator;


public final class Constants {

    public static final String SMS_ADDRESS = "address";
    public static final String SMS_DATE = "date";
    public static final String SMS_BODY = "body";
    public static final String SMS_TYPE = "type";
    public static final String SMS_READ = "read";
    public static final int SMS_INCOMING_TYPE_VALUE = Telephony.Sms.MESSAGE_TYPE_INBOX;
    public static final int SMS_OUTGOING_TYPE_VALUE = Telephony.Sms.MESSAGE_TYPE_SENT;
    public static final String SMS_URI = "content://sms";
    public static final String CONVERSATIONS_URI = "content://mms-sms/conversations?simple=true";
    public static final String SMS_SENT_URI = "content://sms/sent";
    public static final long[] VIBRATE_PATTERN = new long[] { 500, 250, 500, 250, 500};
    public static final int    LIGHT_COLOR = Color.BLUE;
    public static final int    LIGHT_ON = 1000;
    public static final int    LIGHT_OFF = 800;
    public static final String LOG_FILE_NAME = "phonelocker_log.txt";
    public static final String CALL_CONDITIONS_DUMP_FILE_NAME = "call_conditions_dump_file.txt";
    public static final String SMS_CONDITIONS_DUMP_FILE_NAME = "SMS_conditions_dump_file.txt";

	   private Constants() {
	   }


    public static String GetLogFileDir(Context context) {
        if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()))
            return Environment.getExternalStorageDirectory() + File.separator + LOG_FILE_NAME;
        return context.getFilesDir() + File.separator + LOG_FILE_NAME;
    }

    public static String GetCallConditionDumpFilePath(Context context) {
        if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()))
            return Environment.getExternalStorageDirectory() + File.separator + CALL_CONDITIONS_DUMP_FILE_NAME;
        return context.getFilesDir() + File.separator + CALL_CONDITIONS_DUMP_FILE_NAME;
    }

    public static String GetSmsConditionDumpFilePath(Context context) {
        if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()))
            return Environment.getExternalStorageDirectory() + File.separator + SMS_CONDITIONS_DUMP_FILE_NAME;
        return context.getFilesDir() + File.separator + SMS_CONDITIONS_DUMP_FILE_NAME;
    }

    public static void configure(Context context) {
        File file = new File(GetLogFileDir(context));
        file.setReadable(true);
        if(file.exists()) {

         double bytes = file.length();
         double kilobytes = (bytes / 1024);
         double megabytes = (kilobytes / 1024);
         if(megabytes >=15)
             file.delete();
        }
        final LogConfigurator logConfigurator = new LogConfigurator();
        logConfigurator.setFileName(GetLogFileDir(context));
        logConfigurator.setRootLevel(Level.INFO);
        // Set log level of a specific logger
        logConfigurator.configure();
    }

    public static String GetVersion(Context context) {
        String version = "ERROR";
        try {
            version = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception ex) {
            Logger.getLogger("Constant").warning("Problem with version");
        }
        return version;
    }

}
