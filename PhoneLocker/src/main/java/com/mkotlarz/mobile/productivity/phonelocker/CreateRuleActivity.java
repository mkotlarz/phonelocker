package com.mkotlarz.mobile.productivity.phonelocker;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.apache.log4j.Logger;

import com.mkotlarz.mobile.productivity.phonelocker.R;
import com.mkotlarz.mobile.productivity.phonelocker.data.DataManagerImpl;
import com.mkotlarz.mobile.productivity.phonelocker.interfaces.ICondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.CallRulesModel;
import com.mkotlarz.mobile.productivity.phonelocker.models.SmsRulesModel;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.CallTypeCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.Condition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.ContactNameCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.DateCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.DayCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.HourCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.KeywordCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.NumberCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.PatternCondition;


import java.util.Date;
import java.util.Calendar;
import java.util.regex.Pattern;

public class CreateRuleActivity extends Activity {

    private String m_action = null;
    private boolean isEdit = false;
    private int editItemId = 0;
    private CallRulesModel callRulesModel;
    private SmsRulesModel smsRulesModel;
    private String m_ruleName = null;
    private Condition conditions = new Condition();
    private Date m_createDate = null;
    private boolean m_ruleEnable = true;
    private int m_conditionsCounter = 0;
    private boolean m_conditionAdded = false;
    private DataManagerImpl dm;
    private final Logger Log = Logger.getLogger(CreateRuleActivity.class);


    //Inicijalizuje potrzebne zmienne
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_rule);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
        dm = new DataManagerImpl(this);
        Bundle extras = getIntent().getExtras();
        try {
            m_action = extras.getString("action");
            isEdit = extras.getBoolean("isEdit");
            if(isEdit) {
                editItemId = extras.getInt("editItemPosition");
                if(m_action.equals("call")) {
                    callRulesModel = dm.findCallRuleByPosition(editItemId);
                    m_ruleName = callRulesModel.getRuleName();
                    if(callRulesModel.getRuleActive() == 1)
                        m_ruleEnable = true;
                    conditions.fromJSON(callRulesModel.getRuleConditions());
                }
                if(m_action.equals("sms")) {
                    smsRulesModel = dm.findSmsRuleByPosition(editItemId);
                    m_ruleName = smsRulesModel.getRuleName();
                    if(smsRulesModel.getRuleActive() == 1)
                        m_ruleEnable = true;
                    conditions.fromJSON(smsRulesModel.getRuleConditions());
                }
            }
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getMessage());
        }
    }

    //Dalsza częśc inicjalizacji zmiennych
    @Override
    protected void onResume() {
        super.onResume();
        EditText ruleNameEditText = (EditText) findViewById(R.id.ruleName);
        if(m_ruleName != null && !m_ruleName.equals("")) {
            ruleNameEditText.setText(m_ruleName);
        }
        if(m_conditionAdded) {
            updateConditionsView();
            m_conditionAdded = false;
        }
        updateConditionsView();
    }

    @Override
    protected void onPause() {
        super.onPause();
        EditText ruleNameEditText = (EditText) findViewById(R.id.ruleName);
        m_ruleName = ruleNameEditText.getText().toString();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.create_rule, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.save_rule) {
            if(saveRule())
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    //Uruchamia aktywność odpowiedzialną za utworzenie warunku
    public void startConditionsAddActivity(View view) {
        Intent intent = new Intent(this, ConditionAddActivity.class);
        Bundle extras = new Bundle();
        extras.putString("action", m_action);
        intent.putExtras(extras);
        startActivityForResult(intent, 1);
    }

    /*
    * Zapisuje regułę w bazie, do serializacji danych wykorzystuje pakiet JSON
    */
    private boolean saveRule() {
        if(!prepareSaveAction()) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            //Sprawdza czy pola nie są puste
            alertDialogBuilder.setTitle(getResources().getString(R.string.rule_create_empty_name_warning));
            alertDialogBuilder
                    .setMessage(getResources().getString(R.string.rule_create_empty_name_warning_text))
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Do nothing
                        }
                    })
                    .setIcon(android.R.drawable.stat_sys_warning);
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
            return false;
        } else {
            try {
                if(m_action.equals("call"))
                    if(!isEdit)
                        dm.saveCallRule(m_ruleName, m_createDate, conditions.toJSON(), true);
                    else {
                        callRulesModel.setRuleName(m_ruleName);
                        callRulesModel.setRuleConditions(conditions.toJSON());
                        dm.editCallRule(callRulesModel);
                    }
                else if(m_action.equals("sms"))
                    if(!isEdit)
                        dm.saveSmsRule(m_ruleName, m_createDate, conditions.toJSON(), true);
                    else {
                        smsRulesModel.setRuleName(m_ruleName);
                        smsRulesModel.setRuleConditions(conditions.toJSON());
                        dm.editSmsRule(smsRulesModel);
                    }
                else
                    return false;
                return true;
            } catch (Exception ex) {
                Log.error(ex.getMessage() + "..." + ex.getMessage());
                return false;
            }
        }
    }

    //Przygotowywuje dane do zapisu do bazy danych
    private boolean prepareSaveAction() {
        EditText ruleNameEditText = (EditText) findViewById(R.id.ruleName);
        m_ruleName = ruleNameEditText.getText().toString();
        m_createDate = Calendar.getInstance().getTime();
        return !m_ruleName.equals("");
    }


    //Pobiera wyniki przekazane przez aktywność odpowiedzialną za utworzenie warunku
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == Activity.RESULT_OK) {
            Bundle extras = data.getExtras();
            ICondition condition = (ICondition) extras.getSerializable("CONDITION");
            conditions.Add(condition);
            m_conditionAdded = true;
        }
    }


    //Aktualizuje listę conditionsów na ekranie zależnie od ich typu
    // Wyświetla wszystkie zdefiniowane warunki
    private void updateConditionsView() {
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.create_rule_condtions_relative_view);
        clearConditionList();
        int conditionListItemId = 50;
        for (int i = 0; i < conditions.countConditions(); i++) {
            TextView conditionTextView = new TextView(this);
            ICondition condition = conditions.getCondition(i);
            switch (condition.getClassCode()) {
                case 0:
                    CallTypeCondition callTypeCondition = (CallTypeCondition) condition;
                    conditionTextView.setText(getResources().getString(R.string.create_rule_block_call_text) + " " + getResources().getString(callTypeCondition.getCallTypeDisplay()));
                    break;
                case 1:
                    NumberCondition numberCondition = (NumberCondition) condition;
                    conditionTextView.setText(getResources().getString(R.string.create_rtle_block_number_text)  + " " + numberCondition.getNumber());
                    break;
                case 2:
                    DayCondition dayCondition = (DayCondition) condition;
                    conditionTextView.setText(getResources().getString(R.string.create_rule_block_at) + " " + getResources().getStringArray(R.array.days_of_week)[dayCondition.getDayDisplay()]);
                    break;
                case 3:
                    DateCondition dateCondition = (DateCondition) condition;
                    conditionTextView.setText(getResources().getString(R.string.create_rule_block_between) + " " + dateCondition.getStartDate().toString() + " - " + dateCondition.getEndDate().toString());
                    break;
                case 4:
                    ContactNameCondition contactNameCondition = (ContactNameCondition) condition;
                    conditionTextView.setText(getResources().getString(R.string.create_rule_block_when) + " " +  contactNameCondition.getContactName() + " " + getResources().getString(R.string.create_rule_calling));
                    break;
                case 5:
                    HourCondition hourCondition = (HourCondition) condition;
                    conditionTextView.setText(getResources().getString(R.string.create_rule_block_between) + " " + hourCondition.getStartHour() + ":" + hourCondition.getStartMinutes() + " "
                            + getResources().getString(R.string.create_rule_hour) + " " + getResources().getString(R.string.create_rule_and) + " "
                            + hourCondition.getEndHour() + ":" + hourCondition.getEndMinutes() + " " + getResources().getString(R.string.create_rule_hour));
                    break;
                case 6:
                    KeywordCondition keywordCondition = (KeywordCondition) condition;
                    conditionTextView.setText("Block when message contains: " + keywordCondition.getKeyword());
                    break;
                case 7:
                    PatternCondition patternCondition = (PatternCondition) condition;
                    String match_for = "Match ";
                    if (patternCondition.getMatchContactName())
                        match_for += getString(R.string.contact_name) + ", ";
                    if(patternCondition.getMatchNumber())
                        match_for += getString(R.string.number) + ", ";
                    if (patternCondition.getMatchMessageBody())
                        match_for += getString(R.string.message_body) + ", ";
                    match_for += "with pattern: " + patternCondition.getPattern();
                    conditionTextView.setText(match_for);
                    break;
            }
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 20, 0, 0);
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
            if(conditionListItemId != 50) {
                params.addRule(RelativeLayout.BELOW, (conditionListItemId - 1));
            }
            conditionTextView.setLayoutParams(params);
            conditionTextView.setId(conditionListItemId);
            conditionListItemId++;
            layout.addView(conditionTextView);
        }
    }

    //Czyści listę warunków
    public void clearConditionList() {
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.create_rule_condtions_relative_view);
        for (int i = layout.getChildCount() - 1; i >= 0; i-- ) {
            layout.removeViewAt(i);
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_create_rule, container, false);
            getActivity().getActionBar().setTitle(getString(R.string.create_rule_fragment_name));
            return rootView;
        }
    }
}