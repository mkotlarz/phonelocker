package com.mkotlarz.mobile.productivity.phonelocker.adapter;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mkotlarz.mobile.productivity.phonelocker.R;
import com.mkotlarz.mobile.productivity.phonelocker.data.Constants;
import com.mkotlarz.mobile.productivity.phonelocker.data.DataManagerImpl;
import com.mkotlarz.mobile.productivity.phonelocker.sms.models.Conversation;
import com.mkotlarz.mobile.productivity.phonelocker.sms.models.DataProvider;
import com.mkotlarz.mobile.productivity.phonelocker.sms.models.Message;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by Maciej on 2014-06-30.
 */
public class ConversationDetailAdapter extends BaseAdapter implements Observer {

    /**
     * The inflator used to inflate the XML layout
     */
    private LayoutInflater inflator;

    /**
     * A list containing some sample data to show.
     */
    private List<Message> dataList;
    private Context m_context = null;
    private DataManagerImpl m_dataManager = null;
    private Conversation conversation = null;
    private String recipientPhotoUri = null;
    private String senderPhotoUri = null;

    public ConversationDetailAdapter(LayoutInflater inflator, Conversation conversation, Context context) {
        super();
        this.inflator = inflator;

        dataList = conversation.getMessages();
        recipientPhotoUri = conversation.getPhotoUri();
        m_context = context;
        m_dataManager = new DataManagerImpl(m_context);
        this.conversation = conversation;
        DataProvider.getInstance().addObserver(this);
    }

    private void getSenderPhotoUri() {
        TelephonyManager manager = (TelephonyManager) m_context.getSystemService(Context.TELEPHONY_SERVICE);
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(manager.getLine1Number()));
        ContentResolver contentResolver = m_context.getContentResolver();
        Cursor thumbnailLookup = contentResolver.query(uri, new String[]{BaseColumns._ID,
                ContactsContract.PhoneLookup.PHOTO_URI}, null, null, null);
        if (thumbnailLookup.moveToFirst())
            senderPhotoUri = thumbnailLookup.getString(thumbnailLookup.getColumnIndex(ContactsContract.PhoneLookup.PHOTO_URI));
        thumbnailLookup.close();
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Message getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Message message = getItem(position);
        ImageView photo;
        TextView bubleText;
        if(convertView != null) {
            convertView = null;
        }
        if (convertView == null && message.getMessageType() == Constants.SMS_INCOMING_TYPE_VALUE) {
            convertView = inflator.inflate(R.layout.conversation_detail_buble_recipient, parent, false);
            convertView.setTag(R.id.recipientPhoto, convertView.findViewById(R.id.recipientPhoto));
            convertView.setTag(R.id.buble_text, convertView.findViewById(R.id.buble_text));
            photo = (ImageView) convertView.findViewById(R.id.recipientPhoto);
            if(recipientPhotoUri != null)
                photo.setImageURI(Uri.parse(recipientPhotoUri));
            else
                photo.setImageResource(R.drawable.anonim);
        }
        if(convertView == null && message.getMessageType() != Constants.SMS_INCOMING_TYPE_VALUE) {
            convertView = inflator.inflate(R.layout.conversation_detail_buble_item_sender, parent, false);
            convertView.setTag(R.id.senderPhoto, convertView.findViewById(R.id.senderPhoto));
            convertView.setTag(R.id.buble_text, convertView.findViewById(R.id.buble_text));
            photo = (ImageView) convertView.findViewById(R.id.senderPhoto);
            if(senderPhotoUri != null)
                photo.setImageURI(Uri.parse(senderPhotoUri));
            else
                photo.setImageResource(R.drawable.anonim);
        }
        bubleText = (TextView) convertView.findViewById(R.id.buble_text);
        bubleText.setText(message.getContent());

        if(!message.isReaded())
            message.setMessageAsRead(m_context);
        return convertView;
    }

    public void refreshAdapterValues(List<Message> messageList) {
        dataList = messageList;
        notifyDataSetChanged();
    }

    @Override
    public void update(Observable observable, Object data) {
        refreshAdapterValues(conversation.getMessageList());
        }
    }