package com.mkotlarz.mobile.productivity.phonelocker.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mkotlarz.mobile.productivity.phonelocker.R;

import org.apache.log4j.Logger;


/**
 * Created by Maciej on 08.12.13.
 */
public class ViewAdapter extends BaseAdapter {

    private String[][] m_values = null;
    private LayoutInflater mInflater;
    private int m_type;
    private final Logger Log = Logger.getLogger(ViewAdapter.class);


    public ViewAdapter (Context c, String[][] values, int type) {
        m_values = values;
        //Cache a reference to avoid looking it up on every getView() call
        mInflater = LayoutInflater.from(c);
        m_type = type;
    }

    @Override
    public int getCount () {
        return m_values.length;
    }

    @Override
    public long getItemId (int position) {
        return position;
    }

    @Override
    public Object getItem (int position) {
        return m_values[position];
    }

    public int getItemType() {
        return m_type;
    }

    @Override
    public View getView (int position, View convertView, ViewGroup parent) {
        //If there's no recycled view, inflate one and tag each of the views
        //you'll want to modify later
        if (convertView == null) {
            convertView = mInflater.inflate (R.layout.list_item, parent, false);

            //This assumes layout/row_left.xml includes a TextView with an id of "textview"
            convertView.setTag (R.id.number, convertView.findViewById(R.id.number));
            convertView.setTag(R.id.thumb, convertView.findViewById(R.id.thumb));
            convertView.setTag(R.id.date, convertView.findViewById(R.id.date));
            convertView.setTag(R.id.extra, convertView.findViewById(R.id.extra));
        }

        //Retrieve the tagged view, get the item for that position, and
        //update the text
        String[] values = (String[]) getItem(position);
        String phoneNumber  = values[0];
        String date         = values[1];
        String extra        = values[2];
        String alias        = values[3];
        int typ             = getItemType();

        TextView pNumberView = (TextView) convertView.getTag(R.id.number);
        if (alias == null || alias.equals("")) {
        pNumberView.setText(phoneNumber);
        } else {
            pNumberView.setText(alias);
        }
        TextView dateView = (TextView) convertView.getTag(R.id.date);
        dateView.setText(date);
        TextView extraView = (TextView) convertView.getTag(R.id.extra);
        Log.info("View type - " + typ);
        if (typ == 1) {
            if (extra.equals("0")) extra = "Przychodzace";
            if (extra.equals("1")) extra = "Wychodzace";
        }
        extraView.setText(extra);
        ImageView thumb = (ImageView) convertView.getTag(R.id.thumb);
        if (typ == 1) {
            if(extra.equals("Przychodzace")) {
                //thumb.setImageDrawable(convertView.getResources().getDrawable(R.drawable.icalls));
            }
            if(extra.equals("Wychodzace")) {
                //thumb.setImageDrawable(convertView.getResources().getDrawable(R.drawable.ocalls));
            }
        }
        if (typ == 2) {
            thumb.setPadding(0, 10, 0, 0);
            //thumb.setImageDrawable(convertView.getResources().getDrawable(R.drawable.envelope));
        }

        return convertView;
    }


    public void refreshAdapterValues(String[][] rules) {
        m_values = rules;
        notifyDataSetChanged();
    }

}
