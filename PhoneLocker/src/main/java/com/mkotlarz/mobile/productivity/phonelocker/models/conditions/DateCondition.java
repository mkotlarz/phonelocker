package com.mkotlarz.mobile.productivity.phonelocker.models.conditions;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mkotlarz.mobile.productivity.phonelocker.R;
import com.mkotlarz.mobile.productivity.phonelocker.interfaces.ICondition;

import org.apache.log4j.Logger;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Maciej on 2014-06-29.
 */
public class DateCondition implements Serializable, ICondition {

    public final static String JSON_NAME = "DateCondition";

    @Expose
    @SerializedName("startDate")
    private String m_startDate = null;

    @Expose
    @SerializedName("endDate")
    private String m_endDate = null;

    private String m_matched_on = null;

    private SimpleDateFormat m_dateFormater = null;

    public DateCondition(Date startDate, Date endDate) {
        m_dateFormater = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        this.m_startDate = m_dateFormater.format(startDate);
        this.m_endDate = m_dateFormater.format(endDate);
    }

    public DateCondition() {
        m_dateFormater = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
    }

    public String getMatchedOn() { return m_matched_on; }

    public void setMatchedOn(String matched_on) { m_matched_on = matched_on; }

    public String getStartDate() {
        return m_startDate;
    }

    public void setStartDate(Date m_startDate) {
        this.m_startDate = m_dateFormater.format(m_startDate);
    }

    public String getEndDate() {
            return m_endDate;
    }

    public void setEndDate(Date m_endDate) {
        this.m_endDate = m_dateFormater.format(m_endDate);
    }

    public String getJSONName() {
        return "DateCondition";
    }

    public Class<?> getConditionClass() {
        return DateCondition.class;
    }

    public int getClassCode() {
        return 3;
    }

    public boolean Match(Date date) {
        final Logger Log = Logger.getLogger(DateCondition.class);
        Log.info("Matching if date is between: " + m_startDate.toString() + " and " + m_endDate.toString());
        try {
            Date startDate = m_dateFormater.parse(m_startDate);
            Date endDate = m_dateFormater.parse(m_endDate);
            if (date.getTime() > startDate.getTime() && date.getTime() < endDate.getTime()) {
                Log.info("Result: true");
                m_matched_on = date.toString();
                return true;
            }
            Log.info("Result: false");
            return false;
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getStackTrace());
            return false;
        }
    }

    public String getClassTranslatedName(Context ctx) {
        return ctx.getString(R.string.date_condition);
    }
}
