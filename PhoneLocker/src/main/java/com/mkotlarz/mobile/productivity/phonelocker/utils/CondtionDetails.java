package com.mkotlarz.mobile.productivity.phonelocker.utils;

import android.content.Context;
import android.text.Html;
import android.text.InputType;
import android.text.method.LinkMovementMethod;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;


import com.mkotlarz.mobile.productivity.phonelocker.R;

/**
 * Created by Maciej on 2014-06-28.
 * Kontroler widoku odpowiedzialny za wyswietlenie danych o warunku
 */
public class CondtionDetails {

    private Spinner m_callTypeSpinner = null;
    private Spinner m_daySpinner = null;
    private CheckBox m_everyWeekCheckBox = null;
    private DatePicker m_startPicker = null;
    private DatePicker m_endPicker = null;
    private EditText m_phoneNumberEditText = null;
    private EditText m_keywordEditText = null;
    private EditText m_contactNameEditText = null;
    private TimePicker m_startTimePicker = null;
    private TimePicker m_endTimePicker = null;
    private EditText m_patternEditText = null;
    private CheckBox m_matchContactNameCheckBox = null;
    private CheckBox m_matchNumberCheckBox = null;
    private CheckBox m_matchMessageBodyCheckBox = null;
    private TextView m_helpLink = null;
    private int m_lastItemId = 30;
    private Context m_context = null;


    public CondtionDetails(Context context) {
        this.m_context = context;
    }

    public void ClearWindow(RelativeLayout layout) {
        for(int i = (layout.getChildCount() - 1); i >= 2; i--) {
            layout.removeViewAt(i);
        }
    }

    public void addCallTypeDetails(RelativeLayout layout) {
        m_callTypeSpinner = new Spinner(m_context);
        m_callTypeSpinner.setAdapter(new ArrayAdapter<String>(m_context, android.R.layout.simple_spinner_dropdown_item, m_context.getResources().getStringArray(R.array.call_rules_call_type_spinner)));
        m_callTypeSpinner.setLayoutParams(SpinnerParams(R.id.conditionTypeSpinner));
        m_callTypeSpinner.setId(m_lastItemId);
        layout.addView(m_callTypeSpinner);
    }

    public void addNumberDetails(RelativeLayout layout) {
        int textId = 21;
        TextView textView = new TextView(m_context);
        textView.setText(m_context.getResources().getString(R.string.condition_add_phone_number));
        RelativeLayout.LayoutParams textParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        textParams.addRule(RelativeLayout.BELOW, R.id.conditionTypeSpinner);
        textParams.setMargins(0,25,0,10);
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(14);
        textView.setId(textId);
        textView.setLayoutParams(textParams);
        m_phoneNumberEditText = new EditText(m_context);
        RelativeLayout.LayoutParams editTextParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        editTextParams.addRule(RelativeLayout.BELOW, R.id.conditionTypeSpinner);
        editTextParams.addRule(RelativeLayout.RIGHT_OF, textId);
        editTextParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        m_phoneNumberEditText.setLayoutParams(editTextParams);
        m_phoneNumberEditText.setInputType(InputType.TYPE_CLASS_PHONE);
        m_phoneNumberEditText.setId(m_lastItemId);
        layout.addView(textView);
        layout.addView(m_phoneNumberEditText);
    }

    public void addDayDetails(RelativeLayout layout) {
        int id = 20;
        m_daySpinner = new Spinner(m_context);
        m_daySpinner.setAdapter(new ArrayAdapter<String>(m_context, android.R.layout.simple_spinner_dropdown_item, m_context.getResources().getStringArray(R.array.days_of_week)));
        m_daySpinner.setLayoutParams(SpinnerParams(R.id.conditionTypeSpinner));
        m_daySpinner.setId(id);
        m_everyWeekCheckBox = new CheckBox(m_context);
        m_everyWeekCheckBox.setText(m_context.getResources().getString(R.string.condition_every_week));
        RelativeLayout.LayoutParams checkBoxParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        checkBoxParams.addRule(RelativeLayout.BELOW, id);
        checkBoxParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        m_everyWeekCheckBox.setLayoutParams(checkBoxParams);
        m_everyWeekCheckBox.setId(m_lastItemId);
        m_everyWeekCheckBox.setVisibility(View.GONE);
        layout.addView(m_daySpinner);
        layout.addView(m_everyWeekCheckBox);
    }

    public void addDateDetails(RelativeLayout layout) {
        int id = 22;
        int m_startPickerId = 23;
        int endTextId = 24;
        TextView startText = new TextView(m_context);
        startText.setText(m_context.getResources().getString(R.string.condition_from));
        startText.setPadding(0, 15, 0, 0);
        startText.setLayoutParams(TextParams(R.id.conditionTypeSpinner));
        startText.setId(id);
        m_startPicker = new DatePicker(m_context);
        m_startPicker.setLayoutParams(SecondItemInLine(R.id.conditionTypeSpinner, id));
        m_startPicker.setCalendarViewShown(false);
        m_startPicker.setId(m_startPickerId);
        TextView endText = new TextView(m_context);
        endText.setText(m_context.getResources().getString(R.string.condition_to));
        endText.setLayoutParams(TextParams(m_startPickerId));
        endText.setId(endTextId);
        m_endPicker = new DatePicker(m_context);
        m_endPicker.setCalendarViewShown(false);
        m_endPicker.setLayoutParams(SecondItemInLine(m_startPickerId, endTextId));
        m_endPicker.setId(m_lastItemId);
        layout.addView(m_startPicker);
        layout.addView(startText);
        layout.addView(endText);
        layout.addView(m_endPicker);
    }

    public void addContactNameDetails(RelativeLayout layout) {
        int id = 25;
        TextView textView = new TextView(m_context);
        textView.setText(m_context.getResources().getString(R.string.condition_add_contact_name));
        textView.setLayoutParams(TextParams(R.id.conditionTypeSpinner));
        textView.setPadding(0, 20, 0, 0);
        textView.setId(id);
        m_contactNameEditText = new EditText(m_context);
        m_contactNameEditText.setLayoutParams(SecondItemInLine(R.id.conditionTypeSpinner, id));
        m_contactNameEditText.setId(m_lastItemId);
        layout.addView(textView);
        layout.addView(m_contactNameEditText);
    }

    public void addHourDetails(RelativeLayout layout) {
        int id = 28;
        int m_startPickerId = 29;
        int endTextId = 30;
        TextView startText = new TextView(m_context);
        startText.setText(m_context.getResources().getString(R.string.condition_from));
        startText.setPadding(0, 15, 0, 0);
        startText.setLayoutParams(TextParams(R.id.conditionTypeSpinner));
        startText.setId(id);
        m_startTimePicker = new TimePicker(m_context);
        m_startTimePicker.setLayoutParams(SecondItemInLine(R.id.conditionTypeSpinner, id));
        m_startTimePicker.setId(m_startPickerId);
        TextView endText = new TextView(m_context);
        endText.setText(m_context.getResources().getString(R.string.condition_to));
        endText.setLayoutParams(TextParams(m_startPickerId));
        endText.setId(endTextId);
        m_endTimePicker = new TimePicker(m_context);
        m_endTimePicker.setLayoutParams(SecondItemInLine(m_startPickerId, endTextId));
        m_endTimePicker.setId(m_lastItemId);
        layout.addView(m_startTimePicker);
        layout.addView(startText);
        layout.addView(endText);
        layout.addView(m_endTimePicker);
    }

    public void addKeywordDetails(RelativeLayout layout) {
        int textId = 28;
        TextView textView = new TextView(m_context);
        textView.setText(m_context.getResources().getString(R.string.condition_add_keyword));
        RelativeLayout.LayoutParams textParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        textParams.addRule(RelativeLayout.BELOW, R.id.conditionTypeSpinner);
        textParams.setMargins(0,25,0,10);
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(14);
        textView.setId(textId);
        textView.setLayoutParams(textParams);
        m_keywordEditText = new EditText(m_context);
        RelativeLayout.LayoutParams editTextParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        editTextParams.addRule(RelativeLayout.BELOW, R.id.conditionTypeSpinner);
        editTextParams.addRule(RelativeLayout.RIGHT_OF, textId);
        editTextParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        editTextParams.addRule(RelativeLayout.ALIGN_RIGHT, textId);
        m_keywordEditText.setLayoutParams(editTextParams);
        m_keywordEditText.setInputType(InputType.TYPE_CLASS_TEXT);
        m_keywordEditText.setId(m_lastItemId);
        layout.addView(textView);
        layout.addView(m_keywordEditText);
    }

    public void addPatternDetails(RelativeLayout layout) {
        int editTextId = 31;
        m_patternEditText = new EditText(m_context);
        m_patternEditText.setHint(m_context.getString(R.string.add_pattern_condition_hint));
        m_patternEditText.setLayoutParams(SpinnerParams(R.id.conditionTypeSpinner));
        m_patternEditText.setId(editTextId);
        int matchContactNameId = 32;
        m_matchContactNameCheckBox = new CheckBox(m_context);
        m_matchContactNameCheckBox.setText(m_context.getString(R.string.match_contact_name_label));
        m_matchContactNameCheckBox.setLayoutParams(TextParams(editTextId));
        m_matchContactNameCheckBox.setId(matchContactNameId);
        int matchNumberId = 33;
        m_matchNumberCheckBox = new CheckBox(m_context);
        m_matchNumberCheckBox.setText(m_context.getString(R.string.match_number_label));
        m_matchNumberCheckBox.setLayoutParams(TextParams(matchContactNameId));
        m_matchNumberCheckBox.setId(matchNumberId);
        int matchMessageBody = m_lastItemId;
        m_matchMessageBodyCheckBox = new CheckBox(m_context);
        m_matchMessageBodyCheckBox.setText(m_context.getString(R.string.match_message_body_label));
        m_matchMessageBodyCheckBox.setLayoutParams(TextParams(matchNumberId));
        m_matchMessageBodyCheckBox.setId(matchMessageBody);
        m_helpLink = new TextView(m_context);
        m_helpLink.setText(
                Html.fromHtml(
                       "<a href=\"http://developer.android.com/reference/java/util/regex/Pattern.html\">" +  m_context.getString(R.string.pattern_help_link_content) + "</a>"));
        m_helpLink.setMovementMethod(LinkMovementMethod.getInstance());
        m_helpLink.setLayoutParams(HelpLinkParams(matchMessageBody));

        layout.addView(m_patternEditText);
        layout.addView(m_matchContactNameCheckBox);
        layout.addView(m_matchNumberCheckBox);
        layout.addView(m_matchMessageBodyCheckBox);
        layout.addView(m_helpLink);
    }

    public RelativeLayout.LayoutParams SpinnerParams(int below) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.BELOW, below);
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        return params;
    }

    public RelativeLayout.LayoutParams ButtonParams(int below) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.BELOW, below);
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        params.setMargins(0, 20, 10, 0);
        return params;
    }

    public RelativeLayout.LayoutParams SecondItemInLine(int below, int rightOf) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.BELOW, below);
        params.addRule(RelativeLayout.RIGHT_OF, rightOf);
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        return params;
    }

    public RelativeLayout.LayoutParams TextParams(int below) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.BELOW, below);
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        return params;
    }

    public RelativeLayout.LayoutParams HelpLinkParams(int below) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.BELOW, below);
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        params.setMargins(15, 5, 0, 0);
        return params;
    }

    //Getters
    public Spinner GetCallTypeSpinner() {
        return this.m_callTypeSpinner;
    }

    public Spinner GetDaysSpinner() {
        return this.m_daySpinner;
    }

    public CheckBox GetEveryWeekCheckBox() {
        return this.m_everyWeekCheckBox;
    }

    public DatePicker GetStartDatePicker() {
        return this.m_startPicker;
    }

    public DatePicker GetEndDatePicker() {
        return this.m_endPicker;
    }

    public EditText GetPhoneNumberEditText() {
        return m_phoneNumberEditText;
    }

    public EditText GetKeywordEditText() { return m_keywordEditText; }

    public EditText GetContactNameEditText() {
        return m_contactNameEditText;
    }

    public TimePicker GetStartHourPicker() { return m_startTimePicker; }

    public TimePicker GetEndHourPicker() { return m_endTimePicker; }

    public CheckBox getMatchContactNameCheckBox() { return m_matchContactNameCheckBox; }

    public EditText getPatternEditText() { return m_patternEditText; }

    public CheckBox getMatchNumberCheckBox() { return m_matchNumberCheckBox; }

    public CheckBox getMatchMessageBodyCheckBox() { return m_matchMessageBodyCheckBox; }

    public int GetLastItemId() {
        return m_lastItemId;
    }
}
