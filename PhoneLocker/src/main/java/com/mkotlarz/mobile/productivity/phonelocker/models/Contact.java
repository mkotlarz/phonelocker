package com.mkotlarz.mobile.productivity.phonelocker.models;

/**
 * Created by Maciej on 2014-08-04.
 */
public class Contact {
    private String m_number = null;
    private String m_name = null;

    public Contact(String number, String name) {
        m_number = number;
        m_name = name;
    }

    public String getNumber() {
        return m_number;
    }

    public String getName() {
        return m_name;
    }
}
