package com.mkotlarz.mobile.productivity.phonelocker.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.provider.BaseColumns;

import com.mkotlarz.mobile.productivity.phonelocker.data.CallRulesTable.CallRulesColumns;
import com.mkotlarz.mobile.productivity.phonelocker.models.CallRulesModel;

public class CallRulesDao {

    private static final String INSERT =
            "insert into " + CallRulesTable.TABLE_NAME + "("
                    + CallRulesColumns.RULENAME + ", "
                    + CallRulesColumns.DATE + ", "
                    + CallRulesColumns.RULECONDITIONS + ", "
                    + CallRulesColumns.RULEACTIVE + ") values (?, ?, ?, ?)";

    private SQLiteDatabase db;
    private SQLiteStatement insertStatement;

    public CallRulesDao(SQLiteDatabase db) {
        this.db = db;
        insertStatement = db.compileStatement(CallRulesDao.INSERT);
    }
    public long save(CallRulesModel cRulesModel) {
        insertStatement.clearBindings();
        insertStatement.bindString(1, cRulesModel.getRuleName());
        insertStatement.bindString(2, cRulesModel.getRuleDate());
        insertStatement.bindString(3, cRulesModel.getRuleConditions());
        insertStatement.bindLong(4, cRulesModel.getRuleActive());
        return insertStatement.executeInsert();
    }

    public int update(CallRulesModel cModel) {
        final ContentValues values = new ContentValues();
        values.put("rule_name" , cModel.getRuleName());
        values.put("rule_conditions", cModel.getRuleConditions());
        values.put("active", cModel.getRuleActive());
        db.update(CallRulesTable.TABLE_NAME, values, BaseColumns._ID + " = ?", new String[] { String.valueOf(cModel.getId()) });
        return 1;
    }

    public  void deleteCallRule(CallRulesModel cModel) {
        db.delete(CallRulesTable.TABLE_NAME, BaseColumns._ID + " = ?", new String[] { String.valueOf(cModel.getId())});
    }

    public CallRulesModel getId(long call_id) {
        CallRulesModel call = null;
        Cursor c =
                db.query(CallRulesTable.TABLE_NAME, new String[] { BaseColumns._ID, CallRulesColumns.RULENAME,
                                CallRulesColumns.RULETYPE},
                        BaseColumns._ID + " = ?", new String[] { String.valueOf(call_id) }, null, null, null, "1");
        if (c.moveToFirst()) {
            call = this.buildCallFromCursor(c);
        }
        if (!c.isClosed()) {
            c.close();
        }
        return call;
    }
    public String[][] getAllRules() {

        Cursor callsCursor = db.rawQuery("select * from " + CallRulesTable.TABLE_NAME + " ORDER BY " + CallRulesColumns._ID +" ASC", null);
        String[][] result = new String[callsCursor.getCount()][5];
        callsCursor.moveToFirst();
        for(int i = 0; i < callsCursor.getCount(); i++){
            String ruleName = callsCursor.getString(callsCursor.getColumnIndex(CallRulesColumns.RULENAME));
            String date = callsCursor.getString(callsCursor.getColumnIndex(CallRulesColumns.DATE));
            String ruleConditions = callsCursor.getString(callsCursor.getColumnIndex(CallRulesColumns.RULECONDITIONS));
            String active = callsCursor.getString(callsCursor.getColumnIndex(CallRulesColumns.RULEACTIVE));
            //You can here manipulate a single string as you please
            result[i][0] = active;
            result[i][1] = ruleName;
            result[i][2] = ruleConditions;
            result[i][3] = date;
            callsCursor.moveToNext();
        }
        callsCursor.close();
        return result;
    }

    public String dumpToFile() {
        Cursor callsCursor = db.rawQuery("select * from " + CallRulesTable.TABLE_NAME, null);
        String sql = CallRulesTable.CreateTableSql() + System.getProperty("line.separator");
        callsCursor.moveToFirst();
        for(int i = 0; i< callsCursor.getCount(); i++) {
            sql += INSERT.replace("(?, ?, ?, ?)", "");
            sql += "('" + callsCursor.getString(callsCursor.getColumnIndex(SmsRulesTable.SmsRulesColumns.RULENAME)) + "', '" + callsCursor.getString(callsCursor.getColumnIndex(SmsRulesTable.SmsRulesColumns.DATE))
                    + "', " + callsCursor.getString(callsCursor.getColumnIndex(SmsRulesTable.SmsRulesColumns.RULECONDITIONS)) + "', '" + callsCursor.getString(callsCursor.getColumnIndex(SmsRulesTable.SmsRulesColumns.RULEACTIVE)) + "');" + System.getProperty("line.separator");
            callsCursor.moveToNext();
        }
        callsCursor.close();
        return sql;
    }

    public String[][] getAllActiveRules() {

        Cursor callsCursor = db.rawQuery("select * from " + CallRulesTable.TABLE_NAME + " WHERE " + CallRulesColumns.RULEACTIVE + " = '1' ORDER BY " + CallRulesColumns._ID + " ASC", null);
        String[][] result = new String[callsCursor.getCount()][5];
        callsCursor.moveToFirst();
        for (int i = 0; i < callsCursor.getCount(); i++) {
            String ruleName = callsCursor.getString(callsCursor.getColumnIndex(CallRulesColumns.RULENAME));
            String date = callsCursor.getString(callsCursor.getColumnIndex(CallRulesColumns.DATE));
            String ruleConditions = callsCursor.getString(callsCursor.getColumnIndex(CallRulesColumns.RULECONDITIONS));
            String active = callsCursor.getString(callsCursor.getColumnIndex(CallRulesColumns.RULEACTIVE));
            //You can here manipulate a single string as you please
            result[i][0] = active;
            result[i][1] = ruleName;
            result[i][2] = ruleConditions;
            result[i][3] = date;
            callsCursor.moveToNext();
        }
        callsCursor.close();
        return result;
    }

    private CallRulesModel buildCallFromCursor(Cursor c) {
        CallRulesModel call = null;
        if (c != null) {
            call = new CallRulesModel();
            call.setId(c.getLong(0));
        }
        return call;
    }
}
