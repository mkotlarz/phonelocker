package com.mkotlarz.mobile.productivity.phonelocker.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.apache.log4j.Logger;

//
// SQLiteOpenHelper   
//
public class OpenHelper extends SQLiteOpenHelper {

    private final Logger Log = Logger.getLogger(OpenHelper.class);


    private static final int DATABASE_VERSION = 2;
   
   private Context context;

   OpenHelper(final Context context) {
      super(context, DataConstants.DATABASE_NAME, null, DATABASE_VERSION);
      this.context = context;
   }

   @Override
   public void onOpen(final SQLiteDatabase db) {
      super.onOpen(db);
      if (!db.isReadOnly()) {
         // versions of SQLite older than 3.6.19 don't support foreign keys
         // and neither do any version compiled with SQLITE_OMIT_FOREIGN_KEY
         // http://www.sqlite.org/foreignkeys.html#fk_enable
         // 
         // make sure foreign key support is turned on if it's there (should be already, just a double-checker)          
         db.execSQL("PRAGMA foreign_keys=ON;");

         // then we check to make sure they're on 
         // (if this returns no data they aren't even available, so we shouldn't even TRY to use them)
         Cursor c = db.rawQuery("PRAGMA foreign_keys", null);
         if (c.moveToFirst()) {
            int result = c.getInt(0);
         } else {
         }
         if (!c.isClosed()) {
            c.close();
         }
          Constants.configure(context);
      }
   }

   @Override
   public void onCreate(final SQLiteDatabase db) {
      Log.info("DataHelper.OpenHelper onCreate creating database " + DataConstants.DATABASE_NAME);
      SettingsTable.onCreate(db);
      CallsTable.onCreate(db);
      // populate initial categories (one way, there are several, this works ok for small data set)
      // (this is just as an example, MyMovies really doesn't use/need the "category manager"
      SmsTable.onCreate(db);
      CallRulesTable.onCreate(db);
      SmsRulesTable.onCreate(db);

   }

   @Override
   public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
      Log.info("SQLiteOpenHelper onUpgrade - oldVersion:" + oldVersion + " newVersion:"
                        + newVersion);
      
      SettingsTable.onUpgrade(db, oldVersion, newVersion);

      SmsTable.onUpgrade(db, oldVersion, newVersion);

      CallsTable.onUpgrade(db, oldVersion, newVersion);

      CallRulesTable.onUpgrade(db, oldVersion, newVersion);

      SmsRulesTable.onUpgrade(db, oldVersion, newVersion);
   }
}
