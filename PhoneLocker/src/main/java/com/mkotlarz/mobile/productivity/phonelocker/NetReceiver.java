package com.mkotlarz.mobile.productivity.phonelocker;

import java.lang.reflect.Method;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.net.ConnectivityManager;

import org.apache.log4j.Logger;

/*
Klasa zajmujaca sie pobieraniem informacji o polaczeniu i odpowiada za jego odrzcenie
@author maciej_kotlarz
 */
public class NetReceiver {

    private static final Logger Log = Logger.getLogger(NetReceiver.class);

    //Odrzuca polaczenie na podstawie danych przekazanych z MainReceiver.java
    public static void disableWiFi(Context context) {
        try {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        boolean isEnabled = wifiManager.isWifiEnabled();
        if(isEnabled) {
            wifiManager.setWifiEnabled(false);
            Log.info("WiFi wylaczono");
        }
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getStackTrace());
        }
    }

    public static void disableNetwork(Context context) {
        setMobileDataEnabled(context, false);
    }


    /*
    * Block data transer
    */
    public static void setMobileDataEnabled(Context context, boolean enabled){

        ConnectivityManager conman = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        Method setMobileDataEnabledMethod = null;
        try {
            setMobileDataEnabledMethod = ConnectivityManager.class.getDeclaredMethod("setMobileDataEnabled", boolean.class);
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getStackTrace());
        }

        setMobileDataEnabledMethod.setAccessible(true);
        try {
            setMobileDataEnabledMethod.invoke(conman, enabled);
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getStackTrace());
        }

    }
}
