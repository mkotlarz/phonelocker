package com.mkotlarz.mobile.productivity.phonelocker.models;

import java.util.Date;

public class SMSModel extends ModelBase {

    private long id;
    private String phoneNumber;
    private String body;
    private String date;
    private int typ;
    private String alias;
    private String matchedOn;

    public SMSModel(String phoneNumber, String body, Date date, int typ, String alias, String matchedOn) {
        this.phoneNumber			= 	phoneNumber;
        this.body                   =   body;
        this.date					= 	date.toString();
        this.typ 					=	typ;
        this.alias                  =   alias;
        this.matchedOn              =   matchedOn;
    }

    public SMSModel(String phoneNumber, String body, String date, int typ, String alias, String matchedOn) {
        this.phoneNumber			= 	phoneNumber;
        this.body                   =   body;
        this.date					= 	date;
        this.typ 					=	typ;
        this.alias                  =   alias;
        this.matchedOn              =   matchedOn;
    }

    public SMSModel(String phoneNumber, Date date, int typ, long id, String alias, String matchedOn) {
        this.id 					=	id;
        this.phoneNumber			= 	phoneNumber;
        this.date					= 	date.toString();
        this.typ 					=	typ;
        this.alias                  =   alias;
        this.matchedOn              =   matchedOn;
    }
    //Universal constructor
    public SMSModel() {
        //test
    }
    //Constructor for findById
    public SMSModel(long id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public String getSmsBody() {
        return this.body;
    }

    public String getSmsDate() { return this.date; }

    public long getSmsType() {
        return this.typ;
    }

    public void setPhoneNumber(String number) {
        this.phoneNumber = number;
    }

    public void setDate(Date date) { this.date = date.toString(); }

    public String getAlias() { return this.alias; }

    public void setAlias(String alias) { this.alias = alias; }

    public void setType(int typ) {
        this.typ = typ;
    }

    public String getMatchedOn() {  return matchedOn;   }

    public void setMatchedOn(String matchedOn) {    this.matchedOn = matchedOn; }
}
