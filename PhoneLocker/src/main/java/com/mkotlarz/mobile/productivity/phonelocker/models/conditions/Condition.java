package com.mkotlarz.mobile.productivity.phonelocker.models.conditions;

import java.lang.reflect.Constructor;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import com.google.gson.*;
import com.mkotlarz.mobile.productivity.phonelocker.interfaces.ICondition;
import com.mkotlarz.mobile.productivity.phonelocker.main.RulesExecutor;

import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 * Created by Maciej on 2014-06-28.
 * Przechowywuje wszystki conditiony w aktywność CreateRule
 */
public class Condition {

    private Gson m_gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    private String m_JSONString = null;
    private boolean m_isFirstCondition = true;
    private ArrayList<ICondition> m_conditionsArrayList = new ArrayList<ICondition>();
    private final Logger Log = Logger.getLogger(Condition.class);

    //Dodaje warunek do listy
    public boolean Add(ICondition condition) {
        return m_conditionsArrayList.add(condition);
    }


    //Serializuje dane za pomocą GSONa
    public String toJSON() {
        m_JSONString = "{\"Conditions\": {";
        for(int i = 0; i < m_conditionsArrayList.size(); i++) {
            m_JSONString += prepareJSON(m_conditionsArrayList.get(i));
        }
        m_JSONString += "} }";
        return m_JSONString;
    }

    //Przygotowywuje dane do zapisu za pomocą GSON
    private String prepareJSON(ICondition condition) {
        if(m_isFirstCondition) {
            m_isFirstCondition = false;
            return "\"" + condition.getJSONName() + "\":" + m_gson.toJson(condition) + "";
        }
        return ", \"" + condition.getJSONName() + "\":" + m_gson.toJson(condition) + "";
    }


    //Zczytuje dane z JSON
    public boolean fromJSON(String conditionsString) {
        try {
            JSONObject conditions = new JSONObject(conditionsString).getJSONObject("Conditions");
            Iterator keys = conditions.keys();
            while (keys.hasNext()) {
                String conditionName = (String) keys.next();
                parseCondition(conditionName, conditions.getJSONObject(conditionName));
            }
            return true;
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getStackTrace());
            return false;
        }
    }

    private void parseCondition(String conditionName, JSONObject values) {
        try {
            Class<?> clazz = Class.forName(RulesExecutor.PATH_TO_CONDITION + conditionName);
            Constructor<?> constructor = clazz.getConstructor();
            ICondition condition = (ICondition) constructor.newInstance();
            condition = InitializeCondition(condition, values);
            if(condition != null) {
                m_conditionsArrayList.add(condition);
            }
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getMessage().toString());
        }
    }

    private ICondition InitializeCondition(ICondition condition, JSONObject values) {
        switch (condition.getClassCode()) {
            case 0:
                CallTypeCondition callTypeCondition = (CallTypeCondition) condition;
                return parseCondition(callTypeCondition, values);
            case 1:
                NumberCondition numberCondition = (NumberCondition) condition;
                return parseCondition(numberCondition, values);
            case 2:
                DayCondition dayCondition = (DayCondition) condition;
                return parseCondition(dayCondition, values);
            case 3:
                DateCondition dateCondition = (DateCondition) condition;
                return parseCondition(dateCondition, values);
            case 4:
                ContactNameCondition contactNameCondition = (ContactNameCondition) condition;
                return parseCondition(contactNameCondition, values);
            case 5:
                HourCondition hourCondition = (HourCondition) condition;
                return parseCondition(hourCondition, values);
            case 6:
                KeywordCondition keywordCondition = (KeywordCondition) condition;
                return parseCondition(keywordCondition, values);
            case 7:
                PatternCondition patternCondition = (PatternCondition) condition;
                return parseCondition(patternCondition, values);
        }
        return null;
    }

    private ICondition parseCondition(CallTypeCondition condition, JSONObject values) {
        try {
            String callType = values.getString("callType");
            condition.setCallType(callType);
            return condition;
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getMessage());
            return null;
        }
    }

    private ICondition parseCondition(NumberCondition condition, JSONObject values) {
        try {
            String conditionNumber = values.getString("callNumber");
            condition.setNumber(conditionNumber);
            return condition;
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getMessage());
            return null;
        }
    }

    private ICondition parseCondition(DayCondition condition, JSONObject values) {
        try {
            int day = values.getInt("Day");
            condition.setDay(day);
            condition.setEveryWeek(true);
            return  condition;
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getMessage());
            return null;
        }
    }

    private ICondition parseCondition(DateCondition condition, JSONObject values) {
        try {
            SimpleDateFormat dateFormater = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            String startDateString = values.getString("startDate");
            String endDateString = values.getString("endDate");
            Date startDate = dateFormater.parse(startDateString);
            Date endDate = dateFormater.parse(endDateString);
            condition.setStartDate(startDate);
            condition.setEndDate(endDate);
            return condition;
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getMessage());
            return null;
        }
    }

    private ICondition parseCondition(ContactNameCondition condition, JSONObject values) {
        try {
            String conditionContactName = values.getString("contactName");
            condition.setContactName(conditionContactName);
            return condition;
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getMessage());
            return null;
        }
    }

    private ICondition parseCondition(HourCondition condition, JSONObject values) {
        try {
            int startHour = values.getInt("startHour");
            int startMinutes = values.getInt("startMinutes");
            int endHour = values.getInt("endHour");
            int endMinutes = values.getInt("endMinutes");
            condition.setStartHour(startHour);
            condition.setStartMinutes(startMinutes);
            condition.setEndHour(endHour);
            condition.setEndMinutes(endMinutes);
            return condition;
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getMessage());
            return null;
        }
    }

    private ICondition parseCondition(KeywordCondition condition, JSONObject values) {
        try {
            String keyword = values.getString("keyword");
            condition.setKeyword(keyword);
            return condition;
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getMessage());
            return null;
        }
    }

    private ICondition parseCondition(PatternCondition condition, JSONObject values) {
        try {
            String pattern = values.getString("pattern");
            boolean match_contacts = values.getBoolean("match_contact_name");
            boolean match_number = values.getBoolean("match_number");
            boolean match_message_body = values.getBoolean("match_message_body");
            condition.setPattern(pattern);
            condition.setMatchContactName(match_contacts);
            condition.setMatchNumber(match_number);
            condition.setMatchMessageBody(match_message_body);
            return condition;
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getMessage());
            return null;
        }
    }



    //Zwraca warunek
    public ICondition getCondition(int index) {
        return m_conditionsArrayList.get(index);
    }

    public int countConditions() {
        return m_conditionsArrayList.size();
    }
}