package com.mkotlarz.mobile.productivity.phonelocker.data;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.SystemClock;

import com.mkotlarz.mobile.productivity.phonelocker.data.CallsTable.CallsColumns;
import com.mkotlarz.mobile.productivity.phonelocker.data.SmsTable.SmsColumns;
import com.mkotlarz.mobile.productivity.phonelocker.models.CallRulesModel;
import com.mkotlarz.mobile.productivity.phonelocker.models.CallsModel;
import com.mkotlarz.mobile.productivity.phonelocker.models.SMSModel;
import com.mkotlarz.mobile.productivity.phonelocker.models.SettingsModel;
import com.mkotlarz.mobile.productivity.phonelocker.models.SmsRulesModel;


import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Android DataManagerImpl to encapsulate SQL and DB details.
 * Includes SQLiteOpenHelper, and uses Dao objects
 * to create/update/delete and otherwise manipulate data.
 *
 * @author ccollins
 *
 */
public class DataManagerImpl implements DataManager {

    private Context context;

    private SQLiteDatabase db;

    private SettingsDao settingsDao;
    private CallsDao callsDao;
    private SmsDao smsDao;
    private CallRulesDao callRulesDao;
    private SmsRulesDao smsRulesDao;
    private final Logger Log = Logger.getLogger(DataManagerImpl.class);

    public DataManagerImpl(Context context) {

        this.context = context;

        SQLiteOpenHelper openHelper = new OpenHelper(this.context);
        db = openHelper.getWritableDatabase();

        settingsDao = new SettingsDao(db);
        callsDao = new CallsDao(db);
        smsDao = new SmsDao(db);
        callRulesDao = new CallRulesDao(db);
        smsRulesDao = new SmsRulesDao(db);

        Cursor stCursor = db.rawQuery("select * from " + SettingsTable.TABLE_NAME, null);
        if (!stCursor.moveToFirst()) {
            try {
                db.beginTransaction();

                // first save movie
                settingsDao.initialize();
                //Close connection
                db.setTransactionSuccessful();
                //If are errors with database connection
            } catch (SQLException ex) {
                Log.error("Error saving call (transaction rolled back)" + ex.getMessage() + "..." + ex.getStackTrace());
            } finally {
                stCursor.close();
                // an "alias" for commit
                db.endTransaction();
            }
        }
    }

    public SQLiteDatabase getDb() {
        return db;
    }

    private void openDb() {
        if (!db.isOpen()) {
            db = SQLiteDatabase.openDatabase(DataConstants.DATABASE_PATH, null, SQLiteDatabase.OPEN_READWRITE);
            // since we pass db into DAO, have to recreate DAO if db is re-opened
            settingsDao = new SettingsDao(db);
            callsDao = new CallsDao(db);
            smsDao = new SmsDao(db);
            callRulesDao = new CallRulesDao(db);
            smsRulesDao = new SmsRulesDao(db);
        }
    }

    private void closeDb() {
        if (db.isOpen()) {
            db.close();
        }
    }

    private void resetDb() {
        Log.info("Resetting database connection (close and re-open).");
        closeDb();
        SystemClock.sleep(500);
        openDb();
    }

    //
    // "Manager" data methods that wrap DAOs
    //
    // this lets us encapsulate usage of DAOs
    // we only expose methods app is actually using, and we can combine DAOs, with logic in one place
    //

    //Settings

    public SettingsModel getSettings() {
        Cursor stCursor = db.rawQuery("select * from " + SettingsTable.TABLE_NAME, null);
        stCursor.moveToFirst();
        int icalls = stCursor.getInt(stCursor.getColumnIndex("incoming_calls"));
        int ocalls = stCursor.getInt(stCursor.getColumnIndex("outgoing_calls"));
        int isms = stCursor.getInt(stCursor.getColumnIndex("incoming_sms"));
        int wifi = stCursor.getInt(stCursor.getColumnIndex("wifi"));
        int network = stCursor.getInt(stCursor.getColumnIndex("network"));
        SettingsModel stModel = new SettingsModel(icalls, ocalls, isms, wifi, network);
        stCursor.close();
        return stModel;
    }

    public int getAllSettingsState() {
        Cursor stCursor = db.rawQuery("select * from " + SettingsTable.TABLE_NAME, null);
        stCursor.moveToFirst();
        int icalls = stCursor.getInt(stCursor.getColumnIndex("incoming_calls"));
        int ocalls = stCursor.getInt(stCursor.getColumnIndex("outgoing_calls"));
        int isms = stCursor.getInt(stCursor.getColumnIndex("incoming_sms"));
        int wifi = stCursor.getInt(stCursor.getColumnIndex("wifi"));
        int network = stCursor.getInt(stCursor.getColumnIndex("network"));
        stCursor.close();
        if (icalls == 1 && ocalls == 1 && isms == 1 && wifi == 1 && network == 1) {
            return 1;
        } else {
            return 0;
        }
    }

    public void setAllToValue(int value) {
        settingsDao.changeAllToValue(value);
    }

    @Override
    public List<String> getSettingsHeaders() {
        List<String> list = new ArrayList<String>();
        list.add(1, SettingsTable.SettingsColumns.INCOMING_CALLS);
        list.add(2, SettingsTable.SettingsColumns.OUTGOING_CALLS);
        list.add(3, SettingsTable.SettingsColumns.INCOMING_SMS);
        return list;
    }

    @Override
    public int getIncomingCallsState() {
        Cursor stCursor = db.rawQuery("select incoming_calls from " + SettingsTable.TABLE_NAME, null);
        stCursor.moveToFirst();
        int icalls = stCursor.getInt(stCursor.getColumnIndex("incoming_calls"));
        stCursor.close();
        return icalls;
    }

    @Override
    public int getOutgoingCallsState() {
        Cursor stCursor = db.rawQuery("select outgoing_calls from " + SettingsTable.TABLE_NAME, null);
        stCursor.moveToFirst();
        int ocalls = stCursor.getInt(stCursor.getColumnIndex("outgoing_calls"));
        stCursor.close();
        return ocalls;
    }

    public int findSettingsByName(String name) {
        try {
            Cursor stCursor = db.rawQuery("select " + name + " from " + SettingsTable.TABLE_NAME, null);
            stCursor.moveToFirst();
            int name_state = stCursor.getInt(stCursor.getColumnIndex(name));
            stCursor.close();
            return name_state;
        } catch (Exception ex) {
            Log.error("Column name error" + ex.getMessage() + "..." + ex.getStackTrace());
            return 2;
        }
    }

    @Override
    public void saveSelected(String column_name, int state) {
        settingsDao.saveSelected(column_name, state);
    }

    @Override
    public int getIncomingSmsState() {
        Cursor stCursor = db.rawQuery("select incoming_sms from " + SettingsTable.TABLE_NAME, null);
        stCursor.moveToFirst();
        int isms = stCursor.getInt(stCursor.getColumnIndex("incoming_sms"));
        stCursor.close();
        return isms;
    }


    // SMS
    @Override
    public SMSModel findSMSbyID(long smsId) {
        String phoneNumber = "", date = " ", body = "", alias = "", matchedOn = "";
        int typ = 0;
        SMSModel sms;
        String sql = "select * from " + SmsTable.TABLE_NAME + " ORDER BY " + SmsColumns._ID + " DESC  LIMIT " + smsId + ",1";
        Cursor smsCursor = db.rawQuery(sql, null);
        if (smsCursor.moveToFirst()) {
            phoneNumber = smsCursor.getString(smsCursor.getColumnIndex(SmsColumns.PHONE_NUMBER));
            date = smsCursor.getString(smsCursor.getColumnIndex(SmsColumns.DATE));
            body = smsCursor.getString(smsCursor.getColumnIndex(SmsColumns.BODY));
            alias = smsCursor.getString(smsCursor.getColumnIndex(SmsColumns.ALIAS));
            typ = smsCursor.getInt(smsCursor.getColumnIndex(SmsColumns.typ));
            matchedOn = smsCursor.getString(smsCursor.getColumnIndex(SmsColumns.MATCHED_ON));
        }
        smsCursor.close();
        sms = new SMSModel(phoneNumber, body, date, typ, alias, matchedOn);
        return sms;
    }

    @Override
    public String[][] getAllSMS() {
        // these movies don't have categories, but they're really used as "headers" anyway, it's ok
        return smsDao.getAllSMS();
    }

    @Override
    public List<SMSModel> findSMSbyNumber(long number) {
        return smsDao.getSMSbyNumber(number);
    }

    @Override
    public long saveSMS(SMSModel sms) {


        // NOTE could wrap entity manip functions in DataManagerImpl, make "manager" for each entity
        // here though, to keep it simpler, we use the DAOs directly (even when multiple are involved)
        long smsId = 0;

        // put it in a transaction, since we're touching multiple tables
        try {
            db.beginTransaction();

            // first save movie
            smsId = smsDao.save(sms);
            //Close connection
            db.setTransactionSuccessful();
            //If are errors with database connection
        } catch (SQLException ex) {
            Log.error("Error saving call (transaction rolled back) " + ex.getMessage() + "..." + ex.getStackTrace());
            smsId = 0;
        } finally {
            // an "alias" for commit
            db.endTransaction();
        }
        return smsId;
    }

    public boolean deleteSMS(long smsId) {
      /*boolean result = false;
      // NOTE switch this order around to see constraint error (foreign keys work)
      try {
         db.beginTransaction();
         // make sure to use getMovie and not movieDao directly, categories need to be included
         SMSModel sms = new SMSModel(smsId);
         if (sms != null) {
            SmsDao.deleteSMS(sms);
         }
         db.setTransactionSuccessful();
         result = true;
      } catch (SQLException e) {
         Log.e("Error deleting movie (transaction rolled back)", e);
      } finally {
         db.endTransaction();
      }
      return result;*/
        return false;
    }

    public Cursor getSMSCursor() {
        // note that query MUST have a column named _id
        return db.rawQuery("select " + SmsColumns._ID + ", " + SmsColumns.PHONE_NUMBER
                + ", " + SmsColumns.BODY + ", " + SmsColumns.DATE + " from " + SmsTable.TABLE_NAME, null);
    }


    //Calls


    @Override
    public CallsModel findCallbyID(long callId) {
        String phoneNumber = "", date = "", alias = "", matchedOn = "";
        int typ = 0;
        CallsModel call;
        String sql = "select " + CallsColumns._ID + ", " + CallsColumns.PHONE_NUMBER
                + ", " + CallsColumns.DATE + ", " + CallsColumns.TYP + ", " + CallsColumns.ALIAS + ", " + CallsColumns.MATCHED_ON +  " from " + CallsTable.TABLE_NAME + " ORDER BY " + CallsColumns._ID + " DESC  LIMIT " + callId + ",1";
        Cursor callsCursor = db.rawQuery(sql, null);
        if (callsCursor.moveToFirst()) {
            phoneNumber = callsCursor.getString(callsCursor.getColumnIndex(CallsColumns.PHONE_NUMBER));
            date = callsCursor.getString(callsCursor.getColumnIndex(CallsColumns.DATE));
            typ = callsCursor.getInt(callsCursor.getColumnIndex(CallsColumns.TYP));
            alias = callsCursor.getString(callsCursor.getColumnIndex(CallsColumns.ALIAS));
            matchedOn = callsCursor.getString(callsCursor.getColumnIndex(CallsColumns.MATCHED_ON));
        }
        callsCursor.close();
        call = new CallsModel(phoneNumber, date, typ, alias, matchedOn);
        return call;
    }

    @Override
    public String[][] getAllCalls() {
        // these movies don't have categories, but they're really used as "headers" anyway, it's ok
        return callsDao.getAllCalls();
    }

    @Override
    public List<CallsModel> findCallsbyNumber(long number) {
        return callsDao.getCallsbyNumber(number);
    }

    @Override
    public long saveCall(CallsModel call) {

        // NOTE could wrap entity manip functions in DataManagerImpl, make "manager" for each entity
        // here though, to keep it simpler, we use the DAOs directly (even when multiple are involved)
        long callId = 0L;

        // put it in a transaction, since we're touching multiple tables
        try {
            db.beginTransaction();

            // first save movie
            callId = callsDao.save(call);
            //Close connection
            db.setTransactionSuccessful();
            //If are errors with database connection
        } catch (SQLException ex) {
            Log.error("Error saving call (transaction rolled back) " + ex.getMessage() + "..." + ex.getStackTrace());
            callId = 0L;
        } finally {
            // an "alias" for commit
            db.endTransaction();
        }

        return callId;
    }

    @Override
    public boolean deleteCall(long callId) {
        boolean result = false;
        // NOTE switch this order around to see constraint error (foreign keys work)
        try {
            db.beginTransaction();
            // make sure to use getMovie and not movieDao directly, categories need to be included
            CallsModel call = new CallsModel(callId);
            CallsDao.deleteCall(call);
            db.setTransactionSuccessful();
            result = true;
        } catch (SQLException ex) {
            Log.error("Error deleting movie (transaction rolled back) " + ex.getMessage() + "..." + ex.getStackTrace());
        } finally {
            db.endTransaction();
        }
        return result;
    }

    public Cursor getCallCursor() {
        // note that query MUST have a column named _id
        return db.rawQuery("select " + CallsColumns._ID + ", " + CallsColumns.PHONE_NUMBER
                + " from " + CallsTable.TABLE_NAME, null);
    }

    public String[][] getAllCallRules() {
        return callRulesDao.getAllRules();
    }

    public String[][] getAllActiveCallRules() {
        return callRulesDao.getAllActiveRules();
    }

    public CallRulesModel findCallRuleByPosition(long position) {
        String ruleName = "", date = " ", ruleConditions = "";
        int enabled = 0;
        int id = 0;
        CallRulesModel cModel;
        String sql = "select * from " + CallRulesTable.TABLE_NAME + " ORDER BY " + CallRulesTable.CallRulesColumns._ID + " ASC  LIMIT " + position + ",1";
        Cursor callCursor = db.rawQuery(sql, null);
        if (callCursor.moveToFirst()) {
            ruleName = callCursor.getString(callCursor.getColumnIndex(CallRulesTable.CallRulesColumns.RULENAME));
            date = callCursor.getString(callCursor.getColumnIndex(CallRulesTable.CallRulesColumns.DATE));
            ruleConditions = callCursor.getString(callCursor.getColumnIndex(CallRulesTable.CallRulesColumns.RULECONDITIONS));
            enabled = callCursor.getInt(callCursor.getColumnIndex(CallRulesTable.CallRulesColumns.RULEACTIVE));
            id = callCursor.getInt(callCursor.getColumnIndex(CallRulesTable.CallRulesColumns._ID));
        }
        callCursor.close();
        cModel = new CallRulesModel(ruleName, ruleConditions, enabled, date);
        cModel.setId(id);
        return cModel;
    }

    public CallRulesModel saveCallRule(String ruleName, Date date, String ruleConditions, boolean active) {
        CallRulesModel cRulesModel = new CallRulesModel();
        cRulesModel.setRuleName(ruleName);
        cRulesModel.setDate(date);
        cRulesModel.setRuleConditions(ruleConditions);
        if (active)
            cRulesModel.setRuleActive(1);
        else
            cRulesModel.setRuleActive(0);
        callRulesDao.save(cRulesModel);
        return cRulesModel;
    }

    public CallRulesModel editCallRule(long id, String ruleName, String ruleConditions, boolean active) {
        CallRulesModel callRulesModel = new CallRulesModel();
        callRulesModel.setId(id);
        callRulesModel.setRuleName(ruleName);
        callRulesModel.setRuleConditions(ruleConditions);
        if (active)
            callRulesModel.setRuleActive(1);
        else
            callRulesModel.setRuleActive(0);
        callRulesDao.update(callRulesModel);
        return callRulesModel;
    }

    public CallRulesModel editCallRule(CallRulesModel cModel) {
        callRulesDao.update(cModel);
        return cModel;
    }

    public boolean deleteCallRule(CallRulesModel cModel) {
        callRulesDao.deleteCallRule(cModel);
        return true;
    }

    public String[][] getAllSmsRules() {
        return smsRulesDao.getAllRules();
    }

    public String[][] getAllActiveSmsRules() {
        return smsRulesDao.getAllActiveRules();
    }

    public SmsRulesModel findSmsRuleByPosition(long position) {
        String ruleName = "", date = " ", ruleConditions = "";
        int enabled = 0;
        int id = 0;
        SmsRulesModel sModel;
        String sql = "select * from " + SmsRulesTable.TABLE_NAME + " ORDER BY " + SmsRulesTable.SmsRulesColumns._ID + " ASC  LIMIT " + position + ",1";
        Cursor smsCursor = db.rawQuery(sql, null);
        if (smsCursor.moveToFirst()) {
            ruleName = smsCursor.getString(smsCursor.getColumnIndex(SmsRulesTable.SmsRulesColumns.RULENAME));
            date = smsCursor.getString(smsCursor.getColumnIndex(SmsRulesTable.SmsRulesColumns.DATE));
            ruleConditions = smsCursor.getString(smsCursor.getColumnIndex(SmsRulesTable.SmsRulesColumns.RULECONDITIONS));
            enabled = smsCursor.getInt(smsCursor.getColumnIndex(SmsRulesTable.SmsRulesColumns.RULEACTIVE));
            id = smsCursor.getInt(smsCursor.getColumnIndex(SmsRulesTable.SmsRulesColumns._ID));
        }
        smsCursor.close();
        sModel = new SmsRulesModel(ruleName, ruleConditions, enabled, date);
        sModel.setId(id);
        return sModel;
    }

    public SmsRulesModel saveSmsRule(String ruleName, Date date, String ruleConditions, boolean active) {
        SmsRulesModel sRulesModel = new SmsRulesModel();
        sRulesModel.setRuleName(ruleName);
        sRulesModel.setDate(date);
        sRulesModel.setRuleConditions(ruleConditions);
        if (active)
            sRulesModel.setRuleActive(1);
        else
            sRulesModel.setRuleActive(0);
        smsRulesDao.save(sRulesModel);
        return sRulesModel;
    }

    public SmsRulesModel editSmsRule(long id, String ruleName, String ruleConditions, boolean active) {
        SmsRulesModel smsRulesModel = new SmsRulesModel();
        smsRulesModel.setId(id);
        smsRulesModel.setRuleName(ruleName);
        smsRulesModel.setRuleConditions(ruleConditions);
        if (active)
            smsRulesModel.setRuleActive(1);
        else
            smsRulesModel.setRuleActive(0);
        smsRulesDao.update(smsRulesModel);
        return smsRulesModel;
    }

    public SmsRulesModel editSmsRule(SmsRulesModel sModel) {
        smsRulesDao.update(sModel);
        return sModel;
    }

    public boolean deleteSmsRule(SmsRulesModel sModel) {
        smsRulesDao.deleteSmsRule(sModel);
        return true;
    }

}