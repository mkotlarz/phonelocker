package com.mkotlarz.mobile.productivity.phonelocker.sms;

import android.app.Fragment;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;


import com.mkotlarz.mobile.productivity.phonelocker.R;
import com.mkotlarz.mobile.productivity.phonelocker.adapter.ConversationDetailAdapter;
import com.mkotlarz.mobile.productivity.phonelocker.data.Constants;
import com.mkotlarz.mobile.productivity.phonelocker.sms.models.Conversation;
import com.mkotlarz.mobile.productivity.phonelocker.sms.models.DataProvider;
import com.mkotlarz.mobile.productivity.phonelocker.sms.models.Message;

import org.apache.log4j.Logger;

import java.util.Date;
import java.util.Observable;
import java.util.Observer;

public class ConversationDetailFragment extends Fragment implements Observer {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    /**
     * The dummy content this fragment is presenting.
     */
    private Conversation mItem;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */

    private final Logger Log = Logger.getLogger(ConversationDetailFragment.class);

    public ConversationDetailFragment() {
    }

    private ConversationDetailAdapter messageAdapter;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
            mItem = DataProvider.getInstance().getConversationMap().get(getArguments().getString(ARG_ITEM_ID));
            messageAdapter = new ConversationDetailAdapter(getActivity().getLayoutInflater(), mItem, getActivity().getApplicationContext());
        // Register as a listener
        DataProvider.getInstance().addObserver(this);
        View rootView = inflater.inflate(R.layout.fragment_conversation_details, container, false);
        ListView listView = (ListView) rootView.findViewById(R.id.conversationDeatailListView);
        listView.setAdapter(messageAdapter);
        listView.setSelection(listView.getCount()-1);
        listView.setDividerHeight(0);

        ImageView sendReply = (ImageView) rootView.findViewById(R.id.sendReplyButton);
        sendReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onReplyButtonClick();
            }
        });
        getActivity().getActionBar().setTitle(mItem.toString());
        return rootView;
    }

    @Override
    public void onDestroy() {
        // Remove ourself as a listener
        DataProvider.getInstance().deleteObserver(this);
        super.onDestroy();
    }

    @Override
    public void update(Observable observable, Object data) {
        if(messageAdapter != null) {
            messageAdapter.notifyDataSetChanged();
            ListView listView = (ListView) getActivity().findViewById(R.id.conversationDeatailListView);
            listView.setSelection(listView.getCount()-1);
        }
    }

    public void onReplyButtonClick() {
        SmsManager manager = SmsManager.getDefault();
        try
        {
            EditText editText = (EditText) getActivity().findViewById(R.id.replyMessage);
            String phoneNumber = mItem.getSender();
            String message_body = editText.getText().toString();
            manager.sendTextMessage(phoneNumber, null, message_body, null, null);
            ContentValues values = new ContentValues();
            values.put(Constants.SMS_ADDRESS, phoneNumber);
            values.put(Constants.SMS_BODY, message_body);
            Uri messageAdded = getActivity().getContentResolver().insert(Uri.parse(Constants.SMS_SENT_URI), values);
            Cursor addedMessageCursor = getActivity().getContentResolver().query(messageAdded, null, null, null, null);
            int id = 0;
            if(addedMessageCursor.moveToFirst()) {
                id = addedMessageCursor.getInt(addedMessageCursor.getColumnIndex(Telephony.Sms._ID));
            }
            addedMessageCursor.close();
            editText.setText(null);
            Log.info("SMS message has been send to: " + phoneNumber);
            Message message = new Message(id, message_body, phoneNumber, mItem.toString(), "recipient", new Date(), Telephony.Sms.MESSAGE_TYPE_SENT, 1,mItem.getThreadId());
            DataProvider.getInstance().addMessage(message, getActivity().getApplicationContext());
            getActivity().findViewById(R.id.conversationDeatailListView).requestFocus();
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getStackTrace());
        }
    }
}