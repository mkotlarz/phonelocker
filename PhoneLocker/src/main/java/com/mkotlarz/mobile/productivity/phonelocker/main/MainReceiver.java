package com.mkotlarz.mobile.productivity.phonelocker.main;

import com.mkotlarz.mobile.productivity.phonelocker.CallsReceiver;
import com.mkotlarz.mobile.productivity.phonelocker.LockerMain;
import com.mkotlarz.mobile.productivity.phonelocker.NetReceiver;
import com.mkotlarz.mobile.productivity.phonelocker.R;
import com.mkotlarz.mobile.productivity.phonelocker.SmsReceiver;
import com.mkotlarz.mobile.productivity.phonelocker.data.Constants;
import com.mkotlarz.mobile.productivity.phonelocker.data.DataManagerImpl;
import com.mkotlarz.mobile.productivity.phonelocker.interfaces.ICondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.SettingsModel;
import com.mkotlarz.mobile.productivity.phonelocker.sms.models.DataProvider;
import com.mkotlarz.mobile.productivity.phonelocker.sms.models.Message;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;

import java.sql.Date;
import android.annotation.TargetApi;

/*
Głowny odbiornik aplikacji PhoneLocker, tu wlasnie znajduje sie kod odpowiedzialny za decyzje jaka akcje ma podjac program,
na zasadzie danych przekazanych do odbiornika w intencie lub TelephonyManagerze.
@version: 1.0.0
@author maciej_kotlarz
 */
public class MainReceiver extends BroadcastReceiver {
	private static final String CALLS_ACTION = 	        "android.intent.action.PHONE_STATE";
    private static final String OCALLS_ACTION =         "android.intent.action.NEW_OUTGOING_CALL";
	private static final String RECEIVE_SMS = 			"android.provider.Telephony.SMS_RECEIVED";
    private static final String WIFI_ACTION =           "android.net.wifi.WIFI_STATE_CHANGED";
    private static final String NETWORK_ACTION =        "android.net.conn.CONNECTIVITY_CHANGE";
    private RulesExecutor m_rulesExecutor = null;
	CallsReceiver cReceiver = new CallsReceiver();
	SmsReceiver SMSReceiver = new SmsReceiver();
    DataManagerImpl dm;
    private Context m_context = null;

	 @Override
    public void onReceive(Context context, Intent intent) {
        String callNumber = null;
         String messageBody = null;
        m_context = context;
        String action = intent.getAction();
        //Tworzenie modelu zawierajacego aktualne dane na temat blokowania polaczen
        dm = new DataManagerImpl(context);
        m_rulesExecutor = new RulesExecutor(dm, context);
        SettingsModel stModel = dm.getSettings();
        //Akcja dotyczy odrzucania polaczen glosowych i zapisu polaczen przych do bazy
        if (action.equals(MainReceiver.CALLS_ACTION)) {
            abortBroadcast();
            int type = 2;
            TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            Bundle extras = intent.getExtras();
            String call_type = extras.getString(TelephonyManager.EXTRA_STATE);
            if ((call_type.equals("RINGING") && stModel.getIncomingCallsState() == 1) || (call_type.equals("OFFHOOK") && stModel.getOutgoingCallsState() == 1)) {
                if (call_type.equals("RINGING")) {
                    type = 0;
                    callNumber = extras
                            .getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
                }
                if (call_type.equals("OFFHOOK")) {
                    type = 1;
                    callNumber = extras.getString(intent.EXTRA_PHONE_NUMBER);
                }
                messageBody = null;
                if(m_rulesExecutor.Process(type, callNumber, messageBody))
                    cReceiver.abortCall(context, intent, type, telephony, m_rulesExecutor.MatchedCondition);
            }
        }
        //Akcja dotyczy zapisu do bazy polaczen wychodzacych
        else if(action.equals(MainReceiver.OCALLS_ACTION) && stModel.getOutgoingCallsState() == 1) {
            if(m_rulesExecutor.Process(1, callNumber, messageBody))
                cReceiver.saveOCall(context, intent, m_rulesExecutor.MatchedCondition);
        }
        //Akcja dotyczy smsow przychodzacych
        else if (action.equals(MainReceiver.RECEIVE_SMS)) {
            Bundle pudsBundle = intent.getExtras();
            Object[] pdus = (Object[]) pudsBundle.get("pdus");
            SmsMessage messages = SmsMessage.createFromPdu((byte[]) pdus[0]);
            if (stModel.getIncimongSmsState() == 1) {
                callNumber = messages.getOriginatingAddress();
                messageBody = messages.getMessageBody();
                if (m_rulesExecutor.Process(2, callNumber, messageBody)) {
                    abortBroadcast();
                    SMSReceiver.incomingSMS(context, intent, m_rulesExecutor.MatchedCondition);
                } else {
                    if(Build.VERSION.SDK_INT >= 19)
                        addSmsToInbox(messages);
                }
            } else {
                if(Build.VERSION.SDK_INT >= 19)
                    addSmsToInbox(messages);
            }
        }
        else if (action.equals(MainReceiver.WIFI_ACTION) && stModel.getWifiState() == 1) {
            NetReceiver.disableWiFi(context);
        } else if (action.equals(MainReceiver.NETWORK_ACTION) && stModel.getNetworkState() == 1) {
            NetReceiver.disableNetwork(context);
        }
    }

    @TargetApi(19)
    private boolean CheckIfAppIsDefaultAppForSms() {
        final String packageName = m_context.getPackageName();
        return Telephony.Sms.getDefaultSmsPackage(m_context).equals(packageName);
    }

    private void addSmsToInbox(SmsMessage message) {
        if(!CheckIfAppIsDefaultAppForSms())
            return;
        ContentValues values = new ContentValues();
        values.put(Constants.SMS_ADDRESS, message.getOriginatingAddress());
        values.put(Constants.SMS_DATE, System.currentTimeMillis() + "");
        values.put(Constants.SMS_BODY, message.getMessageBody());
        values.put(Constants.SMS_TYPE, Constants.SMS_INCOMING_TYPE_VALUE);
        Uri messageUri = m_context.getContentResolver().insert(Uri.parse(Constants.SMS_URI), values);
        Cursor messageCursor = m_context.getContentResolver().query(messageUri, null, null, null, null);
        int id = 0;
        int thread_id = 0;
        if(messageCursor.moveToFirst()) {
            id = messageCursor.getInt(messageCursor.getColumnIndex(Telephony.Sms._ID));
            thread_id = messageCursor.getInt(messageCursor.getColumnIndex(Telephony.Sms.Conversations.THREAD_ID));
        }
        messageCursor.close();
        Message messageToAdd = new Message(id, message.getMessageBody(), message.getOriginatingAddress(), message.getDisplayOriginatingAddress(), "recipient", new Date(System.currentTimeMillis()),
                                            Constants.SMS_INCOMING_TYPE_VALUE, 0, thread_id);
        DataProvider.getInstance().addMessage(messageToAdd, m_context);
        DataProvider.getInstance().updateMessagesList(m_context);
        showNotification(messageToAdd);
    }

    private void showNotification(Message message) {
        String messageContent = "";
        int mId = 0;
        if(message.getContent().length() < 80)
            messageContent = message.getContent();
        else
            messageContent = message.getContent().substring(0, 80);
        String contactName =  message.searchContactName(message.getSender(), m_context);
        if(contactName != null && !contactName.equals(""))
            contactName =  " - " + contactName;
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(m_context)
                        .setSmallIcon(R.drawable.notification_icon)
                        .setContentTitle(m_context.getString(R.string.new_message_event_notyfication) + contactName)
                        .setContentText(messageContent);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setVibrate(Constants.VIBRATE_PATTERN);
        mBuilder.setLights(Constants.LIGHT_COLOR, Constants.LIGHT_ON, Constants.LIGHT_OFF);
        mBuilder.setSound(alarmSound);

        Intent resultIntent = new Intent(m_context, LockerMain.class);
        Bundle extras = new Bundle();
        extras.putInt("showFragment", 1);
        extras.putInt("removeNotification", mId);
        resultIntent.putExtras(extras);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(m_context);
        stackBuilder.addParentStack(LockerMain.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) m_context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(mId, mBuilder.build());
    }
}