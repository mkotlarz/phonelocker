package com.mkotlarz.mobile.productivity.phonelocker;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Telephony;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.mkotlarz.mobile.productivity.phonelocker.data.DataManagerImpl;
import com.mkotlarz.mobile.productivity.phonelocker.models.SettingsModel;
import com.mkotlarz.mobile.productivity.phonelocker.sms.ComposeSmsActivity;
import com.mkotlarz.mobile.productivity.phonelocker.sms.ConversationListFragment;
import com.mkotlarz.mobile.productivity.phonelocker.utils.SendHttpRequest;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.Random;


public class LockerMain extends Activity {

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private String mDrawerTitle;
    private CharSequence mTitle = "PhoneLocker";
    private String[] mOptionTitles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Build.VERSION.SDK_INT >= 19) {
            if (!Telephony.Sms.getDefaultSmsPackage(this).equals(getPackageName())) {
                Intent intent = new Intent(this, DefaultSmsAppActivity.class);
                startActivity(intent);
            }
        }
        setContentView(R.layout.side_menu);
        mOptionTitles = getResources().getStringArray(R.array.drawer_options_list);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mOptionTitles));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        mDrawerTitle = getString(R.string.home_screen);
        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        Bundle extras = getIntent().getExtras();
        int position = 0;
        try {
            position = extras.getInt("showFragment");
        } catch (Exception ex) {
            //Nic nie rób
        }
        try {
            int notificationId = extras.getInt("removeNotyfication");
            removeNotification(notificationId);
        } catch (Exception ex)  {
            //Nic nie rob
        }
        selectItem(position);
        BuildFeatureRequestMessageBox();
    }

    private void BuildFeatureRequestMessageBox() {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if(!prefs.contains("feature_request_has_been_sent"))
            CreatePrefsKey(prefs);
        boolean featureRequestSent = prefs.getBoolean("feature_request_has_been_sent", false);
        Random random = new Random();
        int random_int = random.nextInt(10);
        if(featureRequestSent == false && isOnline() && random_int == 7) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(LockerMain.this);
            builder.setTitle(getResources().getString(R.string.feature_request_dialog_title));
            final EditText featureRequestEditTextBox = new EditText(this);
            featureRequestEditTextBox.setHint(getResources().getString(R.string.feature_request_dialog_hint));
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            featureRequestEditTextBox.setLayoutParams(lp);
            builder.setView(featureRequestEditTextBox);
            builder.setPositiveButton(getString(R.string.compose_send), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    sendFeatureRequest(featureRequestEditTextBox.getText().toString(), getResources().getConfiguration().locale.getCountry());
                    ChangeSharedPreferencesKey(prefs, "feature_request_has_been_sent", true);
                    dialog.cancel();
                }
            });
            builder.setNegativeButton(getString(R.string.compose_cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            builder.show();
        }
    }

    private void ChangeSharedPreferencesKey(SharedPreferences prefs, String key, boolean value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    private void sendFeatureRequest(String content, String country) {
        new SendHttpRequest().execute(new String[]{content, country});
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    private void CreatePrefsKey(SharedPreferences prefs) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("feature_request_has_been_sent", false);
        editor.commit();
    }

//    private void doCheck() {
//        mChecker.checkAccess(mLicenseCheckerCallback);
//    }

    private void removeNotification(int id) {
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(id);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionbar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action buttons
        switch (item.getItemId()) {
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }


    //Wybiera opcje z navigation drawera oraz ustawia odpowiedni fragment
    public void selectItem(int position) {
        Fragment fragment = null;
        // update the main content by replacing fragments
        switch (position) {
            case 0:
                fragment = new PhoneLockerFragment();
                break;
            case 1:
                fragment = new ConversationListFragment();
                break;
            case 2:
                fragment = new CallEventsFragment();
                break;
            case 3:
                fragment = new SmsEventsFragment();
                break;
            case 4:
                fragment = new CallRulesFragment();
                break;
            case 5:
                fragment = new SmsRulesFragment();
                break;
            case 6:
                fragment = new AutoresponderSettingsFragment();
                break;
            case 7:
                fragment = new SettingsFragment();
                break;
        }
        if(fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
        }


        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);
        setTitle(mOptionTitles[position]);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    public static class PhoneLockerFragment extends Fragment {

        private Context m_ctx = null;
        private DataManagerImpl dm = null;
        private SettingsModel stModel = null;

        public PhoneLockerFragment() {
        }

        // Składowe klasy inicjujemy dopiero w tej metodzie ponieważ getActivity.getApplicationContext() nie jest dostępne w konstruktorze
        // Odpowiada ona za przygotowanie wyswietlanego widoku
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            m_ctx = getActivity().getApplicationContext();
            dm = new DataManagerImpl(m_ctx);
            stModel = new SettingsModel();
            View rootView = inflater.inflate(R.layout.fragment_locker_main, container, false);
            InitializeButtons(rootView);
            InitializeRulesCounters(rootView);
            InitializeAddConditionButtonsIfPossible(rootView);
            return rootView;
        }


        /*
        * @param view root view off fragment
        * Nadaje wartości zapisane w bazie(true/false) dla switchów odpowiedzialnych za ustawienia blokowania zdarzeń(wystąpień)
        */
        private void InitializeButtons(View view) {
            //Switches
            final View rootView = view;
            Switch CallsSwitch = (Switch) rootView.findViewById(R.id.block_calls_switch);
            Switch SmsSwitch = (Switch) rootView.findViewById(R.id.block_sms_switch);
            Switch wifiSwitch = (Switch) rootView.findViewById(R.id.block_wifi_switch);
            Switch networkSwitch = (Switch) rootView.findViewById(R.id.block_3g_switch);
            FloatingActionButton mFabSmsButton = (FloatingActionButton) rootView.findViewById(R.id.CreateSmsFloatingButton);
            FloatingActionButton mFabCallConditionButton = (FloatingActionButton) rootView.findViewById(R.id.CreateCallConditionFloatingButton);
            FloatingActionButton mFabSmsConditionButton = (FloatingActionButton) rootView.findViewById(R.id.CreateSmsConditionFloatingButton);
            dm = new DataManagerImpl(m_ctx);
            stModel = dm.getSettings();
            if (stModel.getOutgoingCallsState() == 1 && stModel.getIncomingCallsState() == 1) {
                CallsSwitch.setChecked(true);
            }
            if (stModel.getIncimongSmsState() == 1) {
                SmsSwitch.setChecked(true);
            }
            if (stModel.getWifiState() == 1) {
                wifiSwitch.setChecked(true);
            }
            if (stModel.getNetworkState() == 1) {
                networkSwitch.setChecked(true);
            }

            CallsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        dm.saveSelected("incoming_calls", 1);
                        dm.saveSelected("outgoing_calls", 1);
                    } else {
                        dm.saveSelected("incoming_calls", 0);
                        dm.saveSelected("outgoing_calls", 0);
                    }
                }
            });

            SmsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        dm.saveSelected("incoming_sms", 1);
                    } else {
                        dm.saveSelected("incoming_sms", 0);
                    }
                }
            });

            wifiSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if (isChecked) {
                        dm.saveSelected("wifi", 1);
                    } else {
                        dm.saveSelected("wifi", 0);
                    }
                }
            });

            networkSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if (isChecked) {
                        dm.saveSelected("network", 1);
                    } else {
                        dm.saveSelected("network", 0);
                    }
                }
            });

            //Floating Action Buttons
            mFabSmsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getActivity().getApplicationContext(), ComposeSmsActivity.class));
                }
            });
            mFabSmsButton.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.create_new_sms_floating_button_tooltip),
                            Toast.LENGTH_LONG).show();
                    return false;
                }
            });


            mFabCallConditionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity().getApplicationContext(), CreateRuleActivity.class);
                    Bundle extras = new Bundle();
                    extras.putString("action", "call");
                    intent.putExtras(extras);
                    startActivity(intent);
                }
            });
            mFabCallConditionButton.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.create_new_call_rule_floating_button_tooltip),
                            Toast.LENGTH_LONG).show();
                    return false;
                }
            });


            mFabSmsConditionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity().getApplicationContext(), CreateRuleActivity.class);
                    Bundle extras = new Bundle();
                    extras.putString("action", "sms");
                    intent.putExtras(extras);
                    startActivity(intent);
                }
            });
            mFabSmsConditionButton.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.create_new_sms_rule_floating_button_tooltip),
                            Toast.LENGTH_LONG).show();
                    return false;
                }
            });
    }

        /*
        * @param view root view off fragment
        * Tworzy aktywność która jest odpowiedzialna za tworzenie warunku
        * action:
        * call - połączenie
        * sms - sms
        */

        private void InitializeRulesCounters(View view) {
            TextView callRulesCounter = (TextView) view.findViewById(R.id.active_call_rules_number);
            TextView smsRulesCounter = (TextView) view.findViewById(R.id.active_sms_rules_number);
            callRulesCounter.setText(String.format("%s", dm.getAllActiveCallRules().length));
            smsRulesCounter.setText(String.format("%s", dm.getAllActiveSmsRules().length));
        }

        /*
        * @param view root view off fragment
        * Tworzy aktywność która jest odpowiedzialna za tworzenie warunku
        * action:
        * call - połączenie
        * sms - sms
        */

        private void InitializeAddConditionButtonsIfPossible(View view) {
            TextView addCallsConditionsButton = (TextView) view.findViewById(R.id.addCallConditionButton);
            TextView addSmsConditionButton = (TextView) view.findViewById(R.id.addSmsConditionButton);
            if(addCallsConditionsButton != null) {
                addCallsConditionsButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), CreateRuleActivity.class);
                        Bundle extras = new Bundle();
                        extras.putString("action", "call");
                        intent.putExtras(extras);
                        startActivity(intent);
                    }
                });
            }

            if(addSmsConditionButton != null) {
                addSmsConditionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), CreateRuleActivity.class);
                        Bundle extras = new Bundle();
                        extras.putString("action", "sms");
                        intent.putExtras(extras);
                        startActivity(intent);
                    }
                });
            }
        }


        private boolean onClickChange(View v, MotionEvent event) {
            if(event.getAction() == MotionEvent.ACTION_DOWN) {
                v.setBackgroundResource(R.drawable.add_condition_background_hover);
                return true;
            }
            if(event.getAction() == MotionEvent.ACTION_UP) {
                v.setBackgroundResource(R.drawable.add_condition_background);
                return true;
            }
            return false;
        }
  }
}