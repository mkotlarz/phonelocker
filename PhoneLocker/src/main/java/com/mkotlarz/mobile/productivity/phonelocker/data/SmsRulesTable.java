package com.mkotlarz.mobile.productivity.phonelocker.data;

import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public final class SmsRulesTable {

    public static final String TABLE_NAME = "sms_rules";

    public static class SmsRulesColumns implements BaseColumns {
        public static final String RULENAME = "rule_name";
        public static final String DATE = "date";
        public static final String RULECONDITIONS = "rule_conditions";
        public static final String RULEACTIVE = "active";
    }

    public static void onCreate(SQLiteDatabase db) {
        db.execSQL(CreateTableSql());
    }

    public static String CreateTableSql() {
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE " + SmsRulesTable.TABLE_NAME + " (");
        sb.append(BaseColumns._ID + " INTEGER PRIMARY KEY, ");
        sb.append(SmsRulesColumns.RULENAME + " VARCHAR, ");
        sb.append(SmsRulesColumns.DATE + " TEXT, "); // movie names aren't unique, but for simplification we constrain
        sb.append(SmsRulesColumns.RULECONDITIONS + " TEXT, ");
        sb.append(SmsRulesColumns.RULEACTIVE + " INT");
        sb.append(");");
        return sb.toString();
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(newVersion == 1) {
            db.execSQL("DROP TABLE IF EXISTS " + SmsTable.TABLE_NAME);
            SmsTable.onCreate(db);
        }
    }

}
