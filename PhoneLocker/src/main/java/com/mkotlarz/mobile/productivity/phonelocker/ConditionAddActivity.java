package com.mkotlarz.mobile.productivity.phonelocker;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.mkotlarz.mobile.productivity.phonelocker.R;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.CallTypeCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.ContactNameCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.DateCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.DayCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.HourCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.KeywordCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.NumberCondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.conditions.PatternCondition;
import com.mkotlarz.mobile.productivity.phonelocker.utils.CondtionDetails;

import java.io.Serializable;
import java.sql.Date;

import info.hoang8f.widget.FButton;

public class ConditionAddActivity extends Activity {

    private int selectedOption = 0;
    private FButton m_saveButton = null;
    private CondtionDetails m_conditionsDetails = null;
    private String m_action = null;

    //Tworzy nową aktuywność oraz pobiera z intentu informacje o aktualniej akcji (sms czy połączenie)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar bar = getActionBar();
        setContentView(R.layout.activity_condition_add);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
        getActionBar().setTitle(getString(R.string.condition_add_title));
        m_conditionsDetails = new CondtionDetails(this);
        bar.setHomeButtonEnabled(false);
        Bundle extras = getIntent().getExtras();
        m_action = extras.getString("action");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.condition_add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    //Ustawia listener dla spinnera odpowiedzialny za wyswietlanie odpowiednich pol dla wybranego typu warunku
    @Override
    protected void onResume() {
        super.onResume();
        Spinner spinner = (Spinner) findViewById(R.id.conditionTypeSpinner);
        //Wyklucza typ polaczenia gdy akcja to polaczenie
        if(m_action.equals("sms")) {
            spinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.sms_rules_conditions_spinner)));
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    //Wyswietla pola specyficzne dla warunku
                    showDetailOptionsSms(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
        else {
            spinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.call_rules_conditions_spinner)));
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    showDetailOptionsCall(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }


    /*
    @param selectedIndex - index elementu wybranego w spinerze warunków
    0 - Typ połączenia
    1 - Numer
    2 - Dzień
    3 - Data
    4 - Nazwa kontaktu
    5 - Godzina
     */
    private void showDetailOptionsCall(int selectedIndex) {
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.condtionsAddActivity);
        selectedOption = selectedIndex;
        m_conditionsDetails.ClearWindow(layout);
        switch (selectedIndex) {
            case 0:
                //Wyświetl condtion odnośnie typu połączenia
                m_conditionsDetails.addCallTypeDetails(layout);
                break;
            case 1:
                m_conditionsDetails.addNumberDetails(layout);
                break;
            case 2:
                m_conditionsDetails.addDayDetails(layout);
                break;
            case 3:
                m_conditionsDetails.addDateDetails(layout);
                break;
            case 4:
                m_conditionsDetails.addContactNameDetails(layout);
                break;
            case 5:
                m_conditionsDetails.addHourDetails(layout);
                break;
            case 6:
                m_conditionsDetails.addPatternDetails(layout);
                break;
        }
        AddSaveButton(layout);
    }

    /*
    *     @param selectedIndex - index elementu wybranego w spinerze warunków
    0 - Numer
    1 - Dzień
    2 - Data
    3 - Nazwa kontaktu
    4 - Godzina
     */
    private void showDetailOptionsSms(int selectedIndex) {
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.condtionsAddActivity);
        selectedOption = selectedIndex;
        m_conditionsDetails.ClearWindow(layout);
        switch (selectedIndex) {
            case 0:
                m_conditionsDetails.addNumberDetails(layout);
                break;
            case 1:
                m_conditionsDetails.addDayDetails(layout);
                break;
            case 2:
                m_conditionsDetails.addDateDetails(layout);
                break;
            case 3:
                m_conditionsDetails.addContactNameDetails(layout);
                break;
            case 4:
                m_conditionsDetails.addHourDetails(layout);
                break;
            case 5:
                m_conditionsDetails.addKeywordDetails(layout);
                break;
            case 6:
                m_conditionsDetails.addPatternDetails(layout);
        }
        AddSaveButton(layout);
    }

    //Dodaje przycisk zapisz, zawsze na samym dole
    private void AddSaveButton(RelativeLayout layout) {
        m_saveButton = new FButton(this);
        m_saveButton.setLayoutParams(m_conditionsDetails.ButtonParams(m_conditionsDetails.GetLastItemId()));
        m_saveButton.setText(getString(R.string.condition_save));
        m_saveButton.setButtonColor(getResources().getColor(R.color.fabbuttoncolor));
        m_saveButton.setShadowColor(getResources().getColor(R.color.fabbuttoncolorpressed));
        m_saveButton.setShadowEnabled(true);
        m_saveButton.setShadowHeight(5);
        m_saveButton.setCornerRadius(5);
        m_saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnResultOnButtonClick(v);
            }
        });
        m_saveButton.setTextColor(getResources().getColor(android.R.color.white));
        m_saveButton.setFButtonPadding(5, 0, 5, 0);
        m_saveButton.setTextSize(12);
        m_saveButton.setId(99);
        layout.addView(m_saveButton);
    }


    /*
    selectedIndex - index elementu wybranego w spinerze warunków
    0 - Typ połączenia
    1 - Numer
    2 - Dzień
    3 - Data
    4 - Nazwa kontaktu
    5 - Godzina
     */
    public void returnResultOnButtonClick(View view) {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        if(m_action.equals("sms"))
            bundle.putSerializable("CONDITION", SetActivityResultSms());
        else
            bundle.putSerializable("CONDITION", SetActivityResult());
        bundle.putInt("ConditionType", selectedOption);
        intent.putExtras(bundle);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    //Wybiera akcje odpowiedzialna za zwrocenie wyniku do aktywności rodzica dla akcji call
    private Serializable SetActivityResult() {
        switch (selectedOption) {
            case 0:
                return createCallTypeCondition();
            case 1:
                return createNumberCondition();
            case 2:
                return createDayCondition();
            case 3:
                return createDateCondition();
            case 4:
                return createContactNameCondition();
            case 5:
                return createHourCondition();
            case 6:
                return createPatternCondition();
        }
        return null;
     }

    //Wybiera akcje odpowiedzialna za zwrocenie wyniku do aktywności rodzica dla akcji sms
    private Serializable SetActivityResultSms() {
        switch (selectedOption) {
            case 0:
                return createNumberCondition();
            case 1:
                return createDayCondition();
            case 2:
                return createDateCondition();
            case 3:
                return createContactNameCondition();
            case 4:
                return createHourCondition();
            case 5:
                return createKeywordCondition();
            case 6:
                return createPatternCondition();
        }
        return null;
    }

    //Ustawia activityResult dla callTypeCondition
    private CallTypeCondition createCallTypeCondition() {
        CallTypeCondition callType = new CallTypeCondition();
        Spinner spinner = m_conditionsDetails.GetCallTypeSpinner();
        switch (spinner.getSelectedItemPosition()) {
            case 0:
                callType.setCallType(callType.Incoming);
                break;
            case 1:
                callType.setCallType(callType.Outgoing);
                break;
        }
        return callType;
    }

    private NumberCondition createNumberCondition() {
        NumberCondition numberCondition = new NumberCondition();
        EditText numberText = m_conditionsDetails.GetPhoneNumberEditText();
        numberCondition.setNumber(numberText.getText().toString());
        return numberCondition;
    }
    /*
    0 - Monday
    1 - Tuesday
    2 - Wednesday
    4 - Thursday
    5 - Friday
    6 - Saturday
    7 - Sunday
     */
    private DayCondition createDayCondition() {
        DayCondition dayCondition = new DayCondition();
        Spinner daySpinner = m_conditionsDetails.GetDaysSpinner();
        dayCondition.setDay(daySpinner.getSelectedItemPosition());
        CheckBox isEveryWeekCheckBox = m_conditionsDetails.GetEveryWeekCheckBox();
        dayCondition.setEveryWeek(isEveryWeekCheckBox.isChecked());
        return dayCondition;
    }

    private DateCondition createDateCondition() {
        DatePicker startDate = m_conditionsDetails.GetStartDatePicker();
        DatePicker endDate = m_conditionsDetails.GetEndDatePicker();
        Date start = new Date(startDate.getCalendarView().getDate());
        Date end = new Date(endDate.getCalendarView().getDate());
        DateCondition dateCondition = new DateCondition(start, end);
        return dateCondition;
    }

    private ContactNameCondition createContactNameCondition() {
        ContactNameCondition contactNameCondition = new ContactNameCondition();
        EditText contactName = m_conditionsDetails.GetContactNameEditText();
        contactNameCondition.setContactName(contactName.getText().toString());
        return contactNameCondition;
    }

    private HourCondition createHourCondition() {
        TimePicker startPicker = m_conditionsDetails.GetStartHourPicker();
        TimePicker endPicker = m_conditionsDetails.GetEndHourPicker();
        int startHour = startPicker.getCurrentHour();
        int startMinutes = startPicker.getCurrentMinute();
        int endHour = endPicker.getCurrentHour();
        int endMinutes = endPicker.getCurrentMinute();
        HourCondition hourCondition = new HourCondition(startHour, startMinutes, endHour, endMinutes);
        return hourCondition;
    }

    private KeywordCondition createKeywordCondition() {
        EditText keywordEditText = m_conditionsDetails.GetKeywordEditText();
        String keyword = keywordEditText.getText().toString();
        KeywordCondition keywordCondition = new KeywordCondition();
        keywordCondition.setKeyword(keyword);
        return keywordCondition;
    }

    private PatternCondition createPatternCondition() {
        EditText patternEditText = m_conditionsDetails.getPatternEditText();
        CheckBox matchContactNameCheckBox = m_conditionsDetails.getMatchContactNameCheckBox();
        CheckBox matchNumberCheckBox = m_conditionsDetails.getMatchNumberCheckBox();
        CheckBox matchMessageBodyCheckBox = m_conditionsDetails.getMatchMessageBodyCheckBox();
        PatternCondition patternCondition = new PatternCondition();
        patternCondition.setPattern(patternEditText.getText().toString());
        patternCondition.setMatchContactName(matchContactNameCheckBox.isChecked());
        patternCondition.setMatchNumber(matchNumberCheckBox.isChecked());
        patternCondition.setMatchMessageBody(matchMessageBodyCheckBox.isChecked());
        return patternCondition;
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_condition_add, container, false);
            ActionBar bar = getActivity().getActionBar();
            bar.setHomeButtonEnabled(false);
            bar.setDisplayHomeAsUpEnabled(false);
            return rootView;
        }
    }
}
