package com.mkotlarz.mobile.productivity.phonelocker.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.provider.BaseColumns;

import com.mkotlarz.mobile.productivity.phonelocker.data.SmsTable.SmsColumns;
import com.mkotlarz.mobile.productivity.phonelocker.models.SMSModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SmsDao {

    private static final String INSERT =
            "insert into " + SmsTable.TABLE_NAME + "("
                    + SmsColumns.PHONE_NUMBER + ", "
                    + SmsColumns.BODY + ", "
                    + SmsColumns.typ + ", "
                    + SmsColumns.DATE + ", "
                    + SmsColumns.ALIAS + ", "
                    + SmsColumns.MATCHED_ON + ") values (?, ? , ?, ?, ?, ?)";

    private SQLiteDatabase db;
    private SQLiteStatement insertStatement;
    private SQLiteOpenHelper openHelper;

   public SmsDao(SQLiteDatabase db) {
      this.db = db;
      insertStatement = db.compileStatement(SmsDao.INSERT);
   }
    public long save(SMSModel sModel) {
        insertStatement.clearBindings();
        insertStatement.bindString(1, sModel.getPhoneNumber());
        insertStatement.bindString(2, sModel.getSmsBody());
        insertStatement.bindLong(3, sModel.getSmsType());
        insertStatement.bindString(4, sModel.getSmsDate());
        insertStatement.bindString(5, sModel.getAlias());
        insertStatement.bindString(6, sModel.getMatchedOn());
        return insertStatement.executeInsert();
    }

   public static void deleteSMS(SMSModel sms) {
	   //delete
   }

   public int update(SMSModel sModel) {
	   final ContentValues values = new ContentValues();
	      values.put("phone_number" , sModel.getPhoneNumber());
	      //values.put("body", sModel.getSmsBody());
	      values.put("typ" , sModel.getSmsType());
	      values.put("date" , sModel.getSmsDate().toString());
          values.put("alias", sModel.getAlias());
          values.put("matched_on", sModel.getMatchedOn());
	      db.update(SmsTable.TABLE_NAME, values, BaseColumns._ID + " = ?", new String[] { String.valueOf(sModel.getId()) });
	      return 1;
   }

   public String[][] getAllSMS() {
       Cursor callsCursor = db.rawQuery("select * from " + SmsTable.TABLE_NAME + " ORDER BY " + SmsColumns._ID +" DESC", null);
       String[][] result = new String[callsCursor.getCount()][4];
       callsCursor.moveToFirst();
       for(int i = 0; i < callsCursor.getCount(); i++){
           String phoneNumber = callsCursor.getString(callsCursor.getColumnIndex(SmsColumns.PHONE_NUMBER));
           String date = callsCursor.getString(callsCursor.getColumnIndex(SmsColumns.DATE));
           String body = callsCursor.getString(callsCursor.getColumnIndex(SmsColumns.BODY));
           String alias = callsCursor.getString(callsCursor.getColumnIndex(SmsColumns.ALIAS));
           //You can here manipulate a single string as you please
           result[i][0] = phoneNumber;
           result[i][1] = date;
           result[i][2] = body;
           result[i][3] = alias;
           callsCursor.moveToNext();
       }
       callsCursor.close();
       return result;
   }

   public List<SMSModel> getSMSbyNumber(long number) {
	   List<SMSModel> list = new ArrayList<SMSModel>();
	   return list;
   }

   public SMSModel getSMSbyId(long sms_id) {
	   SMSModel sms = null;
	      Cursor c =
	               db.query(SmsTable.TABLE_NAME, new String[] { BaseColumns._ID, SmsColumns.PHONE_NUMBER,
	            		   SmsColumns.BODY , SmsColumns.typ, SmsColumns.DATE},
	                        BaseColumns._ID + " = ?", new String[] { String.valueOf(sms_id) }, null, null, null, "1");
	      if (c.moveToFirst()) {
	         sms = this.buildSmsFromCursor(c);
	      }
	      if (!c.isClosed()) {
	         c.close();
	      }
	      return sms;
   }
   
   private SMSModel buildSmsFromCursor(Cursor c) {
	   SMSModel sms = null;
       Date date = new Date();
	   if (c != null) {
	         //sms = new SMSModel(c.getString(1), c.getString(2), date, c.getInt(3));
	      }
	      return sms;
	   
   }
}
