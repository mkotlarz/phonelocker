package com.mkotlarz.mobile.productivity.phonelocker.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.mkotlarz.mobile.productivity.phonelocker.CreateRuleActivity;
import com.mkotlarz.mobile.productivity.phonelocker.R;
import com.mkotlarz.mobile.productivity.phonelocker.data.DataManagerImpl;
import com.mkotlarz.mobile.productivity.phonelocker.models.CallRulesModel;
import com.mkotlarz.mobile.productivity.phonelocker.models.SmsRulesModel;

/**
 * Created by Maciej on 2014-06-30.
 */
public class RulesAdapter extends BaseAdapter {

    /**
     * The inflator used to inflate the XML layout
     */
    private LayoutInflater inflator;

    /**
     * A list containing some sample data to show.
     */
    private String[][] dataList;
    private Context m_context = null;
    private DataManagerImpl m_dataManager = null;
    private String m_action = null;

    public RulesAdapter(LayoutInflater inflator, String[][] rules, Context context, String action) {
        super();
        this.inflator = inflator;

        dataList = rules;
        m_context = context;
        m_action = action;
        m_dataManager = new DataManagerImpl(m_context);

    }

    @Override
    public int getCount() {
        return dataList.length;
    }

    @Override
    public Object getItem(int position) {
        return dataList[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflator.inflate(R.layout.call_rules_list_item, parent, false);
            convertView.setTag(R.id.ruleActiveCheckBox, convertView.findViewById(R.id.ruleActiveCheckBox));
            convertView.setTag(R.id.ruleNameTextView, convertView.findViewById(R.id.ruleNameTextView));
        }
        String[] data = (String[]) getItem(position);

        // Set the example text and the state of the checkbox
        CheckBox cb = (CheckBox) convertView.getTag(R.id.ruleActiveCheckBox);
        if (data[0].equals("1"))
            cb.setChecked(true);

        TextView tv = (TextView) convertView.getTag(R.id.ruleNameTextView);
        tv.setText(data[1]);
        setCheckboxOnClickListener(cb, position);
        setCheckBoxOnLongClickListener(convertView, position);
        return convertView;
    }

    public void refreshAdapterValues(String[][] rules) {
        dataList = rules;
        notifyDataSetChanged();
    }

    public void setCheckboxOnClickListener(CheckBox checkBox, final int position) {
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                CallRulesModel cModel;
                SmsRulesModel sModel;
                int checked = 0;
                if(isChecked)
                    checked = 1;
                else
                    checked = 0;

                if(m_action.equals("call")) {
                    cModel = m_dataManager.findCallRuleByPosition(position);
                    cModel.setRuleActive(checked);
                    m_dataManager.editCallRule(cModel);
                    return;
                }
                if(m_action.equals("sms")) {
                    sModel = m_dataManager.findSmsRuleByPosition(position);
                    sModel.setRuleActive(checked);
                    m_dataManager.editSmsRule(sModel);
                    return;
                }

            }
        });
    }

    public void setCheckBoxOnLongClickListener(View view, final int position) {
        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                CharSequence colors[] = new CharSequence[] {m_context.getString(R.string.action_edit), m_context.getString(R.string.action_delete)};
                AlertDialog.Builder builder = new AlertDialog.Builder(m_context);
                builder.setTitle(m_context.getString(R.string.select_action));
                builder.setItems(colors, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        CallRulesModel cModel;
                        SmsRulesModel sModel;
                        if (which == 0) {
                            Intent intent = new Intent(m_context, CreateRuleActivity.class);
                            Bundle extras = new Bundle();
                            extras.putString("action", m_action);
                            extras.putBoolean("isEdit", true);
                            extras.putInt("editItemPosition", position);
                            intent.putExtras(extras);
                            m_context.startActivity(intent);
                        }
                        if (which == 1) {
                            if (m_action.equals("call")) {
                                cModel = m_dataManager.findCallRuleByPosition(position);
                                m_dataManager.deleteCallRule(cModel);
                                dataList = m_dataManager.getAllCallRules();

                            }
                            if (m_action.equals("sms")) {
                                sModel = m_dataManager.findSmsRuleByPosition(position);
                                m_dataManager.deleteSmsRule(sModel);
                                dataList = m_dataManager.getAllSmsRules();
                            }
                            notifyDataSetChanged();
                        }
                    }
                });
                builder.show();
                return true;
            }
        });
    }
}