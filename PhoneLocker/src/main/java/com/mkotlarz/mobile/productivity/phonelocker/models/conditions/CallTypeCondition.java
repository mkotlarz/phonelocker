package com.mkotlarz.mobile.productivity.phonelocker.models.conditions;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mkotlarz.mobile.productivity.phonelocker.R;
import com.mkotlarz.mobile.productivity.phonelocker.interfaces.ICondition;

import org.apache.log4j.Logger;

import java.io.Serializable;

/**
 * Created by Maciej on 2014-06-29.
 */
public class CallTypeCondition implements Serializable, ICondition {

    @Expose
    @SerializedName("callType")
    private String m_callType = null;

    private String m_matched_on = null;

    public final static String JSON_NAME = "CallTypeCondition";
    public final static String Incoming = "Incoming";
    public final static String Outgoing = "Outgoing";

    public void setCallType(String callType) {
        m_callType = callType;
    }

//    public String getCallType() {
//        return this.m_callType;
//    }

    public int getCallTypeDisplay() {
        if(m_callType.equals("Incoming"))
            return R.string.incoming_call;
        else
            return R.string.outgoing_call;
    }

    public String getMatchedOn() { return m_matched_on; }

    public void setMatchedOn(String matched_on) { m_matched_on = matched_on; }

    public String getJSONName() {
        return "CallTypeCondition";
    }

    public Class<?> getConditionClass() {
        return CallTypeCondition.class;
    }

    public int getClassCode() {
        return 0;
    }

    public boolean Match(String callType) {
        final Logger Log = Logger.getLogger(CallTypeCondition.class);
        Log.info("Matching if: " + callType + " equals " + m_callType);
        if(callType.toLowerCase().equals(m_callType.toLowerCase()))
        {
            Log.info("Result: true");
            m_matched_on = m_callType;
            return true;
        }
        Log.info("Result: false");
        return false;
    }

    public String getClassTranslatedName(Context ctx) {
        return ctx.getString(R.string.call_type_condition);
    }
}
