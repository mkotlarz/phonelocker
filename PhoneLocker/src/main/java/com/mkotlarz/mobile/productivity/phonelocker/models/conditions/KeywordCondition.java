package com.mkotlarz.mobile.productivity.phonelocker.models.conditions;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mkotlarz.mobile.productivity.phonelocker.R;
import com.mkotlarz.mobile.productivity.phonelocker.interfaces.ICondition;

import org.apache.log4j.Logger;

import java.io.Serializable;

/**
 * Created by Maciej on 2014-06-29.
 */
public class KeywordCondition implements Serializable, ICondition {

    @Expose
    @SerializedName("keyword")
    private String m_keyword = null;

    private String m_matched_on = null;

    public final static String JSON_NAME = "KeywordCondition";

    public String getMatchedOn() { return m_matched_on; }

    public void setMatchedOn(String matched_on) { m_matched_on = matched_on; }

    public void setKeyword(String keyword) {
        m_keyword = keyword;
    }

    public String getKeyword() { return m_keyword; }

    public String getJSONName() {
        return "KeywordCondition";
    }

    public Class<?> getConditionClass() {
        return CallTypeCondition.class;
    }

    public int getClassCode() {
        return 6;
    }

    public boolean Match(String messageBody) {
        if(messageBody != null) {
            final Logger Log = Logger.getLogger(KeywordCondition.class);
            Log.info("Matching if: " + messageBody + " contains: " + m_keyword);
            if(messageBody.toLowerCase().contains(m_keyword.toLowerCase())) {
                Log.info("Result: true");
                m_matched_on = m_keyword;
                return true;
            }
            Log.info("Result: false");
            return false;
        }
        return false;
    }

    public String getClassTranslatedName(Context ctx) {
        return ctx.getString(R.string.keyword_condition);
    }
}
