package com.mkotlarz.mobile.productivity.phonelocker.tests;

import android.content.Context;
import android.test.InstrumentationTestCase;

import com.mkotlarz.mobile.productivity.phonelocker.data.DataManager;
import com.mkotlarz.mobile.productivity.phonelocker.data.DataManagerImpl;
import com.mkotlarz.mobile.productivity.phonelocker.main.RulesExecutor;
import com.mkotlarz.mobile.productivity.phonelocker.models.CallRulesModel;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Maciej Kotlarz on 2014-11-04.
 */
public class RulesExecutorTestCase extends InstrumentationTestCase {
    public void testEmptyRules() throws Exception {
        Context ctx = this.getInstrumentation().getTargetContext();
        DataManagerImpl dm = new DataManagerImpl(ctx);
        int test_action = 1;
        String test_phone_number = "726734194";
        String test_message_content = "Test content";
        RulesExecutor rulesExecutor = new RulesExecutor(dm, ctx);
        boolean result = rulesExecutor.Process(test_action, test_phone_number, test_message_content);
        deleteDatabase(dm);
        assertFalse(result);
    }

    public void testCallTypeConditionRule() throws Exception {
        Context ctx = getInstrumentation().getTargetContext();
        DataManagerImpl dm = new DataManagerImpl(ctx);
        Date test_date = new Date();
        String condition_string = "{\"Conditions\": {\"CallTypeCondition\":{\"callType\":\"Incoming\"}} }";
        dm.saveCallRule("test", test_date, condition_string, true);
        RulesExecutor rulesExecutor = new RulesExecutor(dm, getInstrumentation().getTargetContext());
        String test_phone_number = "726734194";
        String test_message_content = null;
        boolean result = rulesExecutor.Process(0, test_phone_number, test_message_content);
        deleteDatabase(dm);
        assertTrue(result);
    }

    public void testCallTypeOutgoingConditionRule() throws Exception {
        Context ctx = getInstrumentation().getTargetContext();
        DataManagerImpl dm = new DataManagerImpl(ctx);
        Date test_date = new Date();
        String condition_string = "{\"Conditions\": {\"CallTypeCondition\":{\"callType\":\"Outgoing\"}} }";
        dm.saveCallRule("test", test_date, condition_string, true);
        RulesExecutor rulesExecutor = new RulesExecutor(dm, getInstrumentation().getTargetContext());
        String test_phone_number = "726734194";
        String test_message_content = null;
        boolean result = rulesExecutor.Process(1, test_phone_number, test_message_content);
        deleteDatabase(dm);
        assertTrue(result);
    }

    public void testCallNumberConditionRule() throws Exception {
        Context ctx = getInstrumentation().getTargetContext();
        DataManagerImpl dm = new DataManagerImpl(ctx);
        Date test_date = new Date();
        String condition_string = "{\"Conditions\": {\"NumberCondition\":{\"callNumber\":\"726734194\"}} }";
        dm.saveCallRule("test", test_date, condition_string, true);
        RulesExecutor rulesExecutor = new RulesExecutor(dm, ctx);
        String test_phone_number = "726734194";
        String test_message_content = null;
        boolean result = rulesExecutor.Process(0, test_phone_number, test_message_content);
        deleteDatabase(dm);
        assertTrue(result);
    }

    public void testContactNameCondition() throws Exception {
        Context ctx = getInstrumentation().getTargetContext();
        DataManagerImpl dm = new DataManagerImpl(ctx);
        Date test_date = new Date();
        String condition_string = "{\"Conditions\": {\"ContactNameCondition\":{\"contactName\":\"Ja\"}} }";
        dm.saveCallRule("test", test_date, condition_string, true);
        RulesExecutor rulesExecutor = new RulesExecutor(dm, ctx);
        String test_phone_number = "726734194";
        String test_message_content = null;
        boolean result = rulesExecutor.Process(0, test_phone_number, test_message_content);
        deleteDatabase(dm);
        assertTrue(result);
    }

    public void testDayCondition() throws Exception {
        Context ctx = getInstrumentation().getTargetContext();
        DataManagerImpl dm = new DataManagerImpl(ctx);
        Date test_date = new Date();
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        String condition_string = "{\"Conditions\": {\"DayCondition\":{\"Day\":" + day + ",\"everyWeek\":false}} }";
        dm.saveCallRule("test", test_date, condition_string, true);
        RulesExecutor rulesExecutor = new RulesExecutor(dm, ctx);
        String test_phone_number = "726734194";
        String test_message_content = null;
        boolean result = rulesExecutor.Process(0, test_phone_number, test_message_content);
        deleteDatabase(dm);
        assertTrue(result);
    }

    public void testDateCondition() throws Exception {
        Context ctx = getInstrumentation().getTargetContext();
        DataManagerImpl dm = new DataManagerImpl(ctx);
        Date test_date = new Date();
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        String condition_string = "{\"Conditions\": {\"DateCondition\":{\"endDate\":\"06-11-2020\",\"startDate\":\"04-11-2014\"}} }";
        dm.saveCallRule("test", test_date, condition_string, true);
        RulesExecutor rulesExecutor = new RulesExecutor(dm, ctx);
        String test_phone_number = "726734194";
        String test_message_content = null;
        boolean result = rulesExecutor.Process(0, test_phone_number, test_message_content);
        deleteDatabase(dm);
        assertTrue(result);
    }

    public void testHourCondition() throws Exception {
        Context ctx = getInstrumentation().getTargetContext();
        DataManagerImpl dm = new DataManagerImpl(ctx);
        Date test_date = new Date();
        Calendar calendar = Calendar.getInstance();
        String condition_string = "{\"Conditions\": {\"HourCondition\":{\"endHour\":23,\"endMinutes\":59,\"startHour\":0,\"startMinutes\":0}} }";
        dm.saveCallRule("test", test_date, condition_string, true);
        RulesExecutor rulesExecutor = new RulesExecutor(dm, ctx);
        String test_phone_number = "726734194";
        String test_message_content = null;
        boolean result = rulesExecutor.Process(0, test_phone_number, test_message_content);
        deleteDatabase(dm);
        assertTrue(result);
    }

    public void testKeywordCondition() throws Exception {
        Context ctx = getInstrumentation().getTargetContext();
        DataManagerImpl dm = new DataManagerImpl(ctx);
        Date test_date = new Date();
        Calendar calendar = Calendar.getInstance();
        String condition_string = "{\"Conditions\": {\"KeywordCondition\":{\"keyword\":\"test\"}} }";
        dm.saveSmsRule("test", test_date, condition_string, true);
        RulesExecutor rulesExecutor = new RulesExecutor(dm, ctx);
        String test_phone_number = "726734194";
        String test_message_content = "Test content";
        boolean result = rulesExecutor.Process(2, test_phone_number, test_message_content);
        deleteDatabase(dm);
        assertTrue(result);
    }

    public void testPatternKeyword() throws Exception {
        Context ctx = getInstrumentation().getTargetContext();
        DataManagerImpl dm = new DataManagerImpl(ctx);
        Date test_date = new Date();
        String condition_string = "{\"Conditions\": {\"PatternCondition\":{\"pattern\":\"\\\\w*\\\\s*test\", \"match_contact_name\": true, \"match_number\": false, \"match_message_body\": true}} }";
        dm.saveSmsRule("test", test_date, condition_string, true);
        RulesExecutor rulesExecutor = new RulesExecutor(dm, ctx);
        String test_phone_number = "726734194";
        String test_message_content = "example test";
        boolean result = rulesExecutor.Process(2, test_phone_number, test_message_content);
        deleteDatabase(dm);
        assertTrue(result);
    }

    private void deleteDatabase(DataManagerImpl dm) {
        getInstrumentation().getTargetContext().deleteDatabase(dm.getDb().getPath());
    }
}