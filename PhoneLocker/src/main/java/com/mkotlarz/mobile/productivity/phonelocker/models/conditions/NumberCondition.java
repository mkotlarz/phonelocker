package com.mkotlarz.mobile.productivity.phonelocker.models.conditions;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mkotlarz.mobile.productivity.phonelocker.R;
import com.mkotlarz.mobile.productivity.phonelocker.interfaces.ICondition;

import org.apache.log4j.Logger;

import java.io.Serializable;

/**
 * Created by Maciej on 2014-06-29.
 */
public class NumberCondition implements Serializable, ICondition{

    @Expose
    @SerializedName("callNumber")
    private String m_callNumber = null;

    private String m_matched_on = null;

    public final static String JSON_NAME = "NumberCondition";

    public String getMatchedOn() { return m_matched_on; }

    public void setMatchedOn(String matched_on) { m_matched_on = matched_on; }

    public void setNumber(String number) {
        this.m_callNumber = number;
    }

    public String getNumber() {
        return this.m_callNumber;
    }

    public String getJSONName() {
        return "NumberCondition";
    }

    public Class<?> getConditionClass() {
        return NumberCondition.class;
    }

    public int getClassCode() {
        return 1;
    }

    public boolean Match(String callNumber) {
        final Logger Log = Logger.getLogger(NumberCondition.class);
        Log.info("Matching if number: " + callNumber + " contains " + m_callNumber);
        if(callNumber.toLowerCase().contains(m_callNumber.toLowerCase())) {
            Log.info("Result: true");
            m_matched_on = m_callNumber;
            return true;
        }
        Log.info("Result: false");
        return false;
    }

    public String getClassTranslatedName(Context ctx) {
        return ctx.getString(R.string.number_condition);
    }
}
