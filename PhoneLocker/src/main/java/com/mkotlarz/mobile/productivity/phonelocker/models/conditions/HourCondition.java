package com.mkotlarz.mobile.productivity.phonelocker.models.conditions;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mkotlarz.mobile.productivity.phonelocker.R;
import com.mkotlarz.mobile.productivity.phonelocker.interfaces.ICondition;

import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by Maciej on 2014-07-02.
 */

public class HourCondition implements Serializable, ICondition {

    public final static String JSON_NAME = "HourCondition";

    @Expose
    @SerializedName("startHour")
    private int m_startHour;

    @Expose
    @SerializedName("startMinutes")
    private int m_startMinutes;

    @Expose
    @SerializedName("endHour")
    private int m_endHour;

    @Expose
    @SerializedName("endMinutes")
    private int m_endMinutes;

    private String m_matched_on = null;

    public HourCondition(int startHour, int startMinutes,  int endHour, int endMinutes) {
        m_startHour = startHour;
        m_startMinutes = startMinutes;
        m_endHour = endHour;
        m_endMinutes = endMinutes;
    }

    public HourCondition() {

    }

    public String getMatchedOn() { return m_matched_on; }

    public void setMatchedOn(String matched_on) { m_matched_on = matched_on; }

    public int getStartHour() {
        return m_startHour;
    }

    public void setStartHour(int hour) {
        m_startHour = hour;
    }

    public int getStartMinutes() { return m_startMinutes; }

    public void setStartMinutes(int minutes) { m_startMinutes = minutes; }

    public int getEndHour() {
        return m_endHour;
    }

    public void setEndHour(int hour) {
        m_endHour = hour;
    }

    public int getEndMinutes() { return m_endMinutes; }

    public void setEndMinutes(int minutes) { m_endMinutes = minutes; }

    public String getJSONName() {
        return "HourCondition";
    }

    public Class<?> getConditionClass() {
        return HourCondition.class;
    }

    public int getClassCode() {
        return 5;
    }

    public boolean Match() {

        final Logger Log = Logger.getLogger(HourCondition.class);
        try {
            SimpleDateFormat dateFormater = new SimpleDateFormat("HH:mm");
            String timeStart = m_startHour + ":" + m_startMinutes;
            String timeEnd = m_endHour + ":" + m_endMinutes;
            Date startDate = dateFormater.parse(timeStart);
            Date endDate = dateFormater.parse(timeEnd);
            String timeNowString = Calendar.getInstance(TimeZone.getDefault()).get(Calendar.HOUR_OF_DAY) + ":" + Calendar.getInstance(TimeZone.getDefault()).get(Calendar.MINUTE);
            Date timeNow = dateFormater.parse(timeNowString);
            Log.info("Matching if actual hour is between: " + timeStart + " and " + timeEnd);
            if(timeNow.after(startDate) && timeNow.before(endDate)) {
                Log.info("Result: true");
                m_matched_on = timeNowString;
                return true;
            }
            Log.info("Result: false");
            return false;
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getStackTrace());
            return false;
        }
    }

    public String getClassTranslatedName(Context ctx) {
        return ctx.getString(R.string.hour_condition);
    }
}