package com.mkotlarz.mobile.productivity.phonelocker.sms.models;

/**
 * Created by Maciej on 2014-07-13.
 */
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.telephony.TelephonyManager;

import com.mkotlarz.mobile.productivity.phonelocker.data.Constants;

import java.util.Date;

/**
 * Created by sdavies on 09/01/2014.
 */
public class Message {

    private int id;
    private String Content;
    private String Sender;
    private String Recipient;
    private Date Time;
    private long ThreadId;
    private String ContactName;
    private int MessageType;
    private int Read;

    public Message(int id, String content, String sender, String contactName, String recipient, Date time, int messageType, int read, long threadId) {
        this.id = id;
        Content = content;
        Sender = sender;
        Recipient = recipient;
        Time = time;
        ThreadId = threadId;
        ContactName = contactName;
        MessageType = messageType;
        Read = read;
    }

    public Message(String content, String sender, String recipient, Date time, long threadId) {
        Content = content;
        Sender = sender;
        Recipient = recipient;
        Time = time;
        ThreadId = threadId;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getSender() {
        return Sender;
    }

    public void setSender(String sender) {
        Sender = sender;
    }


    public String getContactName() {
        return ContactName;
    }

    public void setContactName(String contactName) {
        ContactName = contactName;
    }

    public String getRecipient() {
        return Recipient;
    }

    public void setRecipient(String recipient) {
        Recipient = recipient;
    }

    public Date getTime() {
        return Time;
    }

    public void setTime(Date time) {
        Time = time;
    }

     public int getMessageType() {
        return MessageType;
    }

    public void setMessageType(int messageType) {
        MessageType = messageType;
    }

    public long getThreadId() {
        return ThreadId;
    }

    public void setThreadId(long threadId) {
        ThreadId = threadId;
    }

    public boolean isReaded() {
        return Read == 1;
    }

    public void setRead(int read) {
        Read = read;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        if (MessageType == Constants.SMS_INCOMING_TYPE_VALUE)
            return getSender() + ": " + getContent() + "  -  " + getTime().toString();
        return "Ja: " + getContent() + " - " + getTime().toString();
    }

    public String searchContactName(String address, Context context) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(address));
        ContentResolver contentResolver = context.getContentResolver();
        String contactName = "";
        try {
            Cursor contactLookup = contentResolver.query(uri, new String[] {BaseColumns._ID,
                    ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);
            if(contactLookup.moveToFirst()) {
                contactName = contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
                contactLookup.close();
            } else {
                TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(manager.getNetworkCountryIso() + address));
                contactLookup = contentResolver.query(uri, new String[] {BaseColumns._ID,
                        ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);
                if(contactLookup.moveToFirst()) {
                    contactName = contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
                    contactLookup.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return contactName;
    }

    public void setMessageAsRead(Context context) {
        ContentValues values = new ContentValues();
        values.put(Constants.SMS_READ, 1);
        context.getContentResolver().update(Uri.parse(Constants.SMS_URI), values, Telephony.Sms._ID + " = " + this.id, null);
    }
}