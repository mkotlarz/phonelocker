package com.mkotlarz.mobile.productivity.phonelocker.data;

import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public final class CallRulesTable {

    public static final String TABLE_NAME = "call_rules";

    public static class CallRulesColumns implements BaseColumns {
        public static final String RULENAME = "rule_name";
        public static final String DATE = "date";
        public static final String RULETYPE = "rule_type";
        public static final String RULECONDITIONS = "rule_conditions";
        public static final String RULEACTIVE = "active";
    }

    public static void onCreate(SQLiteDatabase db) {
        db.execSQL(CreateTableSql());
    }

    public static String CreateTableSql() {
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE " + CallRulesTable.TABLE_NAME + " (");
        sb.append(BaseColumns._ID + " INTEGER PRIMARY KEY, ");
        sb.append(CallRulesColumns.RULENAME + " VARCHAR, ");
        sb.append(CallRulesColumns.DATE + " TEXT, "); // movie names aren't unique, but for simplification we constrain
        sb.append(CallRulesColumns.RULECONDITIONS + " TEXT, ");
        sb.append(CallRulesColumns.RULEACTIVE + " INT");
        sb.append(");");
        return sb.toString();
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(newVersion == 1) {
            db.execSQL("DROP TABLE IF EXISTS " + CallsTable.TABLE_NAME);
            CallsTable.onCreate(db);
        }
    }

}
