package com.mkotlarz.mobile.productivity.phonelocker.sms;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Telephony;

import android.annotation.TargetApi;

import android.telephony.SmsManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mkotlarz.mobile.productivity.phonelocker.R;
import com.mkotlarz.mobile.productivity.phonelocker.data.Constants;
import com.mkotlarz.mobile.productivity.phonelocker.models.Contact;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class ComposeSmsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compose_sms);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
        // Enable the 'back' button
        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.compose_sm, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        static final int PICK_CONTACT=1;

        private List<Contact> m_contacts = null;
        private List<Contact> m_allContacts = null;
        private EditText m_editText = null;
        private final Logger Log = Logger.getLogger(ComposeSmsActivity.class);

        public PlaceholderFragment() {
            m_contacts = new ArrayList<Contact>();
            m_allContacts =  new ArrayList<Contact>();
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment_compose_sms, container, false);
            ImageButton button = (ImageButton) rootView.findViewById(R.id.findContacts);
            button.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pickContacts();
                }
            });
            ImageView sendMessageButton = (ImageView) rootView.findViewById(R.id.sendReplyButton);
            sendMessageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendTextMessages();
                }
            });
            m_editText = (EditText) rootView.findViewById(R.id.contactNumber);
            return rootView;
        }

        private void sendTextMessages() {
            EditText contactsNumbersEditText = (EditText) getActivity().findViewById(R.id.contactNumber);
            String[] contactsNumbers = contactsNumbersEditText.getText().toString().split(";");
            if(m_contacts.size() != 0 || contactsNumbers.length != 0) {
                try {
                    SmsManager smsManager = SmsManager.getDefault();
                    AutoCompleteTextView replyText = (AutoCompleteTextView) getActivity().findViewById(R.id.replyMessage);
                    String body = replyText.getText().toString();
                    if(m_contacts.size() > 0) {
                        for (int i = 0; i < m_contacts.size(); i++) {
                            send(smsManager, m_contacts.get(i).getNumber(), body);
                        }
                    }
                    if(contactsNumbers.length > 1) {
                        for (int i = 0; i < contactsNumbers.length; i++) {
                            send(smsManager, contactsNumbers[i], body);
                        }
                    }
                    Toast.makeText(getActivity().getApplicationContext(), getString(R.string.message_sent_info), Toast.LENGTH_LONG).show();
                    getActivity().finish();
                }
                catch (IllegalArgumentException ex) {
                    Log.error(ex.getMessage() + "..." + ex.getStackTrace());
                    Toast.makeText(getActivity().getApplicationContext(), getString(R.string.compose_sms_illegal_argument_exception), Toast.LENGTH_LONG).show();
                }
                catch (Exception ex) {
                    Log.error(ex.getMessage() + "..." + ex.getStackTrace());
                }
            } else {
                Toast.makeText(getActivity().getApplicationContext(), getString(R.string.contacts_null_error), Toast.LENGTH_LONG).show();
            }
        }

        private void send(SmsManager smsManager, String number, String replyText) {
            smsManager.sendTextMessage(number, "ME", replyText, null, null);
            addSmsToInbox(number, replyText);
        }

        private void addSmsToInbox(String number, String body) {
            if(!CheckIfAppIsDefaultAppForSms())
                return;
            ContentValues values = new ContentValues();
            values.put(Constants.SMS_ADDRESS, number);
            values.put(Constants.SMS_DATE, System.currentTimeMillis() + "");
            values.put(Constants.SMS_BODY, body);
            values.put(Constants.SMS_TYPE, Constants.SMS_OUTGOING_TYPE_VALUE);
            getActivity().getApplicationContext().getContentResolver().insert(Uri.parse(Constants.SMS_URI), values);
        }

        @TargetApi(19)
        private boolean CheckIfAppIsDefaultAppForSms() {
            final String packageName = getActivity().getApplicationContext().getPackageName();
            return Telephony.Sms.getDefaultSmsPackage(getActivity().getApplicationContext()).equals(packageName);
        }

        @Override
        public void onPause() {
            super.onPause();
            m_editText.getText().toString();
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
                switch (requestCode) {
                    case (PICK_CONTACT):
                        if (resultCode == Activity.RESULT_OK) {
                            Uri contactData = data.getData();
                            Cursor c = getActivity().getContentResolver().query(contactData, null, null, null, null);
                            if (c.moveToFirst()) {
                                int id = c.getInt(c.getColumnIndex(ContactsContract.Contacts._ID));
                                Cursor numberCursor = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                        null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[] { id + ""}, null);
                                if(numberCursor.moveToFirst()) {
                                    String number = numberCursor.getString(numberCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                    String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                                    Contact contact = new Contact(number, name);
                                    showContactButton(contact, m_allContacts.size());
                                    m_contacts.add(contact);
                                    m_allContacts.add(contact);
                                }
                                numberCursor.close();
                            }
                            c.close();
                        }
                        break;
                }
        }

        private void showContactButton(Contact contact, final int index) {
            final LinearLayout contactLayout = (LinearLayout) getActivity().findViewById(R.id.selectedContactsLinearLayout);
            Button contactButton = new Button(getActivity().getApplicationContext());
            LinearLayout.LayoutParams button_params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            button_params.weight = 1;
            button_params.gravity = Gravity.LEFT;
            contactButton.setLayoutParams(button_params);
            contactButton.setText(contact.getName());
                contactButton.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        try {
                            final View view = v;
                            CharSequence actions[] = new CharSequence[]{getString(R.string.compose_sms_action_delete_contact)};
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle(getString(R.string.select_action));
                            builder.setItems(actions, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (which == 0) {
                                        m_contacts.remove(m_allContacts.get(index));
                                        contactLayout.removeView(view);
                                    }
                                    if (which == 1) {
                                        m_contacts.remove(m_allContacts.get(index));
                                    }
                                }
                            });
                            builder.show();
                        } catch (Exception ex) {
                            Log.error(ex.getMessage() + "..." + ex.getStackTrace());
                        }
                        return true;
                    }
                });
            contactLayout.addView(contactButton);
        }

        @Override
        public void onResume() {
            super.onResume();
        }

        public void pickContacts() {
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            startActivityForResult(intent, PICK_CONTACT);
        }
    }
}
