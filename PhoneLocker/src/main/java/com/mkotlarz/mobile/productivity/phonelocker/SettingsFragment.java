package com.mkotlarz.mobile.productivity.phonelocker;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mkotlarz.mobile.productivity.phonelocker.R;
import com.mkotlarz.mobile.productivity.phonelocker.data.CallRulesDao;
import com.mkotlarz.mobile.productivity.phonelocker.data.Constants;
import com.mkotlarz.mobile.productivity.phonelocker.data.DataManagerImpl;
import com.mkotlarz.mobile.productivity.phonelocker.data.SmsRulesDao;
import com.mkotlarz.mobile.productivity.phonelocker.utils.SendHttpRequest;

import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class SettingsFragment extends Fragment {

    private DataManagerImpl dm;

    public SettingsFragment() {
    }

    public void ExportDataOnClick(View view) {
        dm = new DataManagerImpl(getActivity().getApplicationContext());
        Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        intent.setType("text/html");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] { "biuro@mkotlarz.pl" });
        intent.putExtra(Intent.EXTRA_SUBJECT, getActivity().getString(R.string.report_problem_subject));
        intent.putExtra(Intent.EXTRA_TEXT, getActivity().getString(R.string.send_email_body));
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        ArrayList<Uri> uris = new ArrayList<Uri>();
        DumpSqlToFile();
        Log.w("PhoneLocker", "Adding log file");
        Uri logUri = Uri.fromFile(new File(Constants.GetLogFileDir(getActivity().getApplicationContext())));
        Log.w("PhoneLocker", "Adding smsRulesFile");
        Uri smsRulesUri = Uri.fromFile(new File(Constants.GetSmsConditionDumpFilePath(getActivity().getApplicationContext())));
        Log.w("PhoneLocker", "Adding callRules file");
        Uri callRulesUri = Uri.fromFile(new File(Constants.GetCallConditionDumpFilePath(getActivity().getApplicationContext())));
        uris.add(smsRulesUri);
        uris.add(callRulesUri);
        uris.add(logUri);
        intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
        startActivity(Intent.createChooser(intent, getActivity().getString(R.string.send_email)));
    }

    private void DumpSqlToFile() {
        CallRulesDao callRulesDao = new CallRulesDao(dm.getDb());
        String callRulesDump = callRulesDao.dumpToFile();
        SmsRulesDao smsRulesDao = new SmsRulesDao(dm.getDb());
        String smsRulesDump = smsRulesDao.dumpToFile();
        WriteFile(smsRulesDump, Constants.GetSmsConditionDumpFilePath(getActivity().getApplicationContext()));
        WriteFile(callRulesDump, Constants.GetCallConditionDumpFilePath(getActivity().getApplicationContext()));
    }

    private void WriteFile(String content, String filePath) {
        FileOutputStream outputStream;
        try {
            outputStream = new FileOutputStream(filePath, false);
            outputStream.write(content.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setMenuVisibility(true);
    }

    private void BuildRequestForm() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.feature_request_dialog_title));
        final EditText featureRequestEditTextBox = new EditText(getActivity().getApplicationContext());
        featureRequestEditTextBox.setHint(getResources().getString(R.string.feature_request_dialog_hint));
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        featureRequestEditTextBox.setLayoutParams(lp);
        builder.setView(featureRequestEditTextBox);
        builder.setPositiveButton(getString(R.string.compose_send), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sendFeatureRequest(featureRequestEditTextBox.getText().toString(), getResources().getConfiguration().locale.getCountry());
                dialog.cancel();
            }
        });
        builder.setNegativeButton(getString(R.string.compose_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void sendFeatureRequest(String content, String country) {
        new SendHttpRequest().execute(new String[]{content, country});
    }

    //Odswiez liste smsow
    @Override
    public void onResume() {
        super.onResume();
    }

    //Przekaz poczatkowa liste smsow do adaptera i utworz liste
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        dm = new DataManagerImpl(getActivity().getApplicationContext());
        final View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        TextView versionNumberView = (TextView) rootView.findViewById(R.id.versionNumber);
        versionNumberView.setText(Constants.GetVersion(getActivity().getApplicationContext()));
        TextView reportBugView = (TextView) rootView.findViewById(R.id.reportProblem);
        reportBugView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExportDataOnClick(v);
            }
        });
        TextView sendFeatureRequestView = (TextView) rootView.findViewById(R.id.sendFeatureRequest);
        sendFeatureRequestView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BuildRequestForm();
            }
        });
        return rootView;
    }
}
