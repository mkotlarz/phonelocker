package com.mkotlarz.mobile.productivity.phonelocker;

//Import required android references
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;

//Import required java libraries
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.*;

//Import necessary libraries from PhoneLocker project
import com.android.internal.telephony.ITelephony;
import com.mkotlarz.mobile.productivity.phonelocker.data.DataManagerImpl;
import com.mkotlarz.mobile.productivity.phonelocker.interfaces.ICondition;
import com.mkotlarz.mobile.productivity.phonelocker.models.CallsModel;

import org.apache.log4j.Logger;

/*
Klasa zajmujaca sie pobieraniem informacji o polaczeniu i odpowiada za jego odrzcenie
@author maciej_kotlarz
 */
public class CallsReceiver {

	 private ITelephony telephonyService;
	 private DataManagerImpl dm;
     private long id;
    private final Logger Log = Logger.getLogger(CallsReceiver.class);
/*
 * Odrzuca polaczenia na podstawie danych przekazanych z MainReceiver.java, najważniejsza część aplikacji
 * oraz sprawdza ustawienia autorespondera i gdy jest on włączony wysyła auto-odpowiedź
 * @author Maciej Kotlarz
 */
	 public void abortCall(Context context, Intent intent, int typ, TelephonyManager telephony, ICondition matchedCondition) {
         //Start processing call
          Log.info("Start precessing call");

          //Get Data from Intent
		  Bundle extras = intent.getExtras();

          //Get formated Date to database save
          SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd-MM-yyyy", Locale.getDefault());
          String date = sdf.format(new Date());
		  String call_type = extras.getString(TelephonyManager.EXTRA_STATE);

         //Get PhoneNumber from intent
          String phoneNumber = "";
         if (call_type.equals("RINGING")) {
             phoneNumber = extras
                     .getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
         }

         //Log data about call
         Log.info("Call type: " + call_type + " | RINGING - incoming, OFFHOOK - outgoing");
         Log.info("Call from: " + phoneNumber);
         Log.info("Call date: " + date);
         Log.info("Call id: "   + id);

         //Abort call
		   int i = 0;
		   try {
			    if (i == 0 ) {
                   Class c = Class.forName(telephony.getClass().getName());
                   Method m = c.getDeclaredMethod("getITelephony");
                   m.setAccessible(true);
                   telephonyService = (ITelephony) m.invoke(telephony);
                   //telephonyService.silenceRinger();
                   telephonyService.endCall();
                   i++;
				 }
			  }
              catch (Exception ex)
              {
                   //If something went wrong log the stack trace
                   Log.error(ex.getMessage() + "..." + ex.getStackTrace());
		      }

         //Get contact name by phone number
         Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
         ContentResolver contentResolver = context.getContentResolver();
         String contactName = "";

         //Get contactName from contact Lookup
         try {
         Cursor contactLookup = contentResolver.query(uri, new String[] {BaseColumns._ID, ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);
             if(contactLookup.moveToFirst()) {
                 contactName = contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
             }
         } catch (Exception ex) {
             //Log error data
             Log.error("Lookup problem for: " + phoneNumber + " " + ex.getMessage() + "..." + ex.getStackTrace());
         }

         //Save incoming call
         if (typ == 0 ) {
             try {
                 String matched_on_json = "{ \"ClassCode\": " + matchedCondition.getClassCode() + ", \"MatchedOn\": \"" + matchedCondition.getMatchedOn() + "\" }";
                 CallsModel call = new CallsModel(phoneNumber, date, typ, contactName, matched_on_json);
                 dm = new DataManagerImpl(context);
                 id = dm.saveCall(call);
             }
             catch (Exception ex) {
                 Log.error(ex.getMessage() + " Try reinstalling application" + ex.getStackTrace().toString());
             }
         }


            //Checking info about Autoresponder state
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            boolean autoresponder_state;
            autoresponder_state = prefs.getBoolean("autoresponder_state", false);

            //If autoresponder is enabled send SMS message to caller
            if(autoresponder_state)
            {
                    SmsManager manager = SmsManager.getDefault();
                    try
                    {
                        String message_body = prefs.getString("message_body", "DEFAULT");
                        manager.sendTextMessage(phoneNumber, null, message_body, null, null);
                        Log.info("SMS message has been send to: " + phoneNumber);
                    }
                    catch(Exception ex)
                    {
                            Log.error(ex.getMessage() + " Autoresponder problem" + ex.getStackTrace().toString());
                    }
            }
    }

    public void saveOCall(Context context, Intent intent, ICondition matchedCondition) {
        Log.info("Start precessing call");
        String call_type = "OUTGOING";
        Bundle extras = intent.getExtras();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd-MM-yyyy", Locale.getDefault());
        String date = sdf.format(new Date());
        String phoneNumber = extras.getString(intent.EXTRA_PHONE_NUMBER);
        Log.info("Call type: " + call_type);
        Log.info("Call from: " + phoneNumber);
        Log.info("Call date: " + date);
        Log.info("Call id: "   + id);

        //Get contact name by phone number
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        ContentResolver contentResolver = context.getContentResolver();
        String contactName = "";
        try {
            Cursor contactLookup = contentResolver.query(uri, new String[] {BaseColumns._ID,
                    ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);
            if(contactLookup.moveToFirst()) {
                contactName = contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
            }
        } catch (Exception ex) {
            Log.error(ex.getMessage() + "..." + ex.getStackTrace());
        }

        //Save outgoing call
        try {
            String matched_on_json = "{ \"ClassCode\": " + matchedCondition.getClassCode() + ", \"MatchedOn\": \"" + matchedCondition.getMatchedOn() + "\" }";
            CallsModel call = new CallsModel(phoneNumber, date, 0, contactName, matched_on_json);
            dm = new DataManagerImpl(context);
            id = dm.saveCall(call);
        }
        catch (Exception ex) {
            Log.error(ex.getMessage() + " Try reinstalling application" + ex.getStackTrace().toString());
        }

    }
} 