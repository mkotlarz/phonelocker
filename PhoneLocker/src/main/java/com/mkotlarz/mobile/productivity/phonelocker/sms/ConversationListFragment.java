package com.mkotlarz.mobile.productivity.phonelocker.sms;

/**
 * Created by Maciej on 2014-07-13.
 */
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.getbase.floatingactionbutton.AddFloatingActionButton;
import com.mkotlarz.mobile.productivity.phonelocker.R;
import com.mkotlarz.mobile.productivity.phonelocker.adapter.ConversationAdapter;
import com.mkotlarz.mobile.productivity.phonelocker.sms.models.DataProvider;

import java.util.Observable;
import java.util.Observer;

/**
 * A list fragment representing a list of Conversations. This fragment
 * also supports tablet devices by allowing list items to be given an
 * 'activated' state upon selection. This helps indicate which item is
 * currently being viewed in a {@link ConversationDetailFragment}.
 * <p>
 * Activities containing this fragment MUST implement the {@link }
 * interface.
 */
public class ConversationListFragment extends Fragment implements Observer {

    /**
     * The serialization (saved instance state) Bundle key representing the
     * activated item position. Only used on tablets.
     */
    private static final String STATE_ACTIVATED_POSITION = "activated_position";

    /**
     * The fragment's current callback object, which is notified of list item
     * clicks.
     */

    /**
     * The current activated item position. Only used on tablets.
     */
    private int mActivatedPosition = ListView.INVALID_POSITION;


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ConversationListFragment() {
    }

    public ConversationAdapter conversationAdapter;
    public ListView m_listView = null;
    public RelativeLayout m_relativeLayout = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        // Listen for changes in the data provider
        DataProvider.getInstance().addObserver(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_conversation_list, container, false);
        rootView.findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
        conversationAdapter = new ConversationAdapter(getActivity().getLayoutInflater(), DataProvider.getInstance().getConversations(), getActivity().getApplicationContext());
        m_relativeLayout = (RelativeLayout) rootView.findViewById(R.id.loadingPanel);
        m_listView = (ListView) rootView.findViewById(R.id.conversationListListView);
        m_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent detailIntent = new Intent(getActivity().getApplicationContext(), ConversationDetailActivity.class);
                detailIntent.putExtra(ConversationDetailFragment.ARG_ITEM_ID, DataProvider.getInstance().getConversations().get(position).getSender());
                startActivity(detailIntent);
            }
        });
        m_listView.setVisibility(View.GONE);
        getActivity().getActionBar().setTitle(getResources().getString(R.string.title_activity_conversation_list));
        AddFloatingActionButton mFab = (AddFloatingActionButton) rootView.findViewById(R.id.AddSmsFloatingButton);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity().getApplicationContext(), ComposeSmsActivity.class));
            }
        });
        UpdateDataInThread();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void UpdateDataInThread()
    {
        UpdateTask task = new UpdateTask(getActivity().getApplicationContext());
        task.execute();
    }



    @Override
    public void onDestroy() {
        // Remove ourself as an observer on the data provider
        DataProvider.getInstance().deleteObserver(this);

        super.onDestroy();
    }

    public void onItemSelected(String id) {
        return;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.conversation_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Restore the previously serialized activated item position.
        if (savedInstanceState != null
                && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            // Serialize and persist the activated item position.
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }

    /**
     * Turns on activate-on-click mode. When this mode is on, list items will be
     * given the 'activated' state when touched.
     */
    public void setActivateOnItemClick(boolean activateOnItemClick) {
        // When setting CHOICE_MODE_SINGLE, ListView will automatically
        // give items the 'activated' state when touched.
        m_listView.setChoiceMode(activateOnItemClick
                ? ListView.CHOICE_MODE_SINGLE
                : ListView.CHOICE_MODE_NONE);
    }

    private void setActivatedPosition(int position) {
        if (position == ListView.INVALID_POSITION) {
            m_listView.setItemChecked(mActivatedPosition, false);
        } else {
            m_listView.setItemChecked(position, true);
        }

        mActivatedPosition = position;
    }

    /**
     * Implement observer interface
     * @param observable
     * @param data
     */
    @Override
    public void update(Observable observable, Object data) {
        // If the data adapter has changed then we need to reload the list dataadapter
        //conversationAdapter.notifyDataSetChanged();
    }

    private class UpdateTask extends AsyncTask<Void, Void, Void> {

        private Context m_context = null;

        public UpdateTask(Context context) {
            m_context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            m_listView.setAdapter(null);
        }

        @Override
        protected Void doInBackground(Void... params) {
            DataProvider.getInstance().updateMessagesList(m_context);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            m_listView.setAdapter(conversationAdapter);
            conversationAdapter.notifyDataSetChanged();
            m_relativeLayout.setVisibility(View.GONE);
            m_listView.setVisibility(View.VISIBLE);
        }
    }

}
