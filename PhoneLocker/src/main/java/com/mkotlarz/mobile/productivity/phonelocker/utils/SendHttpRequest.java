package com.mkotlarz.mobile.productivity.phonelocker.utils;

import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;

/**
 * Created by Maciej Kotlarz on 2014-11-02.
 */
public class SendHttpRequest extends AsyncTask<String, Void, String> {
    @Override
    protected String doInBackground(String... params) {
        String content = params[0];
        String country = params[1];
        try {
            //http://mkotlarz-feature-requests.herokuapp.com/feature_requests/:content/:country
            String url = "http://mkotlarz-feature-requests.herokuapp.com/feature_requests/" + URLEncoder.encode(content, "utf-8").toString() + "/" + URLEncoder.encode(country, "utf-8");
            HttpResponse response = null;
            // Create http client object to send request to server
            HttpClient client = new DefaultHttpClient();
            // Create URL string
            // Create Request to server and get response
            HttpGet httpget= new HttpGet();
            httpget.setURI(new URI(url));
            response = client.execute(httpget);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }
}