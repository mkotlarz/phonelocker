package com.mkotlarz.mobile.productivity.phonelocker.models;

import java.util.Date;

public class SmsRulesModel extends ModelBase {


    private long id;
    private String ruleName;
    private String date;
    private String ruleConditions;
    private int ruleActive;

    //Constructor for save
    public SmsRulesModel(String ruleName, String ruleConditions, int ruleActive, String date) {
        this.ruleName               =   ruleName;
        this.ruleConditions         =   ruleConditions;
        this.ruleActive             =   ruleActive;
        this.date					= 	date.toString();
    }
    //Universal constructor
    public SmsRulesModel() {
        //Empty constructor
    }

    public long getId() { return this.id; }

    public void setId(long id) { this.id = id; }

    public String getRuleName() {
        return this.ruleName;
    }

    public String getRuleDate() {
        return this.date;
    }

    public String getRuleConditions() { return this.ruleConditions; }

    public int getRuleActive() { return this.ruleActive; }


    public void setRuleName(String ruleName) { this.ruleName = ruleName; }

    public void setDate(Date date) { this.date = date.toString(); }

    public void setDate(String date) { this.date = date; }

    public void setRuleConditions(String ruleConditionsJSONEncoded) { this.ruleConditions = ruleConditionsJSONEncoded; }

    public void setRuleActive(int ruleActive) { this.ruleActive = ruleActive == 1 ? 1 : 0; }

}
