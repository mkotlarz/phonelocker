package com.mkotlarz.mobile.productivity.phonelocker.interfaces;

import android.content.Context;

/**
 * Created by Maciej on 2014-06-30.
 */
public interface ICondition {
    public String getJSONName();

    public Class<?> getConditionClass();

    public int getClassCode();

    public String getMatchedOn();

    public void setMatchedOn(String matchedOn);

    public String getClassTranslatedName(Context ctx);
}
