package com.mkotlarz.mobile.productivity.phonelocker.models.conditions;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mkotlarz.mobile.productivity.phonelocker.R;
import com.mkotlarz.mobile.productivity.phonelocker.interfaces.ICondition;

import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Maciej on 2014-06-29.
 */
public class PatternCondition implements Serializable, ICondition{

    @Expose
    @SerializedName("pattern")
    private String m_pattern = null;

    @Expose
    @SerializedName("match_contact_name")
    private boolean m_match_contact_name = false;

    @Expose
    @SerializedName("match_number")
    private boolean m_match_number = false;

    @Expose
    @SerializedName("match_message_body")
    private boolean m_match_message_body = false;

    private String m_matched_on = null;

    public final static String JSON_NAME = "PatternCondition";

    public String getMatchedOn() { return m_matched_on; }

    public void setMatchedOn(String matched_on) { m_matched_on = matched_on; }

    public void setPattern(String pattern) {
        m_pattern = pattern;
    }

    public String getPattern() {
        return m_pattern;
    }

    public void setMatchContactName(boolean match) {
        m_match_contact_name = match;
    }

    public boolean getMatchContactName() {
        return m_match_contact_name;
    }

    public void setMatchNumber(boolean match) {
        m_match_number = match;
    }

    public boolean getMatchNumber() {
        return m_match_number;
    }

    public void setMatchMessageBody(boolean match) {
        m_match_message_body = match;
    }

    public boolean getMatchMessageBody() {
        return m_match_message_body;
    }

    public String getJSONName() {
        return "PatternCondition";
    }

    public Class<?> getConditionClass() {
        return NumberCondition.class;
    }

    public int getClassCode() {
        return 7;
    }

    public boolean Match(String callNumber, String contact_name, String message_body) {
        final Logger Log = Logger.getLogger(PatternCondition.class);
        Pattern pattern = Pattern.compile(m_pattern);
        if(m_match_contact_name) {
            Log.info("Matching if: " + contact_name + " match pattern: " + m_pattern);
            if (match(contact_name, pattern)) {
                Log.info("Result: true");
                m_matched_on = m_pattern;
                return true;
            }
            Log.info("Result: false");
        }
        if(m_match_number) {
            Log.info("Matching if: " + callNumber + " match pattern: " + m_pattern);
            if (match(callNumber, pattern)) {
                Log.info("Result: true");
                m_matched_on = m_pattern;
                return true;
            }
            Log.info("Result: false");
        }
        if(m_match_message_body) {
            Log.info("Matching if: " + message_body + " match pattern: " + m_pattern);
            if (match(message_body, pattern)) {
                Log.info("Result: true");
                m_matched_on = m_pattern;
                return true;
            }
            Log.info("Result: false");
        }
        Log.info("Matching PatternCondition result: false");
        return false;
    }

    private boolean match(String content, Pattern pattern) {
        Matcher matcher = pattern.matcher(content);
        return matcher.find();
    }

    public String getClassTranslatedName(Context ctx) {
        return ctx.getString(R.string.pattern_condition);
    }
}
