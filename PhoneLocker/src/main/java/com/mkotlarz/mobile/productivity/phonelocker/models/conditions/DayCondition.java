package com.mkotlarz.mobile.productivity.phonelocker.models.conditions;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mkotlarz.mobile.productivity.phonelocker.R;
import com.mkotlarz.mobile.productivity.phonelocker.interfaces.ICondition;

import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.Calendar;

/**
 * Created by Maciej on 2014-06-29.
 */
public class DayCondition implements Serializable, ICondition {

    @Expose
    @SerializedName("Day")
    private int m_day = 0;

    @Expose
    @SerializedName("everyWeek")
    private boolean m_everyWeek = false;

    private String m_matched_on = null;

    public final static String JSON_NAME = "DayCondition";

    private String getDay(int day) {
        switch (day) {
            case 0:
                return "Sunday";
            case 1:
                return "Monday";
            case 2:
                return "Tuesday";
            case 3:
                return "Wednesday";
            case 4:
                return "Thursday";
            case 5:
                return "Friday";
            case 6:
                return "Saturday";
        }
        return null;
    }

    public String getMatchedOn() { return m_matched_on; }

    public void setMatchedOn(String matched_on) { m_matched_on = matched_on; }

    public int getDayDisplay() {
        return m_day;
    }

    public void setDay(int m_day) {
        this.m_day = m_day;
    }
//
//    public boolean isEveryWeek() {
//        return m_everyWeek;
//    }

    public void setEveryWeek(boolean m_everyWeek) {
        this.m_everyWeek = m_everyWeek;
    }

    public String getJSONName() {
        return "DayCondition";
    }

    public Class<?> getConditionClass() {
        return DayCondition.class;
    }

    public int getClassCode() {
        return 2;
    }

    public boolean Match() {
        final Logger Log = Logger.getLogger(DayCondition.class);
        Log.info("Matching if: " + (Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1) + " equals " + m_day);
        if((Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1) == m_day) {
            Log.info("Result: true");
            m_matched_on = getDay(m_day);
            return true;
        }
        Log.info("Result: false");
        return false;
    }

    public String getClassTranslatedName(Context ctx) {
        return ctx.getString(R.string.day_condition);
    }
}
