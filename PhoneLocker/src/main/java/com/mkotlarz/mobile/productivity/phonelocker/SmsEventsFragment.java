package com.mkotlarz.mobile.productivity.phonelocker;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mkotlarz.mobile.productivity.phonelocker.R;
import com.mkotlarz.mobile.productivity.phonelocker.adapter.ViewAdapter;
import com.mkotlarz.mobile.productivity.phonelocker.data.DataManagerImpl;

import android.support.v4.app.NavUtils;

public class SmsEventsFragment extends Fragment {

    private DataManagerImpl dm = null;
    private String[][] m_values = null;
    private ListView m_listView = null;
    private ViewAdapter m_adapter = null;
    private int m_type = 2;

    public SmsEventsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setMenuVisibility(true);
    }

    //Odswiez liste smsow
    @Override
    public void onResume() {
        super.onResume();
        m_values = dm.getAllSMS();
        m_adapter.refreshAdapterValues(m_values);
    }

    //Przekaz poczatkowa liste smsow do adaptera i utworz liste
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        dm = new DataManagerImpl(getActivity().getApplicationContext());
        m_values = dm.getAllSMS();
        final View rootView = inflater.inflate(R.layout.fragment_events, container, false);
        m_listView =(ListView) rootView.findViewById(R.id.list);
        m_adapter = new ViewAdapter(getActivity(), m_values, m_type);
        m_listView.setAdapter(m_adapter);
        m_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
                Intent intent = new Intent(rootView.getContext(), EventInfo.class);
                intent.putExtra("typ", m_type);
                intent.putExtra("event_id", id);
                startActivity(intent);
            }
        });
        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_home) {
            Intent intent = new Intent(getActivity().getApplicationContext(), LockerMain.class);
            startActivity(intent);
            return true;
        }
        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(getActivity());
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.events, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }
}
