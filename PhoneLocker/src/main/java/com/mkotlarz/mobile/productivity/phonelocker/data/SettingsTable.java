package com.mkotlarz.mobile.productivity.phonelocker.data;

import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public final class SettingsTable {
	
	public static final String TABLE_NAME = "settings";
//Create table settings
	   public static class SettingsColumns implements BaseColumns {
		      public static final String INCOMING_CALLS = 	"incoming_calls";
		      public static final String OUTGOING_CALLS = 	"outgoing_calls";
		      public static final String INCOMING_SMS = 	"incoming_sms";
              public static final String WIFI =             "wifi";
              public static final String NETWORK =          "network";
	   }

	   public static void onCreate(SQLiteDatabase db) {
	      StringBuilder sb = new StringBuilder();

	      // movie table
	      sb.append("CREATE TABLE " + SettingsTable.TABLE_NAME + " (");
	      sb.append(BaseColumns._ID + " INTEGER PRIMARY KEY, ");
	      sb.append(SettingsColumns.INCOMING_CALLS + 	" INTEGER, ");
	      sb.append(SettingsColumns.OUTGOING_CALLS + 	" INTEGER, ");
	      sb.append(SettingsColumns.INCOMING_SMS + 		" INTEGER, ");
          sb.append(SettingsColumns.WIFI +              " INTEGER, ");
          sb.append(SettingsColumns.NETWORK +           " INTEGER  ");
	      sb.append(");");
	      db.execSQL(sb.toString());
           }

	   public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
           if(newVersion == 1) {
               db.execSQL("DROP TABLE IF EXISTS " + SettingsTable.TABLE_NAME);
               SettingsTable.onCreate(db);
           }
	   }

}
